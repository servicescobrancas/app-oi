var js  = [
    './public/js/app/*.*' // Todos os arquivos do diretório js e sub diretórios
]

var css = [
    './public/css/app/*.*' // Todos os arquivos do diretório js e sub diretórios
]

// Núcleo do Gulp
var gulp = require('gulp');
var watch = require('gulp-watch')
var cssmin = require('gulp-cssmin')
var stripCssComments = require('gulp-strip-css-comments')
// Transforma o javascript em formato ilegível para humanos
var uglify = require("gulp-uglify");
// Agrupa todos os arquivos em um
var concat = require("gulp-concat");

// Tarefa de minificação do Javascript
gulp.task('minify-css', function () {
    gulp.src(css)                        // Arquivos que serão carregados, veja variável 'js' no início
        .pipe(concat('style.min.css'))      // Arquivo único de saída
        .pipe(stripCssComments({all: true}))
        .pipe(cssmin())
        .pipe(gulp.dest('./public/css/'));   // pasta de destino do arquivo(s)
});

// Tarefa de minificação do Javascript
gulp.task('minify-js', function () {
    gulp.src(js)                        // Arquivos que serão carregados, veja variável 'js' no início
        .pipe(concat('script.min.js'))      // Arquivo único de saída
        .pipe(uglify())                     // Transforma para formato ilegível
        .pipe(gulp.dest('./public/js/'));          // pasta de destino do arquivo(s)
});

// Tarefa padrão quando executado o comando GULP
gulp.task('default',['minify-js', 'minify-css']);

// Tarefa de monitoração caso algum arquivo seja modificado, deve ser executado e deixado aberto, comando "gulp watch".
gulp.task('watch', function() {
    gulp.watch(js, ['minify-js']);
});