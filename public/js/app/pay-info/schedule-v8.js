Date.daysBetween = function( date1, date2 ) {
    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms/one_day);
};

var controlCalendar;
var messageDate = 'Selecione uma data para pagamento em até 5 dias.';

$(function() {

    controlCalendar = new Calendar();
    $('.select-schedule').on('click', function() {

        var datePayment = controlCalendar.getDate();
        var day = datePayment.getDate();
        var monthIndex = datePayment.getMonth();
        var year = datePayment.getFullYear();

        var monthNumbers = [
            "01", "02", "03",
            "04", "05", "06", "07",
            "08", "09", "10",
            "11", "12"
        ];
        if(day < 9) {
            day = "0" + day;
        }

        var datePaymentFormat = day + '/' + monthNumbers[monthIndex] + '/' + year;

        var dateSelect = new Date(year, monthIndex, day);
        var today = new Date();
        var diffDaysSelect = Date.daysBetween(today, dateSelect);

        if(diffDaysSelect <= 5 && diffDaysSelect >= -1) {

            showLoading();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/pay-info/validate-date",
                data: {date_compare:datePaymentFormat},
                success: function (data) {

                    if(data.status) {

                        $('#loading').show();
                        $('.container').fadeOut('fast',function(){
                            if(today.toString().indexOf("Sun") > -1 && (dateSelect.getDay() == today.getDay()) ) {
                                displayMessageFunction("Para sua comodidade estamos ajustando sua data de pagamento para amanhã!", function() {
                                    document.location.href = '/pay-info/register-schedule/' + data.days;
                                });
                            } else if(dateSelect.toString().indexOf("Sun") > -1) {
                                displayMessageFunction("Para sua comodidade estamos ajustando sua data de pagamento para o próximo dia!", function () {
                                    document.location.href = '/pay-info/register-schedule/' + data.days;
                                });
                            } else{
                                document.location.href = '/pay-info/register-schedule/' + data.days;
                            }


                        });

                        return true;
                    }
                    displayMessage(messageDate);
                },
                error: function (data, req) {
                    console.log('Error:');
                    console.log(data);
                }
            });
        } else {
            displayMessage(messageDate);
        }

        return false;
    });

});



