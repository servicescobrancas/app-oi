
function DateConfirm() {

}

DateConfirm.prototype.init = function() {

    if(window.dateConfirm == undefined) {
        return false;
    }

    var contract = $('#contract').val();

    // Logic start
    $(function() {
        $('#send-date').on('click', function() {

            var date = controlCalendar.getDate();
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            var monthNumbers = [
                "01", "02", "03",
                "04", "05", "06", "07",
                "08", "09", "10",
                "11", "12"
            ];

            var dateConfirm = day + '/' + monthNumbers[monthIndex] + '/' + year;

            showLoading();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/pay-info/register-date",
                data: {date_confirm:dateConfirm,contract:contract},
                success: function (data) {
                    if(data.status) {
                        window.location.href = '/pay-info/return-register-date';
                        return true;
                    }
                    displayMessage(data.message);
                },
                error: function (data, req) {
                    console.log('Error:');
                    console.log(data);
                }
            });
        });
    });
};

DateConfirm.prototype.validateDate = function() {

    return true
};


var controlCalendar;
var dateConfirm;

$(function() {
    controlCalendar = new Calendar();
    dateConfirm = new DateConfirm();
    dateConfirm.init();
});





