
function Step3() {

}

Step3.prototype.init = function() {

    $('#next-step').on('click', function(e) {
        e.preventDefault();
        if(window.step3.validateForm()) {
            window.step3.sendForm()
        }
    })

    var phone = $('#phone')
    phone.val('')

    /*
    //telefone('#phone')

    var maskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009'
        },
        options = {onKeyPress: function(val, e, field, options) {
            field.mask(maskBehavior.apply({}, arguments), options)
        }
    }

    phone.mask(maskBehavior, options)
    */

    phone.mask('(00) 00000-0000');

    $('.modal').modal()
}

Step3.prototype.validateForm = function() {

    var email   = $('#email').val()
    var phone   = $('#phone').val()

    var status = true
    var message = ''
    if(!window.step3.validateEmail(email)) {
        message += 'Informe um e-mail válido!<br/>'
        status = false
    }

    if(!window.step3.validatePhone(phone)) {
        message += 'Informe um telefone válido!<br/>'
        status = false
    }

    if (!status) {
        displayMessage(message)
    }

    return status
}

Step3.prototype.validateEmail = function (email) {

    if (email == '') {
        return false
    }

    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    return filter.test(email)
}

Step3.prototype.validatePhone = function (phone) {

    phone = phone.replace("(", "")
    phone = phone.replace(")", "")
    phone = phone.replace("-", "")
    phone = phone.replace(" ", "")

    if (phone.length < 10) {
        return false
    }

    return true
}

Step3.prototype.sendForm = function() {

    showLoading()

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/passo-3-enviar',
        data: $('#send-step').serialize(),
        success: function (data) {
            if(data.status) {
                window.location.href= '/atualizacao-concluida'
                return true;
            } else {
                window.location.href= '/contact'
                return true;
            }
        },
        error: function (data, req) {
        }
    })
}

Step3.prototype.displayErrorMessage = function(message) {

    $('#loading').hide()
    $('.container').fadeIn()

    $("#message").html(message)
    $("#message").show()

    setTimeout(function() {
        $("#message").slideUp()
    }, 3500)
}

$(function () {
    window.step3 = new Step3()
    window.step3.init()

} )
