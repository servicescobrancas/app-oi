
function Step1() {

    this.message = '123';
}

Step1.prototype.validateDoc = function(doc) {

    if(doc.length < 11) {
        this.message += 'Informe um CPF/CNPJ válido!<br/>'
        return false
    } else if(doc.length > 11 && doc.length < 14) {
        this.message += 'Informe um CNPJ válido!<br/>'
        return false
    }

    return true
}

Step1.prototype.init = function() {

    var self = this

    $('#next-step').on('click', function(e) {
        e.preventDefault()
        if(window.step1.validateForm()) {
            window.step1.sendForm()
        }
    })

    var cpfObj = $('#cpf')
    cpfObj.val('')

    cpfObj.on('keypress', function() {
        mascara(this, soNumeros)
    })

    cpfObj.on('focus', function() {
        $(this).removeClass('invalid')
    })

    cpfObj.on('blur', function() {
        var cpf_cnpj = cpfObj.val();

        if (cpf_cnpj == '' || !self.validateDoc(cpf_cnpj)) {
            cpfObj.addClass('invalid')
            displayMessage('Informe um CPF/CNPJ válido!')
        } else {
            cpfObj.removeClass('invalid')
            cpfObj.addClass('valid')
        }
    })
}

Step1.prototype.validateForm = function() {

    var cpf_cnpj   = $('#cpf').val()

    if(cpf_cnpj == "" || !this.validateDoc(cpf_cnpj)) {
        displayMessage('Informe um CPF/CNPJ válido!')
        return false
    }

    return true
}



Step1.prototype.sendForm = function() {

    showLoading()

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/passo-1-enviar',
        data: $('#send-step').serialize(),
        success: function (data) {
            if(data.status) {
                window.location.href= '/passo-2'
                return true;
            } else if (!data.check_recaptcha)  {
                alert('Verificação inválida')
                hideLoading()
                return true;
            } else  {

                window.location.href= '/contact/not-found'
                return true;
            }
        },
        error: function (data, req) {
        }
    })
}

Step1.prototype.displayErrorMessage = function(message) {

    $('#loading').hide()
    $('.container').fadeIn()

    $("#message").html(message)
    $("#message").show()

    setTimeout(function() {
        $("#message").slideUp()
    }, 3500)
}

$(function () {
    window.step1 = new Step1()
    window.step1.init()

} )
