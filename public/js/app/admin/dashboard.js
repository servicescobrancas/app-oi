
function displayDataUnit(unit)
{
    if(unit > 0) {
        window.location.href = '/manager/manager-dashboard/unit/' + unit;
    } else {
        window.location.href = '/manager';
    }
    return false;
}

jQuery(function() {
    jQuery('#flag').bind('change', function() {
        displayDataUnit(jQuery(this).val());
    });
});