
var fieldSearchTopComments = '#search_top_comments';

function searchTopComments()
{
    var string = jQuery(fieldSearchTopComments).val();
    if(string == '') {
        jQuery('.comments-list').show();
        return false;
    }
    jQuery('.comments-list').each(function() {
        var content = jQuery(this).html();
        if(!strstr(content, string)) {
            jQuery(this).hide();
        } else {
            jQuery(this).show();
        }
    });
    return false;
}

jQuery(function() {
    jQuery(fieldSearchTopComments).bind('keydown', function() {
        window.setTimeout(searchTopComments, 500);
    });
});