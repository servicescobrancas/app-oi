
function iOSversion() {

    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        if (!!window.indexedDB) { return 'iOS 8 and up'; }
        if (!!window.SpeechSynthesisUtterance) { return 'iOS 7'; }
        if (!!window.webkitAudioContext) { return 'iOS 6'; }
        if (!!window.matchMedia) { return 'iOS 5'; }
        if (!!window.history && 'pushState' in window.history) { return 'iOS 4'; }
        return true;
    }

    return false;
}

function showLoading()
{
    $('#bar').hide()
    $('nav').hide()
    $('.container').hide()
    $('.header-bar').hide()
    $('.rodape').hide()

    $('#loading').show()
}

function showLoadingFunction(functionGo)
{
    $('#bar').hide()
    $('nav').hide()
    $('.header-bar').hide()
    $('.container').hide()
    $('.rodape').hide()

    $('#loading').show()

    functionGo()
}

function hideLoading()
{
    $('#loading').hide()

    $('#bar').fadeIn('slow')
    $('nav').fadeIn('slow')
    $('.header-bar').fadeIn('slow')
    $('.container').fadeIn('slow')
    $('.rodape').fadeIn('slow')
}


function displayMessage(msg)
{
    hideLoading();

    $('#block-screen').show();

    $('#message-text').html(msg);
    $('#message-box').show();

    $('#message-close').bind('click', function() {
        console.log('click')
        $('#message-box').fadeOut();
        $('#block-screen').fadeOut();
    });

    $('#block-screen').click(function() {
        $('#message-box').hide();
        $('#block-screen').hide();
    });
}

function displayMessageFunction(msg, functionGo)
{
    hideLoading();

    $('#block-screen').show();

    $('#message-text').html(msg);
    $('#message-box').show();

    $('#message-close').bind('click', function() {
        $('#message-box').fadeOut();
        $('#block-screen').fadeOut();
        functionGo();
    });

    $('#message-close').bind('click', function() {
        $('#message-box').hide();
        $('#block-screen').hide();
        functionGo();
    });
}

function goTo(url)
{
    showLoading()
    window.location.href = url
    return false
}

function displayMessageForm(message, id)
{
    var elementError =  $(id).parent().find('.mdl-textfield__error');

    elementError.html(message);
    elementError.css("visibility", "visible");

    return elementError;
}

function hideMessageForm(id)
{
    var elementError =  $(id).parent().find('.mdl-textfield__error');
    elementError.css("visibility", "hidden");
}

var dialogMessage;

$(function(){

    Materialize.updateTextFields();

    // Initialize collapse button
    $(".button-collapse").sideNav();

    // Initialize collapsible (uncomment the line below if you use the dropdown variation)
   $('.collapsible').collapsible();

    //$("input.numeric").numeric();

    $('.back-link').on('click', function() {
        var link = $(this).attr('rel');
        if(link != null && link != undefined) {

            showLoadingFunction(function(){
                document.location.href = link;
            });
        }
    });

    $('.go-to').on('click', function() {
        var link = $(this).attr('rel');
        var value = $(this).attr('value');

        if(link != null && link != undefined) {

            showLoadingFunction(function(){
                document.location.href = link;
            });
        }
    });

    // Prevent key enter submit
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    if(document.URL.indexOf("/contact/chat") == -1) {
        // Loader between pages
        $('#loading').hide();
        $('.container').fadeIn();
    }
});