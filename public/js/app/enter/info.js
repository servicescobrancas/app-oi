
function Info() {

    this.message = '';
}

Info.prototype.validateDoc = function(doc) {

    if(doc.length < 11) {
        this.message += 'Informe um CPF/CNPJ válido!<br/>'
        return false
    } else if(doc.length > 11 && doc.length < 14) {
        this.message += 'Informe um CNPJ válido!<br/>'
        return false
    }

    return true
}

Info.prototype.init = function() {

    $('#next-step').on('click', function(e) {
        e.preventDefault()
        if(window.info.validateForm()) {
            window.info.sendForm()
        }
    })

    var phone = $('#phone')
    phone.val('')

    phone.mask('(00) 00000-0000')
}

Info.prototype.validateForm = function() {

    var email   = $('#email').val()
    var phone   = $('#phone').val()

    var status = true
    var message = ''
    if(!window.info.validateEmail(email)) {
        message += 'Informe um e-mail válido!<br/>'
        status = false
    }

    if(!window.info.validatePhone(phone)) {
        message += 'Informe um telefone válido!<br/>'
        status = false
    }

    if (!status) {
        displayMessage(message)
    }

    return status
}

Info.prototype.validateEmail = function (email) {

    if (email == '') {
        return false
    }

    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    return filter.test(email)
}

Info.prototype.validatePhone = function (phone) {

    phone = phone.replace("(", "")
    phone = phone.replace(")", "")
    phone = phone.replace("-", "")
    phone = phone.replace(" ", "")

    if (phone.length < 10) {
        return false
    }

    return true
}

Info.prototype.sendForm = function() {

    showLoading()

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/enter/send-info',
        data: $('#send-step').serialize(),
        success: function (data) {
            if(data.status) {
                window.location.href= '/enter/return-info'
                return true;
            }
            displayMessage('Houve um erro ao atualizar os dados!')

        },
        error: function (data, req) {
        }
    })
}

Info.prototype.displayErrorMessage = function(message) {

    $('#loading').hide()
    $('.container').fadeIn()

    $("#message").html(message)
    $("#message").show()

    setTimeout(function() {
        $("#message").slideUp()
    }, 3500)
}

$(function () {
    window.info = new Info()
    window.info.init()
})
