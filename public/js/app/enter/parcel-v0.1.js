
var selectedOption = null;

function loadEnter() {

    var classColorSelected = 'blue';

    $('.select-block').on('click', function() {
        var option = $(this).attr('rel');

        $('.select-block').removeClass(classColorSelected).addClass('grey');


        if(option != selectedOption) {
            $(this).removeClass('grey');
            $(this).addClass(classColorSelected);

            selectedOption = option;

            $.ajax({
                type: "GET",
                dataType: "json",
                url: "/enter/update-invoices-parcel",
                data: {"item":selectedOption},
                success: function () {

                },
                error: function (data, req) {
                    console.log('Error:');
                    console.log(data);
                }
            });
        }
    });


    $('.filled-in').on('click', function() {

        $('.filled-in').attr('rel', 0);

        var obj = $(this);
        var item = obj.val();

        $('#select_option').val(item);

        selectedOption = true;
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/enter/update-invoices-parcel",
            data: {"item":item},
            success: function () {
                obj.attr('rel', 1);
            },
            error: function (data, req) {
                console.log('Error:');
                console.log(data);
            }
        });

    });

    $('.go-to-enter').click(function() {
        var link = $(this).attr('rel');
        var value = $(this).attr('value');

        var item =  $('#select_option').val();
        if(item != "") {

            showLoadingFunction(function(){
                document.location.href = link;
            });
        } else {
            displayMessage('Selecione uma forma de pagamento.');
        }
    });
}


$(function() {
    loadEnter();
});