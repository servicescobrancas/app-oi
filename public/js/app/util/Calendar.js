
var Calendar = function() {

    this.currentDate = new Date();

    this.day   = this.currentDate.getUTCDate();
    this.month = this.currentDate.getUTCMonth() + 1;
    this.year  = this.currentDate.getFullYear();

    /**
     * Idenfy fields day
     * @type {string}
     */
    this.dayField = '#day';

    /**
     * Idenfy fields month
     * @type {string}
     */
    this.monthField = '#month';

    /**
     * Idenfy fields year
     * @type {string}
     */
    this.yearField = '#year';


    /**
     * Idenfy field desc date
     * @type {string}
     */
    this.fieldDescDate = '#date_desc';

    /**
     * Contais desc months
     * @type {Array}
     */
    this.monthsDesc = [];

    this.limitYearUp = this.year + 1;
    this.limitYearDown = this.year - 1;

    this.init();
};

Calendar.prototype.init = function() {

    /**
     * Set date for default fields
     */
    this.configMonthsDesc();

    this.setDateDefault();

    this.selectDayUp();
    this.selectDayDown();

    this.selectMonthUp();
    this.selectMonthDown();

    this.selectYearUp();
    this.selectYearDown();
};

Calendar.prototype.configMonthsDesc = function() {

    this.monthsDesc.push("Janeiro");
    this.monthsDesc.push("Fevereiro");
    this.monthsDesc.push("Março");
    this.monthsDesc.push("Abril");
    this.monthsDesc.push("Maio");
    this.monthsDesc.push("Junho");
    this.monthsDesc.push("Julho");
    this.monthsDesc.push("Agosto");
    this.monthsDesc.push("Setembro");
    this.monthsDesc.push("Outubro");
    this.monthsDesc.push("Novembro");
    this.monthsDesc.push("Dezembro");
};

Calendar.prototype.setDateDefault = function() {

    var dayFormat =  this.formatNumber(this.day);
    $(this.dayField).html(dayFormat);

    var monthFormat = this.formatNumber(this.month);
    $(this.monthField).html(monthFormat);

    $(this.yearField).html(this.year);

    this.updateDescFullDate();
};

Calendar.prototype.updateDescFullDate = function() {

    var dayFormat = this.formatNumber(this.day);

    var descFullDate = "";
    var descMonth = this.getMonthDesc(this.month - 1);

    descFullDate += dayFormat + " de " + descMonth + " de " + this.year;
    $('#select-date').html(descFullDate);
};

Calendar.prototype.getMonthDesc = function(index) {
    return this.monthsDesc[index];
};

// check link for update button with one click: http://stackoverflow.com/a/4961201
Calendar.prototype.selectDayUp = function() {

    var self = this;

    $('.up-day').on('click', function(e) {
        var day = parseInt($(self.dayField).html());
        day += 1;

        var dayFormat = self.formatNumber(day);
        self.day = parseInt(day);

        if(self.validateDateDay()) {
            $(self.dayField).html(dayFormat);
        }

    });
};

Calendar.prototype.selectDayDown = function() {

    var self = this;

    $('.down-day').on('click', function() {
        var day = parseInt($(self.dayField).html());
        day -= 1;

        self.day = parseInt(day);

        if(self.validateDateDay()) {
            var dayFormat = self.formatNumber(day);
            $(self.dayField).html(dayFormat);
        }
    });

    this.day = parseInt($('#day').html());
};

Calendar.prototype.selectMonthUp = function() {

    var self = this;

    $('.up-month').on('mousedown', function() {
        var month = parseInt($(self.monthField).html());
        month += 1;

        self.month = parseInt(month);
        self.validateDateMonth();
    });
};

Calendar.prototype.selectMonthDown = function() {

    var self = this;

    $('.down-month').on('mousedown', function() {
        var month = parseInt($(self.monthField).html());
        month -= 1;
        self.month = parseInt(month);
        self.validateDateMonth();
    });
};

Calendar.prototype.selectYearUp = function() {

    var self = this;

    this.limitYearDown = this.year - 1;

    $('.up-year').on('click', function() {
        var year = parseInt($(self.yearField).html());
        year += 1;
        if(year > self.limitYearUp) {
            --year;
            return false;
        }

        self.year = parseInt(year);
        $(self.yearField).html(year);

        self.validateDateMonth();
    });
};

Calendar.prototype.selectYearDown = function() {

    var self = this;

    $('.down-year').on('click', function() {
        var year = parseInt($(self.yearField).html());
        year -= 1;
        if(year < self.limitYearDown) {
            ++year;
            return false;
        }

        self.year = parseInt(year);
        $(self.yearField).html(year);
        self.validateDateMonth();
    });
};

Calendar.prototype.validateDateDay = function() {

    var day = this.day;
    var month = parseInt(this.month) - 1;
    var year = parseInt(this.year);

    var currentMonthDesc = this.getMonthDesc(month);
    this.currentDate = new Date(year, month, day);
    var setMonthDesc = this.getMonthDesc(this.currentDate.getMonth());

    var status = true;
    if(currentMonthDesc != setMonthDesc) { // condition for chance date select

        this.day = this.currentDate.getUTCDate();
        this.month = this.currentDate.getUTCMonth() + 1;
        this.year = this.currentDate.getFullYear();

        $(this.dayField).html( this.formatNumber(this.day) );
        $(this.monthField).html( this.formatNumber(this.month) );
        $(this.yearField).html( this.formatNumber(this.year) );

        status = false;
    }
    this.updateDescFullDate();

    return status;
};

Calendar.prototype.validateDateMonth = function() {

    var day = 1;
    var month = parseInt(this.month) - 1;
    var year = parseInt(this.year);

    this.currentDate = new Date(year, month, day);

    this.day = this.currentDate.getUTCDate();
    this.month = this.currentDate.getUTCMonth() + 1;
    this.year = this.currentDate.getFullYear();

    $(this.dayField).html( this.formatNumber(this.day) );
    $(this.monthField).html( this.formatNumber(this.month) );
    $(this.yearField).html( this.formatNumber(this.year) );

    this.updateDescFullDate();
    return true;
};

Calendar.prototype.formatNumber = function(number) {
    if(number < 10) {
        number = "0" + number;
    }
    return number;
};

Calendar.prototype.getDate = function() {
    return this.currentDate;
};
