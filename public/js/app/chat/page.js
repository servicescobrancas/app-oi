/**
 * @todo add websocket for next versions.
 *
 * @constructor
 */
function Page () {

  this.currentMessageId = 0
  this.token = ''

  this.bntSend = $('#send-message')
  this.messages = $('#messages')

  this.templateMessageLeft = $('#template-message-left').html()
  this.templateMessageRight = $('#template-message-right').html()

  this.init()
}

Page.prototype.init = function () {

  var self = this
  this.loadFirstMessage()
  this.bntSend.on('click', function () {
    self.sendMessage($('#message').val())
  })

}

Page.prototype.loadFirstMessage = function () {

  // @todo carrega a primeira mensagem baseado no token ou parâmetro.

  var self = this;

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "/chat/open-conversation",
    success: function (data) {
      if (data.status) {
        self.token = data.token
        self.currentMessageId = data.id
      }

      $(data.messages).each( function (index, message) {
        self.displayMessageServer(message)
      })


    },
    error: function (data, req) {

    }
  })
}

Page.prototype.sendMessage = function (message) {

  if (message === '') {
    return false
  }

  this.templateMessageClient(message)
  var self = this;

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "/chat/send-message",
    data: {message:message, id: self.currentMessageId},
    success: function (data) {
      if (data.status) {
        self.currentMessageId = data.id
      }

      $(data.messages).each( function (index, message) {
        self.displayMessageServer(message)
      })

      $('#message').val('')
    },
    error: function (data, req) {

    }
  })
}

Page.prototype.displayMessageServer = function (message) {

  var firstMessageTemplate = this.templateMessageLeft.replace('[message]', message);
  $('.messages').append( firstMessageTemplate )

  this.scrollMessages()
}

Page.prototype.templateMessageClient = function (message) {

  var messageTemplate = this.templateMessageRight.replace('[message]', message);

  $('.messages').append( messageTemplate )

  $('.messages div:last').fadeIn()

  this.scrollMessages()

}

Page.prototype.scrollMessages = function () {

  var messages = $('.messages')
  messages.stop().animate({
    scrollTop: messages[0].scrollHeight
  }, 800);

}



$(function () {
  window.page = new Page()
})




