<?php

date_default_timezone_set('America/Sao_Paulo');

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../app/config/environment.php';

if(PROD) {

    $host = $_SERVER["HTTP_HOST"];
    $uri  = $_SERVER["REQUEST_URI"];

    if(!strstr($host, '.br') || !strstr($host, 'pague') || strstr($host, 'www.') != '') {
        header('HTTP/1.1 301 Moved Permanently');
        header('Location:https://oipaguefacil.com.br'.$uri);
        exit();
    }

    if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off') {
        header('Location:https://oipaguefacil.com.br'.$uri);
        exit();
    }
}


if (isset($_SERVER["HTTP_REFERER"]) && $_SERVER["HTTP_REFERER"] == "https://www.google.com.br/") {
    echo 'Restrict Access.';
    exit();
}

if( isset($_GET['server']) ) {
    echo '<pre>';
    var_dump($_SERVER);
}

function d($data, $type = 'var_dump', $stop = true)
{
    echo '<pre>';
    $type($data);
    echo '</pre>';

    if($stop) {
        exit();
    }
}

function debug($data, $method = '', $desc = '', $stop = true)
{
    echo ' -------------------------------------------------------------------- <br /><br /';
    if ($method != '') {
        echo 'Método <b>' . $method . '</b> <br/><br/>';
    }

    if ($desc != '') {
        echo '' . $desc . '<br/><br/>';
    }

    print("<pre>".print_r($data,true)."</pre>");

    if($stop) {
        exit();
    }
}



$app = new \App\LocalApplication();
$app->run();