<?php

$currentIP = gethostbyname(trim(`hostname`));

define('DOMAIN', 'oipaguefacil.com.br');
define('RECAPCHA_KEY', '6LdRTSYUAAAAAJZDtaVCcXSfSKE89Wg7aHlGYyv2');


define('GTM_NORMAL', 'GTM-5CZXDDM'); // for normal access

define('MOCK_DATA', false);

define('PROD_IP', '169.57.141.8');
define('TEST_IP', '169.57.134.204'); // servidor de testes
define('LOCAL_IP', '');

define('PROD', ($currentIP == PROD_IP));
define('TEST', ($currentIP == TEST_IP));
define('DEV', (!PROD && !TEST));

define('API_USER', 'SERVICESAZULAPI');
define('API_PASS', '8ACF6484-2EA4-475F-A936-92F00A931306');
define('UNIT', '00008');

if(PROD) {
    define('URL_WS', 'http://187.5.36.50:8081/apiNoAzul');
    define('URL_WS_SRV', 'http://localhost:8098/api');
    define('URL_WS_OCO', 'http://192.168.210.110/oitelecom/vcomcob_api/api');
} else if(TEST) {

    $ip = filter_input(INPUT_GET, 'ip');
    if ($ip == '') {
        define('URL_WS', 'http://187.5.36.50:8081/apiNoAzul');
    } else {
        define('URL_WS', 'http://'.$ip.':8081/apiNoAzul');
    }

    define('URL_WS_SRV', 'http://localhost:8089/api');
    define('URL_WS_OCO', 'http://192.168.210.110/oitelecom/vcomcob_api/api');
} else {
    define('URL_WS', 'http://192.168.210.111/apiNoAzul');
    define('URL_WS_SRV', 'http://localhost:8098/api');
    define('URL_WS_OCO', 'http://192.168.210.110/oitelecom/vcomcob_api/api');
}