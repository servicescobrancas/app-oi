<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://192.168.210.110/oitelecom/vcomcob_api/token/gerartoken",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\n\t\"DocumentoConsumidor\" : \"02282353919\",\n\t\"IdentificadorUnidadeDeCobranca\" : 1\n}",
  CURLOPT_HTTPHEADER => array(
    "api-senha: 1234",
    "api-usuario: Acesso.Portal",
    "content-type: application/json",
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

?>