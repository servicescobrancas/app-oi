
http://192.168.210.111/apiNoAzul/token/gerartoken

Headers:
{
	"api-senha":"8ACF6484-2EA4-475F-A936-92F00A931306",
	"api-usuario":"SERVICESAZULAPI",
	"Content-Type":"application/json
}


Parâmetros:
{
  "DocumentoConsumidor" : "00352442751",
  "IdentificadorUnidadeDeCobranca" : "00008"
}


Retorno:

{
  "Erro": true,
  "Data": [
    {
      "Codigo": 0,
      "Mensagem": "There was no endpoint listening at http://192.168.210.111/VCServicosNoAzul/servicosnoazul.svc that could accept the message. This is often caused by an incorrect address or SOAP action. See InnerException, if present, for more details."
    }
  ],
  "Processamento": "00:00:00.11"
}