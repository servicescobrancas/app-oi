
function Contact() {
}

Contact.prototype.init = function() {
    if(window.contact == undefined) {
        return false;
    }

    // Logic start login
    $('#next-step').on('click', function(e) {
        e.preventDefault();

        if(window.contact.validateForm()) {
            window.contact.sendForm();
        }
    });
};

Contact.prototype.validateForm = function() {
    var name  = $('#name').val();
    var phone = $('#phone').val();
    var email = $('#email').val();
    var type = $('#type').val();

    if(type == 'whatsapp' || type == 'sms' || type == 'call' || type == 'chat' || type == 'telegram') {
        if(name == '' || phone == '') {
            window.contact.displayErrorMessage('Preencha todos os campos.');
            return false;
        }
        if(!validatePhone(phone)) {
            window.contact.displayErrorMessage('Informe um celular válido!');
            return false;
        }

    } else if(type == 'email') {
        if(name == '' || email == '') {
            window.contact.displayErrorMessage('Preencha todos os campos.');
            return false;
        }
        if(!validateEmail(email)) {
            window.contact.displayErrorMessage('Informe um e-mail válido!');
            return false;
        }
    } else {
        window.contact.displayErrorMessage('Tipo não setado.');
        return false;
    }

    return true;
};

Contact.prototype.sendForm = function() {

    $('#loading').show();
    $('.container').fadeOut('fast',function(){});

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/contact/send",
        data: $('#form-contact').serialize(),
        success: function (data) {
            console.log(data)
            if(data.status) {
                if (data.channel == '4') {
                    var cpf = $('#cpf').val()

                    window.location.href= 'http://atendimento.oipaguefacil.com.br/omne/virtual/chat/' + cpf + '/10'
                    return true;
                }
                window.location.href= '/contact/return-message'
                return true;
            }
            window.login.displayErrorMessage(data.message)
        },
        error: function (data, req) {
            console.log('Error:')
            console.log(data)

            //window.location.href = '/'
        }
    });
};

Contact.prototype.displayErrorMessage = function(message) {

    alert(message)

}


// Logic start login
$(function() {

    window.contact = new Contact()
    window.contact.init()
})

