
function filter() 
{
    var buscar = false;
    var parametrosBusca = [];
    
    for(var i = 0; i < campos.length; ++i) {
        var campoValor = jQuery('input[name="'+ campos[i] +'"]').val();
        console.log(campoValor);
        if(campoValor != '') {
            var campoNome = jQuery('#' + campos[i]).attr('id');
            var jsonPar = campoNome+"="+encodeURIComponent(campoValor);
            parametrosBusca.push(jsonPar);
            buscar = true;
        }
    }
    
    if(!buscar) {
        alert('Informe os parâmetros para efetuar uma busca!');
        jQuery('#' + campos[0]).focus();
        return false;
    } else {
        var query = parametrosBusca.join("&");
        urlDest = urlBase + '?' + query; 
        window.location = urlDest;
    }
    
    return false;
}

function alterConfigLimit(par)
{
    urlDest = urlBase + '/config-limit/' + par; 
    window.location = urlDest;
}

jQuery(function() { 
    jQuery('.search-field').keydown(function(e) {
        if (e.keyCode == 13) {
            return filter();
        }
    });
    
    jQuery('.confirm-delete').click(function() {
        var hrefDelete = jQuery(this).attr('href'); 
        var textConfirm = jQuery(this).attr('rel');
          
        if(textConfirm) {
            window.location.href = hrefDelete; 
        }
        
        return false;
    });
    
    jQuery('.btn-danger').click(function() {
        var hrefDelete  = jQuery(this).attr('href'); 
        var textConfirm = jQuery(this).attr('rel');
        
        jQuery('.modal-body').html(textConfirm);
        jQuery('#confirm-delete').click(function() {
            window.location.href = hrefDelete;
        });
        
        
        jQuery('#modal-1').modal('show');
        return false;
    });
    
    jQuery.each(parsBusca,function(key, value) {
        jQuery("#" + key).val(value);
    });
 });