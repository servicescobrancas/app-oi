
var invoices = [];
var blockSendIfNoHasInvoices = false;
var totalInvoicesTotal = $('#invoices_total_field').val();

function loadEnter() {

    if(online) {

        $('#btn-imprimir-boleto').on('click', function () {
            ga('send', 'event', 'link', 'click', 'Imprimir boleto clicado');
        });

        $('#btn-boleto-email').on('click', function () {
            ga('send', 'event', 'link', 'click', 'Enviar boleto por SMS clicado');
        });

        $('#btn-boleto-sms').on('click', function () {
            ga('send', 'event', 'link', 'click', 'Enviar boleto por SMS clicado');
        });
    }

    $('.item-invoice').on('click', function() {
        if($(this).attr('rel') == 1) {
            removeItem(invoices, $(this).attr('id'));

            $(this).removeClass('item-invoice-active');
            $(this).find('img').attr('src', 'imagens/unchecked_option.png');
            $(this).attr('rel', 0);

            calculateTotalInvoices();
            updateInvoices($(this).attr('id'), false);

        } else {
            invoices.push($(this).attr('id'));

            $(this).addClass('item-invoice-active');
            $(this).find('img').attr('src', 'imagens/checked_option.png');
            $(this).attr('rel', 1);

            calculateTotalInvoices();
            updateInvoices($(this).attr('id'), true);
        }
    });

    $('.go-to-enter').click(function() {
        var link = $(this).attr('rel');
        var value = $(this).attr('value');

        var blockSend = true;
        $('.item-invoice').each(function() {
            if($(this).attr('rel') == 1) {
                blockSend = false;
            }
        });

        if(link != null && link != undefined) {
            if(online) {
                ga('send', 'event', 'link','click', value + ' foi clicado.');
            }

            showLoadingFunction(function(){
                document.location.href = link;
            });
        }
    });
}

function calculateTotalInvoices()
{
    var total = 0.0;
    //
    $('.item-invoice').each(function() {
        if($(this).attr('rel') == 1) {
            total += formatNumberToFloat($(this).attr('content'));
        }
    });

    blockSendIfNoHasInvoices = (total == 0);
    //$('#total_invoices').html( "R$ " + total.formatMoney(2, ',', '.') );
}

function formatNumberToFloat(number)
{
    number = number.replace('.', '');
    number = number.replace(',', '.');
    number = number.replace('R$ ', '');

    return parseFloat(number);
}


function removeItem(array, item)
{
    var length = array.length;

    for(var i = 0; i < length; ++i) {
        if(array[i] == item) {
            invoices.splice(i, 1);
        }
    }
}

function updateInvoices(date, status)
{
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/enter/update-invoices",
        data: {"date":date, "status":status},
        success: function (data) {
            $('#total_invoices').html( data.invoices_total);
        },
        error: function (data, req) {
            console.log('Error:');
            console.log(data);
        }
    });
}