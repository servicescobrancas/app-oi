
var currentTarget = '';

$(function(){

        $('.modal-button').on('click', function() {
            var target = $(this).attr('data-target');

            if(target == '#inform_email') {

                if(flagEmail) {
                    $('#block-screen').show();
                    sendMail();
                    return true;
                }

                currentTarget = target;
                $(target).show();
                $('#block-screen').show();
            }

            if(target == '#inform_phone') {

                if(flagPhone) {
                    $('#block-screen').show();
                    sendSms();
                    return true;
                }

                currentTarget = target;
                $(target).show();
                $('#block-screen').show();
            }
        });

        $('#block-screen').on('click', function(event) {
            $(currentTarget).hide();
            $('#block-screen').hide();
        });

        $('#send-email').on('click', function(e) {

            e.preventDefault();
            var email = $('#email').val();

            if(flagEmail) {
                if(!validateEmail(email)) {
                    alert('Informe um e-mail válido!');
                    return false;
                }

                $('#inform_email').hide();

                showLoading();

                sendMail();
                return true;
            }

            if(email == '' || !validateEmail(email)) {
                alert('Informe um e-mail válido!');
                return false;
            } else {
                $('#inform_email').hide();

                showLoading();

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    data:{email:email},
                    url: '/pay-info/register-email',
                    success: function (data) {

                        if(data.status) {
                            sendMail();
                            return true;
                        }

                        hideLoading();

                        alert(data.message);
                    },
                    error: function (data, req) {
                        console.log(data);
                    }
                });
            }

        });


    });

function sendMail() {

    var link = $('#btn-boleto-email').attr('rel');

    $.ajax({
        type: "GET",
        dataType: "json",
        url: link,
        success: function (data) {

            if(data.status) {
                window.location.href= '/pay-info/return-send-email';
                return true;
            }

            hideLoading();

            alert(data.message);
        },
        error: function (data, req) {
        }
    });
}

$('#send-phone').on('click', function() {

    showLoading();

    var phone = $('#phone').val();
    if(!validatePhone(phone) ) {
        hideLoading();
        alert('Informe um telefone válido!');
        return false;
    } else {

        $('#inform_phone').hide();
        $('.container').hide();

        $('#loading').show();

        $.ajax({
            type: "POST",
            dataType: "json",
            data:{phone:phone},
            url: '/pay-info/register-phone',
            success: function (data) {

                if(data.status) {
                    sendSms();
                    return true;
                }

                hideLoading();

                alert(data.message);
            },
            error: function (data, req) {
            }
        });
    }
});

function  sendSms()
{
    var link = $('#btn-boleto-sms').attr('rel');

    $.ajax({
        type: "GET",
        dataType: "json",
        url: link,
        success: function (data) {

            if(data.status) {
                window.location.href= '/pay-info/return-send-sms';
                return true;
            }

            $('#loading').hide();
            $('.container').fadeIn();

            alert(data.message);
        },
        error: function (data, req) {
        }
    });
}

/******************************************************************************************/

$('.btn-boletos').on('click', function() {
    var link = $(this).attr('rel');

    var conditionLink = (link != null && link != undefined && link == '/boleto/print');

    if(conditionLink) {
        showLoading();

        $.ajax({
            type: "GET",
            dataType: "json",
            url: link,
            success: function (data) {

                hideLoading();

                // Execute actions for boleto - email or print
                if(data.status) {
                    if(link == '/boleto/print') {
                        var iframePrint = $('#iframe_print');
                        iframePrint.on('load', function(){
                            console.log('load the iframe');
                        });

                        window.location.href= '/boleto/download';
                        return false;
                    } else {
                        window.location.href= '/pay-info/return-send-email';
                    }

                    //window.location.href= '/enter/options';
                    return true;
                }
                alert(data.message);
            },
            error: function (data, req) {
            }
        });
    }
    else {
        if(flagEmail && link == '/boleto/email') {

            $('#inform_email').hide();
            $('.container').hide();

            $('#loading').show();

            sendMail();
            return true;
        }

        if(flagPhone && link == '/boleto/sms') {

            $('#inform_sms').hide();
            $('.container').hide();

            $('#loading').show();

            sendSms();
            return true;
        }
    }
});
