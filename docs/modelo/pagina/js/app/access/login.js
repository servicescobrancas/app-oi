
function Login() {

    this.name;
    this.phone;
    this.email;
    this.cpf;
}

Login.prototype.init = function() {
    if(window.login == undefined) {
        return false;
    }

    // Logic start login

    $('#form-send').on('click', function(e) {
        e.preventDefault();
        if(window.login.validateForm()) {
            window.login.sendForm();
        }
    });



    $('#name').val('');
    $('#phone').val('');
    $('#email').val('');
    $('#cpf').val('');


    $('#phone').on('keypress', function() {
        mascara(this, soNumeros);
    });

    $('#cpf').on('keypress', function() {
        mascara(this, soNumeros);
    });
};

Login.prototype.validateForm = function() {
    this.name  = $('#name').val();
    this.phone = $('#phone').val();
    this.email = $('#email').val();
    this.cpf   = $('#cpf').val();

    if(this.name == '' || this.phone == '' || this.cpf == '') {
        displayMessage('Preencha todos os campos');
        return false;
    }

    if(!validatePhone(this.phone)) {
        displayMessage('Informe um celular válido!');
        return false;
    }

    if(!validateCPF(this.cpf)) {
        displayMessage('Informe um CPF válido!');
        return false;
    }

    return true;
};

Login.prototype.sendForm = function() {

    showLoading();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/login",
        data: $('#form-login').serialize(),
        success: function (data) {
            if(data.status) {
                window.location.href= '/enter/options';
                return true;
            } else {
                window.location.href= '/contact';
                return true;
            }
        },
        error: function (data, req) {
        }
    });
};

Login.prototype.displayErrorMessage = function(message) {

    $('#loading').hide();
    $('.container').fadeIn();

    $("#message").html(message);
    $("#message").show();

    setTimeout(function() {
        $("#message").slideUp();
    }, 3500);
};

Login.prototype.checkToken = function() {

};

var login = new Login();
login.init();