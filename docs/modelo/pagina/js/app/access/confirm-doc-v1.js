
function ConfirmDoc() {

}

ConfirmDoc.prototype.init = function() {

    $('.send-form').on('click', function(e) {
        e.preventDefault();
        window.confirmDoc.checkDoc();
    });

    $('#form-confirm input').keydown(function(e) {
        if (e.keyCode == 13) {
            window.confirmDoc.checkDoc();
        }
    });

    $( window ).resize(function() {
        if($(window).height() < 900) {
            $('.atend-b').hide();
        } else {
            $('.atend-b').show();
        }
    });
};

ConfirmDoc.prototype.checkDoc = function() {

    var docPartial = $('#confirm-partial-doc').val();

    if(docPartial == '') {
        displayMessage('Preencha o campo com os dados solicitados.');
        return false;
    }

    showLoading();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "/confirm-doc",
        data: $('#form-confirm').serialize(),
        success: function (data) {
            if(data.status) {
                window.location.href= '/termos';
                return true;
            }
            displayMessage(data.message);
        },
        error: function (data, req) {
            console.log('Error:');
            console.log(data);
        }
    });
    return false;
};

ConfirmDoc.prototype.displayErrorMessage = function(message) {
    $("#message").html(message);
    $("#message").show();

    setTimeout(function() {
        $("#message").slideUp();
    }, 3500);
};

var confirmDoc = new ConfirmDoc();
confirmDoc.init();