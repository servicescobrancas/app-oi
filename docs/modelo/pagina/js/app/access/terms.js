
function Terms() {

}

Terms.prototype.init = function() {

    $('#next-step').on('click', function(e) {
        e.preventDefault();
        if(jQuery( "#accept-terms" ).prop( "checked")) {
            window.terms.sendForm()
        } else {
            displayMessage('É necessário concordar com o termos para prosseguir!')
        }
    })

    jQuery( "#accept-terms" ).prop( "checked", false );
}

Terms.prototype.validateForm = function() {


    return true
}

Terms.prototype.sendForm = function() {

    showLoading()

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/accept-terms',
        data: $('#send-step').serialize(),
        success: function (data) {
            if(data.status) {
                window.location.href= '/enter/menu'
                return true;
            } else {
                window.location.href= '/contact'
                return true;
            }
        },
        error: function (data, req) {
        }
    })
}

Terms.prototype.displayErrorMessage = function(message) {

    $('#loading').hide()
    $('.container').fadeIn()

    $("#message").html(message)
    $("#message").show()

    setTimeout(function() {
        $("#message").slideUp()
    }, 3500)
}

$(function () {
    window.terms = new Terms()
    window.terms.init()

} )
