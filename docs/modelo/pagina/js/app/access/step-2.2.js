
function Step2() {

}

Step2.prototype.init = function() {

    $('#next-step').on('click', function(e) {
        e.preventDefault();
        if(window.step2.validateForm()) {
            window.step2.sendForm()
        }
    })

    $('#date').val('')
    $('#date').mask('00/00/0000')
}

Step2.prototype.validateForm = function() {

    this.cpf   = $('#date').val()

    if(!window.step2.validDate($('#date'), this.cpf)) {
        displayMessage('Data de nascimento inválida!')
        return false
    }

    return true
}

Step2.prototype.validDate = function (campo, valor) {

    var date = valor
    var ardt = []
    var yearStart = (new Date().getFullYear()) - 15;
    var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}")
    ardt=date.split("/")

    erro=false
    if ( date.search(ExpReg)==-1){
        erro = true
    }
    else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
        erro = true
    else if ( ardt[1]==2) {
        if ((ardt[0]>28)&&((ardt[2]%4)!=0))
            erro = true
        if ((ardt[0]>29)&&((ardt[2]%4)==0))
            erro = true
    } else if (ardt[2] > yearStart ) {
        erro = true
    }

    if (erro) {
        campo.focus()
        campo.value = ""
        return false
    }
    return true

}

Step2.prototype.sendForm = function() {

    showLoading()

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/passo-2-enviar',
        data: $('#send-step').serialize(),
        success: function (data) {
            if(data.status) {
                window.location.href= '/passo-3'
                return true;
            } else {
                window.location.href= '/contact'
                return true;
            }
        },
        error: function (data, req) {
        }
    })
}

Step2.prototype.displayErrorMessage = function(message) {

    $('#loading').hide()
    $('.container').fadeIn()

    $("#message").html(message)
    $("#message").show()

    setTimeout(function() {
        $("#message").slideUp()
    }, 3500)
}

$(function () {
    window.step2 = new Step2()
    window.step2.init()

} )
