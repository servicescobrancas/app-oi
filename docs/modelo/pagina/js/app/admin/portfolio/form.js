
function validateForm(formId) {
    
    var validate = true;
    var message  = "";
    
    if(jQuery("#type").val() == "") {
        message += "Informe o tipo do registro.\n";
        validate = false;
    }
    
    if(jQuery("#name").val() == "") {
        message += "Informe o nome do cliente/profissional.\n";
        validate = false;
    }
    
    if(jQuery("#description").val() == "") {
        message += "Preencha o campo Descrição.\n";
        validate = false;
    }
    
    if(jQuery("#id").val() == "") {
        if(jQuery("#type").val() == 1 || jQuery("#type").val() == 2) {
            if(jQuery("#embed").val() == "") {
                message += "Cole o embed no campo indicado.\n";
                validate = false;
            }
        } else if(jQuery("#type").val() == "3") {
            if(jQuery("#file").val() == "") {
                message += "Envie um arquivo.\n";
                validate = false;
            }
        }
    }
    
    
    if(!validate) {
        alert(message);
        return false;
    }
    
    jQuery(formId).submit();
    return true;
}

function handlerEmbed() {
    if(jQuery("#embed").val() != "") {
        jQuery("#block-embed").fadeOut();
        jQuery("#embed-display").html("");
        jQuery("#embed-display").html(jQuery("#embed").val());
        jQuery("#block-embed").fadeIn();
    }
    
}

function handlerType() {
    if(jQuery("#type").val() == "3") {
       jQuery("#block-voice").show(); 
    } else {
       jQuery("#voice").val(""); 
       jQuery("#block-voice").hide(); 
    }
}


jQuery(function () {
    jQuery('button[type="submit"]').click(function() {
        return validateForm("#portfolio");
    });
    
    jQuery("#type").change(function() {
       handlerType(); 
    });
    
    jQuery('#embed').blur(function() {
        handlerEmbed();
    });
    
});