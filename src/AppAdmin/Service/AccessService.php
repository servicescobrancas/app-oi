<?php

namespace AppAdmin\Service;

/**
 * Class AccessService
 * @package App\Service
 */
class AccessService extends BaseService
{

    public function checkAccess($pars)
    {
        $isServices = ($pars['username'] == 'adminoi' && $pars['password'] == 'services#2017');
        $isOi = ($pars['username'] == 'rogerio' && $pars['password'] == 'Services#2017!@');

        return ($isServices || $isOi);
    }
}