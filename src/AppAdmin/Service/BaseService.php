<?php

namespace AppAdmin\Service;

use Doctrine\ORM\EntityManager;

use GuzzleHttp\Client;

use App\Service\BaseService as BaseSite;

class BaseService extends BaseSite
{

    /**
     * Data contains info about entity
     * @var array
     */
    protected $data = [];

    /**
     * Client in session context.
     * @var array
     */
    protected $client;

    /**
     * Flag for tracking status of operation
     * @var bool
     */
    protected $status = true;
    
    /**
     * Message from result operation
     * @var array
     */
    protected $messages = [];

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var int
     */
    protected $userId = 0;

    /**
     * @var int
     */
    protected $userRole = 0;

    /**
     * @var int
     */
    protected $userLevel = 0;


    public function init()
    {
        // implement actions for simulate constructor class
    }

    /**
     * @param int $id
     * @return object
     */
    public function getItem($id)
    {}

    /**
     * @param array $data
     * @return bool
     */
    public function register(array $data)
    {}

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data)
    {}

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {}

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param int $index
     * @return string
     */
    public function getMessage($index = 0)
    {
        return isset($this->messages[$index]) ? $this->messages[$index] : '';
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    public function getUrlData($uri, $method = 'GET')
    {
        $host = 'http://localhost:8089';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $host . $uri . '/');
        $result = curl_exec($curl);
        curl_close($curl);

        return  $result;
    }


    /**
     * @param array $pars
     */
    public function setHeaders($pars = [])
    {
        $this->headersApi['Content-Type'] = 'application/json';
        $this->headersApi['api-senha'] = API_PASS;
        $this->headersApi['api-usuario'] = API_USER;

        if (isset($pars['guid'])) {
            $this->headersApi['api-guid'] = $pars['guid'];
        }

        if (filter_input(INPUT_GET, 'headers') == 1) {
            d($this->headersApi, 'var_dump', false);
        }

        if (filter_input(INPUT_GET, 'client') == 1) {
            d( $this->getClientData(), 'var_dump', false);
        }
    }


    public function getDataApi($uri, $pars)
    {
        if (isset($pars['DocumentoConsumidor'])) {
            $doc = $pars['DocumentoConsumidor'];

        } else {
            $clientData = $this->getClientData();
            $doc = $clientData['DocumentoConsumidor'];
        }

        $id_monitor = $this->apiTimeControl(['uri'=> $uri, 'doc' => $doc, 'pars' => json_encode($pars)] );
        $urlRequest = $this->getUrlWs() . $uri;
        $client = new Client();
        $data = [];
        try {
            $response = $client->request('POST', $urlRequest, [
                    'headers' => $this->headersApi,
                    'json' => $pars]
            );
            if($response->getStatusCode()) {
                $body = $response->getBody();
                $remainingBytes = $body->getContents();
                $data = json_decode($remainingBytes, true, 1024);
                $this->registerInfoCallApi($uri, $pars, $data);

                if ($id_monitor)
                    $this->apiTimeControl(['id'=> $id_monitor, 'return' =>  json_encode($data)], true);
                return $data;
            }
        } catch(\Exception $e) {
            $this->registerErrorApi($uri, $pars, $e->getMessage());

            $dataError = [
                'Erro' => true,
                'Message' => $e->getMessage()
            ];

            if ($id_monitor) {
                $this->apiTimeControl(['id'=> $id_monitor, 'return' =>  json_encode(['error_api' => '1', 'message' => $e->getMessage()]) ], true);
            }

            return $dataError;
        }

        if ($id_monitor) {
            if (count($data) == 0) {
                $data['error_api'] = "Não encontrou o registro";
            }
            $this->apiTimeControl(['id' => $id_monitor, 'return' => json_encode($data)], true);
        }

        return $data;
    }

    /**
     * @param $sql
     * @param string $get
     * @return array|bool|mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executeSql($sql, $get = 'unique')
    {
        if (filter_input(INPUT_GET, 'sql') == 1) {
            echo '<br/>';
            print_r($sql);
            echo '<br/>';
            echo ' ------------------------------------ ';
            echo '<br/>';
        }

        $this->analyzeSql($sql);
        //$this->insertLog($sql);

        $stmt = $this->em->getConnection()->prepare($sql);
        try {
            $stmt->execute();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
            exit();
        }

        if ($get == 'all' ) {
            return $stmt->fetchAll();
        }

        if ($get == null ) {
            return true;
        }

        return $stmt->fetch();
    }

    public function executeSqlNotReturn($sql)
    {
        $stmt = $this->em->getConnection()->prepare($sql);
        try {
            $stmt->execute();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function analyzeSql($sql)
    {
        if (strstr('UPDATE', $sql) != '') {
           // echo $sql; exit();
        }
    }

    public function insertLog($sql)
    {
        $sql = addslashes($sql);
        $userId = $this->userId;

        $sqlLog = ' INSERT INTO register_sql ( ';
        $sqlLog .= ' sql_client, user_id, data ';
        $sqlLog .= ' ) VALUES ( ';
        $sqlLog .= ' "'.utf8_decode($sql).'", '.$userId.', NOW() ';
        $sqlLog .= ' ) ';

        $stmtLog = $this->em->getConnection()->prepare($sqlLog);
        $stmtLog->execute();
    }

    public function getLastId()
    {
        return $this->em->getConnection()->lastInsertId();
    }

    public function getMonth($index)
    {
        $months = [
          '01' => 'Janeiro',
          '02' => 'Fevereiro',
          '03' => 'Março',
          '04' => 'Abril',
          '05' => 'Maio',
          '06' => 'Junho',
          '07' => 'Julho',
          '08' => 'Agosto',
          '09' => 'Setembro',
          '10' => 'Outubro',
          '11' => 'Novembro',
          '12' => 'Dezembro'
        ];

        return isset($months[$index]) ? $months[$index] : null;
    }

    /**
     *
     */
    public function getResult()
    {
        return new \stdClass();
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param int $userRoleId
     */
    public function setUserRole($userRoleId)
    {
        $this->userRole = $userRoleId;
    }

    /**
     *
     * @param $userLevel
     */
    public function setUserLevel($userLevel)
    {
        $this->userLevel = $userLevel;
    }
}
