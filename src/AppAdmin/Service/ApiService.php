<?php

namespace AppAdmin\Service;

use Mockery\CountValidator\Exception;

/**
 * Class AccessService
 * @package App\Service
 */
class ApiService extends BaseService
{
    public function listMethods() {
        try {
            $sql = "select distinct metodo from monitor_api";
            $result = $this->executeSql($sql, 'all');
            return $result;

        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function dataPerMonth($_params)
    {
        try {
            $sql = "select SUM(TIMESTAMPDIFF(SECOND, entrada, saida)) as duracao, count(id) as num, date_format(entrada, '%Y-%m-%d') as data_base from monitor_api";
            $sql .= " where entrada between '".$_params['data1']."' and '".$_params['data2']."'";
            $sql .= " and metodo = '".$_params['metodo']."' ";
            $sql .= " group by data_base";

            $result = $this->executeSql($sql, 'all');
            return (array)$result;

        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function dataPerDay($_dia, $_metodo) {
        try {
            $sql = "select SUM(TIMESTAMPDIFF(SECOND, entrada, saida)) as duracao, count(id) as num, date_format(entrada, '%H') as data_base from monitor_api where entrada like '".$_dia."%' and metodo = '".$_metodo."' group by data_base";
            $result = $this->executeSql($sql, 'all');
            return (array)$result;

        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function retornaNumRequisicoes($data) {
        try {
            $sql = "SELECT count(JSON_EXTRACT(retorno, '$.error_api')) AS erros, metodo,
                    COUNT(id) AS requisicoes FROM monitor_api WHERE entrada LIKE '".$data."%' GROUP BY metodo;";
            $result = $this->executeSql($sql, 'all');
            return (array)$result;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }
}