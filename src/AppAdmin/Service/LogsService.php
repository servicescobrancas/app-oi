<?php

namespace AppAdmin\Service;

use Mockery\CountValidator\Exception;

/**
 * Class LogService
 * @package App\Service
 */
class LogsService extends BaseService
{
    public function listMethods()
    {
        try {
            $sql = "select distinct metodo from monitor_api";
            $result = $this->executeSql($sql, 'all');
            return $result;

        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function lastLogs($pars = [])
    {
        try {
            $doc = isset($pars['doc']) ? $pars['doc'] : '';

            $sql  = " SELECT id, cpf_cnpj, metodo, parametros, retorno, ";
            $sql .= " DATE_FORMAT(entrada, '%d/%m/%Y %H:%i:%s') as entrada, DATE_FORMAT(saida, '%d/%m/%Y %H:%i:%s') as saida  ";
            $sql .= " FROM monitor_api ";
            if ($doc != '') {
                $sql .= " WHERE cpf_cnpj = '".$doc."'";
            }
            $sql .= " order by id desc limit 150";
            $result = $this->executeSql($sql, 'all');
            return $result;

        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }
}