<?php

namespace AppAdmin\Service;

/**
 * Class AccessService
 * @package App\Service
 */
class ExecuteService extends BaseService
{

    private $method = null;
    private $parameters = null;
    private $returnApi = null;

    public function gerartoken($doc)
    {
        $this->method = '/token/gerartoken';
        $this->parameters = [
            'DocumentoConsumidor' => $doc,
            'IdentificadorUnidadeDeCobranca' => UNIT,
        ];

        $this->returnApi = $this->getDataApi($this->method, $this->parameters);
        return $this->returnApi ;
    }

    public function token($doc, $token)
    {
        $this->method = '/token/token';
        $this->parameters = [
            'DocumentoConsumidor' => $doc,
            'ToKen' => $token
        ];
        $this->returnApi = $this->getDataApi($this->method, $this->parameters);
        $data = [];
        $this->status = ($this->returnApi['Erro'] === false);
        if ($this->status) {
            $data =  $this->returnApi['Data'];
        }
        return $this->returnApi;
    }

    /**
     * @param $doc
     * @return array|mixed|null
     */
    public function clearDeals($doc)
    {
        $this->method  = '/negociacao/cancelar';
        $this->parameters = [
            'DocumentoConsumidor' => $doc
        ];

        $this->returnApi = $this->getDataApi($this->method , $this->parameters);
       return $this->returnApi;
    }

    function consultardividas($doc, $token)
    {
        $this->method = '/contas/consultardividas';
        $this->parameters = [
            'Token' => $token
        ];

        $this->parameters = [
            'DocumentoConsumidor' => $doc,
            'ToKen' => $token
        ];

        $this->returnApi = $this->getDataApi($this->method, $this->parameters);

        if ($this->returnApi['Erro'] == false) {
            foreach ($this->returnApi['Data'] as $key => $item) {
                $idConta = $this->returnApi['Data'][$key]['Id'];
                $contrato =  $this->returnApi['Data'][$key]['Contrato'];

                $this->returnApi['Data'][$key]['Id'] = '<a href="/admin/execute/deal/'.$doc.'/'.$idConta.'">'.$idConta.'</a>';

                $linkContrato  = '<a href="/admin/execute/aux/'.$contrato.'" title="Dados Aux.">'.$contrato.'</a> - ';
                $linkContrato .= '<a href="/admin/execute/custom/'.$doc.'" title="Custom">'.$doc.'</a>';
                $this->returnApi['Data'][$key]['Contrato'] = $linkContrato;
            }
        }


        return $this->returnApi;
    }

    public function dadosaux($contrato, $token)
    {
        $this->method = '/Contas/ConsultarDadosAuxiliares';
        $this->parameters = [
            'Contrato' => $contrato,
            'ToKen' => $token
        ];

        $this->returnApi = $this->getDataApi($this->method , $this->parameters);

        return $this->returnApi;
    }

    public function custom($doc, $token)
    {
        $this->method = '/Contas/ConsultarDadosCustomizados';
        $this->parameters = [
            'Documento' => $doc,
            'ToKen' => $token
        ];
        $this->returnApi = $this->getDataApi($this->method , $this->parameters);

        return $this->returnApi;
    }

    public function negociacaoIniciar($idConta, $token)
    {
        $this->method = '/negociacao/iniciar';
        $this->parameters = [
            'IdConta' => $idConta,
            'ToKen' => $token
        ];

        $this->returnApi = $this->getDataApi($this->method , $this->parameters);

        return $this->returnApi;
    }


    public function negociacaoOpcoes($dealId)
    {
        $dataNow = (new \DateTime())->format('Y-m-d');

        $this->method = '/negociacao/opcoes';
        $this->parameters = [
            'IdNegociacao' => $dealId,
            'DataDoCalculo' => $dataNow
        ];

        $this->returnApi = $this->getDataApi($this->method, $this->parameters);

        if ($this->returnApi['Erro'] == false) {
            foreach ($this->returnApi['Data']['Opcoes'] as $key => $item) {
                $idOpcao = $this->returnApi['Data']['Opcoes'][$key]['Id'];
                $idOpcao = $this->returnApi['Data']['Opcoes'][$key]['Id'] = '<a href="/admin/execute/confirm-deal/'.$dealId.'/'.$idOpcao.'">'.$idOpcao.'</a>';
            }
        }

        return $this->returnApi;
    }


    public function finalizarOpcao($dealId, $option)
    {
        $this->method = '/negociacao/finalizar/opcao';
        $this->parameters = [
            'IdNegociacao' => $dealId,
            'IdOpcaoDeCalculo' => $option
        ];

        $this->returnApi = $this->getDataApi($this->method , $this->parameters);

        return $this->returnApi;
    }

    public function emitirboletos($dealId)
    {
        $this->method = '/boleto/emitirboletos';
        $this->parameters = [
            'IdNegociacao' => $dealId
        ];

        $this->returnApi = $this->getDataApi($this->method, $this->parameters);

        return  $this->returnApi;
    }


    /**
     * @return null
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param null $method
     * @return ExecuteService
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return null
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param null $parameters
     * @return ExecuteService
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return null
     */
    public function getReturnApi()
    {
        return $this->returnApi;
    }

    /**
     * @param null $returnApi
     * @return ExecuteService
     */
    public function setReturnApi($returnApi)
    {
        $this->returnApi = $returnApi;
        return $this;
    }
}