<?php

namespace AppAdmin\Service;

use App\Storage\Elastic;
use App\Util\StringUtil;

use AppAdmin\Validate\Post as PostValidate;

class PostService
{

    const INDEX = 'vivo';
    const MAIN_TYPE = 'posts';

    /**
     * @var Elastic
     */
    private $client;

    private $postValidate;

    /**
     * @var array
     */
    protected $messages;

    /**
     * @var bool
     */
    protected $status;

    /**
     * PostService constructor.
     */
    public function __construct()
    {
        $this->client = new Elastic(self::INDEX);
        $this->postValidate = new PostValidate();
    }

    /**
     * @param $pars
     * @param int $from
     * @param int $size
     * @param null $sort
     * @return array
     */
    public function listItems($pars, $from = 0, $size = 10, $sort = null)
    {
        $this->client->setType(self::MAIN_TYPE);
        $this->client->setFrom($from);
        $this->client->setSize($size);
        $this->client->setSort($sort);

        $query = $this->client->getSearchMatch($pars);

        $query = [];
        $result = $this->client->find($query);
        $list = [];

        if(isset($result['hits']['hits'])){
            foreach ($result['hits']['hits'] as $key => $hit) {
                $source = $hit['_source'];
                $source['id'] = $hit['_id'];

                $list[] = $source;
            }
        }

        return $list;
    }

    public function validateCreate($pars)
    {
        $this->postValidate->setPars($pars);
        $this->status = $this->postValidate->validateTitle();
        $this->postValidate = $this->postValidate->validateExcerpt();

        if(!$this->status) {
            $messages = $this->postValidate->getErrorMessages();
            foreach ($messages as $message) {
                $this->addMessage($message);
            }
        }

        return $this->postValidate->getPars();
    }

    /**
     * @param $pars
     * @return mixed
     */
    public function create($pars)
    {
        $this->client->setType(self::MAIN_TYPE);
        $dateTimeFormat = $this->getCurrentDateTime();

        $data = [
            'title' => $pars['title'],
            'slug' => StringUtil::getSlug($pars['title']),
            'excerpt' => $pars['excerpt'],
            'text' => $pars['text'],
            'activated' => $pars['activated'],
            'created' => $dateTimeFormat,
            'modified' => $dateTimeFormat,
        ];

        $response = $this->client->create($data);
        return $response['created'];
    }

    /**
     * @param $id
     * @return array
     */
    public function getItemById($id)
    {
        $this->client->setType(self::MAIN_TYPE);
        $result = [];
        try {
            $data = $this->client->get($id);
            $result = $data['_source'];
            $result['id'] =  $data['_id'];

        } catch(\Exception $e) {

        }

        return $result;
    }

    /**
     * @param $pars
     * @return array
     */
    public function validateUpdate($pars)
    {
        $this->postValidate->setPars($pars);
        $this->status = $this->postValidate->validateTitle();
        $this->status = $this->postValidate->validateExcerpt();

        if(!$this->status) {
            $messages = $this->postValidate->getErrorMessages();
            foreach ($messages as $message) {
                $this->addMessage($message);
            }
        }

        return $this->postValidate->getPars();
    }

    /**
     * @param $pars
     * @return bool
     */
    public function update($pars)
    {
        $this->client->setType(self::MAIN_TYPE);
        $dateTimeFormat = $this->getCurrentDateTime();

        $data = [
            'id' => $pars['id'],
            'title' => $pars['title'],
            'slug' => StringUtil::getSlug($pars['title']),
            'excerpt' => $pars['excerpt'],
            'text' => $pars['text'],
            'thumb' => $pars['thumb'],
            'image' => $pars['image'],
            'activated' => $pars['activated'],
            'created' => $dateTimeFormat,
            'modified' => $dateTimeFormat,
        ];
        $status = $this->client->update($data);

        if ($status) {
            $this->addMessage('Post alterado com sucesso!');
        }

        return $status;
    }

    /**
     * @return string
     */
    public function getCurrentDateTime()
    {
        $date = new \DateTime('now');
        return $date->format('Y-m-d') . 'T' . $date->format('H:i:s');
    }

    /**
     * @param $message
     */
    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
}