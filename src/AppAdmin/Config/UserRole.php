<?php

namespace App\Config;

class UserRole
{
    const ADMIN  = 1;
    const DEVELOPER  = 2;
    const PETITIONER  = 3; // petitioner
    const ANALYST  = 4;// Analyst
    const TESTER  = 5;// Analyst
}