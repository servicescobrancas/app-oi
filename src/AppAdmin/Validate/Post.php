<?php

namespace AppAdmin\Validate;


class Post
{

    /**
     * @var array
     */
    protected $pars;

    /**
     * @var array
     */
    protected $errorMessages;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->pars = [];
        $this->errorMessages = [];
    }

    /**
     * @param $pars
     */
    public function setPars(array $pars)
    {
        $this->pars = $pars;
        $this->applyTrim();
    }

    /**
     * @return array
     */
    public function getPars()
    {
        return $this->pars;
    }

    /**
     * Apply function to pars
     */
    public function applyTrim()
    {
        $this->pars = array_map('trim', $this->pars);
        $this->pars = array_map('ltrim', $this->pars);
        $this->pars = array_map('rtrim', $this->pars);
    }

    public function validateTitle()
    {
        if($this->pars['title'] == '')  {
            $this->errorMessages[] = 'Informe um título para o post!';
            return false;
        }

        return true;
    }

    public function validateExcerpt()
    {
        if($this->pars['excerpt'] == '')  {
            $this->errorMessages[] = 'Informe um resumo para o post!';
            return false;
        }

        return true;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

}