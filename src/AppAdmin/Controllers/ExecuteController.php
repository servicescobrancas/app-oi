<?php

namespace AppAdmin\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppAdmin\Service\ExecuteService;

class ExecuteController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var ExecuteService
     */
    public $service;

    /**
     * @var array
     */
    public $pars;

    /**
     * LogsController constructor.
     * @param ExecuteService $baseService
     */
    public function __construct(ExecuteService $baseService)
    {
        $this->service = $baseService;
    }

    /**
     * @param Application $app
     * @return mixed|\Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');
        $this->get('/start/{doc}', 'start');
        $this->get('/aux/{contract}', 'aux');
        $this->get('/custom/{doc}', 'custom');
        $this->get('/deal/{doc}/{account}', 'deal');
        $this->get('/confirm-deal/{deal}/{option}', 'confirmDeal');


        return $this->controllers;
    }

    /**
     * @todo - refactoring
     *
     * @return string
     */
    public function index()
    {
        $this->checkAccess();

        $data = [
        ];

        $tpl = 'execute/index.twig';
        return $this->render($tpl, $data);
    }

    public function start()
    {
        $this->service->setHeaders([]);

        $doc = $this->getRequest()->get('doc');
        $this->getSession()->set('doc', $doc);
        $contentData = [];

        $dataGerarToken = $this->service->gerartoken($doc);
        $contentData[] = $this->getDisplayResultMethod();

        if (isset($dataGerarToken['Erro']) && $dataGerarToken['Erro'] === false) {

            $this->getSession()->set('gerartoken', $dataGerarToken);
            $token = $dataGerarToken['Data']['Token'];
            $dataToken = $this->service->token($doc, $token);
            $contentGerarToken = $this->getDisplayResultMethod();

            if (isset($dataToken['Erro']) && $dataToken['Erro'] === false) {

                $this->getSession()->set('cilent', $dataToken['Data']);
                $this->getSession()->set('guid', $dataToken['Data']['Guid']);
                $this->getSession()->set('token', $token);

                $contentData[] = $contentGerarToken;

                $this->service->setHeaders(['guid' => $dataToken['Data']['Guid']]);

                $this->service->clearDeals($doc);
                $contentData[] = $this->getDisplayResultMethod();

                $this->service->consultardividas($doc, $token);
                $contentData[] = $this->getDisplayResultMethod();
            }
        }

        $this->getSession()->set('content_start', $contentData);

        print 'Passo 1 <br/><br/>';
        print '/token/gerartoken<br/>';
        print '/token<br/>';
        print '/negociacao/cancelar<br/>';
        print '/contas/consultardividas<br/>';
        print '<br/><br/>';

        $this->displayData($contentData);
    }

    public function aux()
    {
        $guid = $this->getSession()->get('guid');
        $this->service->setHeaders(['guid' => $guid]);

        $contract = $this->getRequest()->get('contract');
        $token = $this->getSession()->get('token');

        $this->service->dadosaux($contract, $token);
        $contentData[] = $this->getDisplayResultMethod();

        print 'Dados Aux. <br/><br/>';
        print '/Contas/ConsultarDadosAuxiliares<br/>';
        print '<br/><br/>';

        $this->displayData($contentData);
    }

    public function custom()
    {
        $guid = $this->getSession()->get('guid');
        $this->service->setHeaders(['guid' => $guid]);

        $doc = $this->getRequest()->get('doc');
        $token = $this->getSession()->get('token');

        $this->service->custom($doc, $token);
        $contentData[] = $this->getDisplayResultMethod();

        print 'Custom.. <br/><br/>';
        print '/Contas/ConsultarDadosCustomizados<br/>';
        print '<br/><br/>';

        $this->displayData($contentData);
    }

    public function deal()
    {
        $guid = $this->getSession()->get('guid');
        $this->service->setHeaders(['guid' => $guid]);

        $doc = $this->getRequest()->get('doc');
        $token = $this->getSession()->get('token');
        $account = $this->getRequest()->get('account');

        $prefix = 'acordos_' . $doc . '_' . $token;

        if (!$this->getSession()->has($prefix)) {
            $dataDeal = $this->service->negociacaoIniciar($account, $token);
            $contentData[] = $this->getDisplayResultMethod();

            if (isset($dataDeal['Erro']) && $dataDeal['Erro'] === false) {
                $negociaoId = $dataDeal['Data']['IdNegociacao'];
                $this->service->negociacaoOpcoes($negociaoId);
                $contentData[] = $this->getDisplayResultMethod();
            }

            $contentDataStart = $this->getSession()->get('content_start');
            //$this->displayData($contentDataStart);
        } else {
            $contentData = $this->getSession()->get($prefix);
        }

        print 'Passo 2 <br/><br/>';
        print '/negociacao/iniciar<br/>';
        print '/negociacao/opcoes<br/>';
        print '<br/><br/>';

        $this->displayData($contentData);
    }

    public function confirmDeal()
    {
        $guid = $this->getSession()->get('guid');
        $this->service->setHeaders(['guid' => $guid]);

        $dealId = $this->getRequest()->get('deal');
        $option = $this->getRequest()->get('option');
        $prefix = 'boletos_' . $dealId . '_' . $option;

        if (!$this->getSession()->has($prefix)) {
            $result = $this->service->finalizarOpcao($dealId, $option);
            $contentData[] = $this->getDisplayResultMethod();
            $status = (isset($result['Erro']) && $result['Erro'] === false);
            if ($status) {
                $returnBol = $this->service->emitirboletos($dealId);
                $contentData[] = $this->getDisplayResultMethod();

                $this->getSession()->set($prefix, $contentData);
            }
        } else {
            $contentData = $this->getSession()->get($prefix);
        }


        print 'Passo 3 <br/><br/>';
        print '/negociacao/finalizar/opcao<br/>';
        print '/boleto/emitirboletos<br/>';
        print '<br/><br/>';
        $this->displayData($contentData);
    }

    public function getDisplayResultMethod()
    {
        $str = '';

        $str .= ' -------------------------------------------------------------------- <br /><br />';

        $str .= 'Método: <b>';
        $str .= $this->service->getMethod();
        $str .= '<br/><br/>';

        if ($this->service->getMethod() != '') {
            $str .= 'Parâmetros: <b>';
            $str .= ("<pre>".print_r($this->service->getParameters(),true)."</pre>");
        }

        if ($this->service->getReturnApi() != '') {
            $str .=  'Retorno: <b>';
            $str .= ("<pre>".print_r($this->service->getReturnApi(),true)."</pre>");
        }

        $str .= '<br /><br />';

        return $str;
    }

    public function displayData($contentData, $block = true)
    {
        foreach ($contentData as $content) {
            print $content;
        }

        if ($block) {
            exit();
        }
    }

}