<?php

namespace AppAdmin\Controllers\Factory;

use Silex\Application;

use AppAdmin\Service\AccessService;
use AppAdmin\Controllers\AccessController;

class AccessControllerFactory
{

    /**
     * @var AccessController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new AccessService($app['orm.em']);
        $this->controller = new AccessController($baseService);
    }

    public function getController()
    {
        return $this->controller;
    }
}