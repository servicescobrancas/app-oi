<?php

namespace AppAdmin\Controllers\Factory;

use Silex\Application;

use AppAdmin\Service\AccessService;
use AppAdmin\Controllers\LoginController;

class LoginControllerFactory
{

    /**
     * @var LoginController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new AccessService($app['orm.em']);
        $this->controller = new LoginController($baseService);
    }

    public function getController()
    {
        return $this->controller;
    }
}