<?php

namespace AppAdmin\Controllers\Factory;

use Silex\Application;

use AppAdmin\Service\ApiService;
use AppAdmin\Service\LogsService;
use AppAdmin\Controllers\LogsController;

class LogsControllerFactory
{

    /**
     * @var LogsController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new LogsService($app['orm.em']);
        $apiService = new ApiService($app['orm.em']);
        $this->controller = new LogsController($baseService);
        $this->controller->setApiService($apiService);
    }

    public function getController()
    {
        return $this->controller;
    }
}