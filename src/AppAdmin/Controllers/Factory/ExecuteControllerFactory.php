<?php

namespace AppAdmin\Controllers\Factory;

use AppAdmin\Controllers\ExecuteController;
use AppAdmin\Service\ExecuteService;
use Silex\Application;

use AppAdmin\Controllers\LogsController;

class ExecuteControllerFactory
{

    /**
     * @var LogsController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new ExecuteService($app['orm.em']);
        $this->controller = new ExecuteController($baseService);
    }

    public function getController()
    {
        return $this->controller;
    }
}