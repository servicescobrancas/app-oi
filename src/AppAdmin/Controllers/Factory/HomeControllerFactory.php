<?php

namespace AppAdmin\Controllers\Factory;

use AppAdmin\Service\ApiService;
use Silex\Application;

use App\Service\BaseService;

use AppAdmin\Controllers\HomeController;

class HomeControllerFactory
{

    /**
     * @var HomeController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $apiService = new ApiService($app['orm.em']);
        $this->controller = new HomeController($apiService);
    }

    public function getController()
    {
        return $this->controller;
    }
}