<?php

namespace AppAdmin\Controllers\Factory;

use Silex\Application;

use AppAdmin\Service\PostService;
use AppAdmin\Controllers\PostsController;

class PostsControllerFactory
{

    /**
     * @var PostsController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new PostService($app['orm.em']);
        $this->controller = new PostsController($baseService);
    }

    public function getController()
    {
        return $this->controller;
    }
}