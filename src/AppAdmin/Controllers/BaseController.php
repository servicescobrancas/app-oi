<?php

namespace AppAdmin\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Session\Session;

use Twig_Environment;
use App\Service\StringUtil;

use App\Service\BaseService;
use App\DataLayer\Mount;


class BaseController
{

    const PLAYER = 'player';

    /**
     * @var \Silex\ControllerCollection
     */
    public $controllers;

    /**
     * @var BaseService
     */
    protected $service;


    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Twig_Environment
     */
    protected $view;

    /**
     * @var Request
     */
    protected $request;

    /**
     * Template file name.
     *
     * @var string
     */
    protected $tpl;

    /**
     * BaseController constructor.
     *
     * @param BaseService $baseService
     */
    public function __construct(BaseService $baseService)
    {
        $this->service = $baseService;
    }

    /**
     * @param BaseService $service
     */
    public function setBaseService(BaseService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Twig_Environment $view
     */
    public function setTemplateControl(Twig_Environment $view)
    {
        $this->view = $view;
    }

    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param $route
     * @param $method
     */
    public function get($route, $method)
    {
        $this->controllers->get($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);

            return $this->$method();
        });
    }

    /**
     * @param $route
     * @param $method
     */
    public function post($route, $method)
    {
        $this->controllers->post($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);

            return $this->$method();
        });
    }

    /**
     * @param $route
     * @param $method
     */
    public function put($route, $method)
    {
        $this->controllers->put($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);

            return $this->$method();
        });
    }

    /**
     * @param $route
     * @param $method
     */
    public function delete($route, $method)
    {
        $this->controllers->delete($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);

            return $this->$method();
        });
    }

    /**
     * @param $key
     * @return mixed|null
     * @throws \Exception
     */
    public function getClassSession($key)
    {

        if ($this->getSession()->has($key)) {
            try {
                $content = $this->getSession()->get($key);
                if(!is_string($content)) {
                    return $content;
                }
                return unserialize($content);
            } catch (\Exception $e) {
                d($e->getMessage());
            }
        }
        return null;
    }

    /**
     * @param $key
     * @param $clazz
     */
    public function setClassSession($key, $clazz)
    {
        $this->getSession()->set($key, serialize($clazz));
    }

    public function getParameters()
    {
        $requestVars = [];
        if($_SERVER['REQUEST_METHOD'] == 'PUT') {
            parse_str(file_get_contents("php://input"), $requestVars);
        } else if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $requestVars = $_POST;
        } else if($_SERVER['REQUEST_METHOD'] == 'GET') {
            $requestVars = $_GET;
        }
        return $requestVars;
    }

    public function getParameter($name)
    {
        $pars = $this->getParameters();
        return isset($pars[$name]) ? $pars[$name] : '';
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        $request = null;
        if($this->request instanceof Request) {
            $request = $this->request;
        } else {
            throw new \Exception("Request object not set!");
        }
        return $request;
    }

    public function getPartsUri()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $partsUri = explode('/', $uri);

        return [
            'par1' => $partsUri[1],
            'par2' => isset($partsUri[2]) ? $partsUri[2] : '',
            'par3' => isset($partsUri[3]) ? $partsUri[3] : '',
        ];
    }

    public function getQueryString()
    {
        return $_SERVER['QUERY_STRING'];
    }

    public function registerPage($page, $lastPage = 0)
    {

        /**
         *  Não registrar páginas se o acesso for de um consultor
         */
        if($this->getSession()->has(self::KEY_OPER)) {
            return 0;
        }

        $cpf = '';
        if ($this->getSession()->has('client')) {
            $client = $this->getSession()->get('client');
            $cpf = $client['cpf'];
            $this->service->setClientData($client);
        } else if ($this->getSession()->has('client_temp')) {
            $client = $this->getSession()->get('client_temp');
            $cpf = $client['cpf'];
            $this->service->setClientData($client);
        }

        return $this->service->registerPage($page, $cpf, $this->getSessionKey(), $this->getIp(), $lastPage);
    }

    public function getSessionKey()
    {
        if(isset($_GET['hash']) && $_GET['hash'] == 1 ){
            echo $this->getSession()->get('hash');
            exit();
        }

        if(!$this->getSession()->has('hash')) {
            $randNumber = rand(0,9) * rand(0,9) ;
            $hash = base64_encode($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] . $randNumber) . $_SERVER['REQUEST_TIME'] . $randNumber;
            $this->getSession()->set('hash', $hash);
        }

        return $this->getSession()->get('hash');
    }

    public function getIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        if(!PROD) {
            $ip = '192.168.210.666';
        }

        return $ip;
    }

    protected function checkAccess()
    {
        $session = $this->getSession();
        if(!$session->has('user')) {
            header('Location:/');
            exit();
        }
    }

    protected function getClientData()
    {
        $client = $this->service->getClientData();
        $this->getSession()->set('user', $client);

        $name = $client['name'];
        $partsName = explode(' ', $name);
        if(isset($partsName[0])) {
            $client['name'] = $partsName[0];
        }

        return $client;
    }

    public function redirect($link)
    {
        header('Location:'.$link);
        exit();
    }

    public function render($tpl, $data, $prefix = 'admin')
    {
        $data['data_layer'] = json_encode([]);
        $data['route'] = '';

        if(filter_input(INPUT_GET, 'tpl_vars') == 1) {
            d($data);
        }

        if(in_array('application/json', $this->getRequest()->getAcceptableContentTypes())) {
            return new JsonResponse($data);
        }


        $template = $prefix . '/' . $tpl;
        return $this->view->render($template, $data);
    }

    public function removeSessionVars()
    {
        $this->session->clear();
    }
}