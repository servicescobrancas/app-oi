<?php

namespace AppAdmin\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppAdmin\Service\AccessService;

class LoginController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var AccessService
     */
    public $service;

    /**
     * AccessController constructor.
     * @param AccessService $baseService
     */
    public function __construct(AccessService $baseService)
    {
        $this->service = $baseService;
    }

    /**
     * @param Application $app
     * @return mixed|\Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];
        $this->get('/', 'index');
        $this->post('/access', 'access');

        $this->get('/logoff', 'logoff');

        return $this->controllers;
    }

    /**
     * @return string
     */
    public function index()
    {
        $data = [];
        return $this->render('login/index.twig', $data);
    }

    /**
     * @return JsonResponse
     */
    public function access()
    {
        $pars = $this->getParameters();
        $isRegister = $this->service->checkAccess($pars);
        if ($isRegister) {
            $this->getSession()->set('user', $pars['username']);
            $this->getSession()->set('logon', true);
        }

        $data = [
            'status' => $isRegister,
            'message' => $this->service->getMessage(),
            'redirect_url' => '/admin/home'
        ];

        return new JsonResponse($data);
    }
}