<?php

namespace AppAdmin\Controllers;

use GuzzleHttp\Psr7\Request;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppAdmin\Service\ApiService;


class HomeController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var ApiService
     */
    public $service;

    /**
     * AccessController constructor.
     * @param ApiService $baseService
     */
    public function __construct(ApiService $baseService)
    {
        $this->service = $baseService;
    }

    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        // Routes
        $this->get('/', 'index');

        $this->post('/change-graph', 'changeViewGraph');

        return $this->controllers;
    }

    public function index()
    {
        $this->checkAccess();

        $data = [];
        $data['data_padrao'] = date('m/Y');

        $metodos = $this->service->listMethods();
        $data['metodos'] = $metodos;

        $tpl = 'home/index.twig';
        return $this->render($tpl, $data);
    }

    public function changeViewGraph()
    {
        $post = $this->getParameters();
        if ($post['visao'] == 'mensal') {
            if (!(int)strlen($post['data'])) {
                $post['data'] = date('m/Y');
            }

            $_data_graf = implode('-', array_reverse(explode('/', $post['data'])))."-01";
            $data_ref = new \DateTime($_data_graf);
            $ultimo_dia = date("t", mktime(0,0,0,$data_ref->format('m'),'01',$data_ref->format('Y')));

            $dados = $this->service->dataPerMonth(array('data1' => $data_ref->format('Y-m-d'), 'data2' => $data_ref->format('Y-m')."-".$ultimo_dia, 'metodo' => $post['metodo']));
            $_arr_valores = array();
            if ($dados) {
                foreach ($dados as $dado) {
                    $_arr_valores[$dado['data_base']] =array( 'duracao' => $dado['duracao'], 'contagem' => $dado['num']);
                }
            }

            $dados_graf = array();

            for ($i = 1; $i <= $ultimo_dia; $i++) {
                $_arr_dados = array();
                $duracao = isset($_arr_valores[$data_ref->format('Y-m-d')]) ? $_arr_valores[$data_ref->format('Y-m-d')]['duracao'] : 0;
                $contagem = isset($_arr_valores[$data_ref->format('Y-m-d')]) ? $_arr_valores[$data_ref->format('Y-m-d')]['contagem'] : 0;

                $_arr_dados['dia'] = $data_ref->format('d');
                $_arr_dados['duracao'] = $duracao;
                $_arr_dados['contagem'] = $contagem;
                $_arr_dados['media'] = $duracao > 0 ? $duracao/$contagem : 0;
                $dados_graf[] = $_arr_dados;

                $data_ref->modify("+1 days");
            }


            $data_graf_methods = date('Y-m', strtotime($_data_graf));
            $contagem_total = $this->service->retornaNumRequisicoes($data_graf_methods);
            foreach ($contagem_total as $idx => $contagem) {
                $contagem_total[$idx]['requisicoes_subtotal'] = $contagem['requisicoes'] - $contagem['erros'];
            }

            $data['grafico_metodos'] = $contagem_total;
            $data['grafico_tempo'] = $dados_graf;
            $data['tempo'] = " mês [".date('m/Y', strtotime($_data_graf))."]";

            return new JsonResponse($data);
        } else {
            if (!(int)strlen($post['data'])) {
                $post['data'] = date('d/m/Y');
            }

            $_data_graf = implode('-', array_reverse(explode('/', $post['data'])));
            $data_ref = new \DateTime("00:00:00");

            $dados = $this->service->dataPerDay($_data_graf, $post['metodo']);
            $_arr_valores = array();
            if ($dados) {
                foreach ($dados as $dado) {
                    $_arr_valores[$dado['data_base']] = array( 'duracao' => $dado['duracao'], 'contagem' => $dado['num']);
                }
            }

            $dados_graf = array();

            for ($i = 0; $i <= 23; $i++) {
                $_arr_dados = array();
                $duracao = isset($_arr_valores[$data_ref->format('H')]) ? $_arr_valores[$data_ref->format('H')]['duracao'] : 0;
                $contagem = isset($_arr_valores[$data_ref->format('H')]) ? $_arr_valores[$data_ref->format('H')]['contagem'] : 0;

                $_arr_dados['dia'] = $data_ref->format('H:i');
                $_arr_dados['duracao'] = $duracao;
                $_arr_dados['contagem'] = $contagem;
                $_arr_dados['media'] = $contagem > 0 ? $duracao/$contagem : 0;
                $dados_graf[] = $_arr_dados;

                $data_ref->modify("+1 hours");
            }

            $data_graf_methods = date('Y-m-d', strtotime($_data_graf));
            $contagem_total = $this->service->retornaNumRequisicoes($data_graf_methods);
            foreach ($contagem_total as $idx => $contagem) {
                $contagem_total[$idx]['requisicoes_subtotal'] = $contagem['requisicoes'] - $contagem['erros'];
            }
            $data['grafico_metodos'] = $contagem_total;
            $data['grafico_tempo'] = $dados_graf;
            $data['tempo'] = " dia [".date('d/m/Y', strtotime($_data_graf))."]";

            return new JsonResponse($data);
        }
    }
}
