<?php

namespace AppAdmin\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppAdmin\Service\AccessService;
use App\Storage\Elastic;
use AppAdmin\Service\PostService;

class PostsController extends BaseController implements ControllerProviderInterface
{

    const INDEX = 'vivo';

    /**
     * @var PostService
     */
    public $service;

    /**
     * @var array
     */
    public $pars;

    /**
     * AccessController constructor.
     * @param PostService $baseService
     */
    public function __construct(PostService $baseService)
    {
        $this->service = $baseService;
    }

    /**
     * @param Application $app
     * @return mixed|\Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');

        $this->get('/new', 'newItem');
        $this->post('/new', 'newItem');

        $this->get('/edit/{id}', 'editItem');
        $this->post('/edit/{id}', 'editItem');

        $this->get('/create', 'create');
        $this->get('/search', 'search');

        return $this->controllers;
    }

    /**
     * @todo - refactoring
     *
     * @return string
     */
    public function index()
    {
        $data = [];

        $pars = [
            //'title' => '136'
        ];

        $from = 0;
        $size = 10;

        $order = [
            ['created' => ['order' => 'desc']],
        ];

        $data['list'] = $this->service->listItems($pars, $from, $size, $order);

        return $this->render('posts/index.twig', $data);
    }

    /**
     * @return string
     */
    public function newItem()
    {
        if ($this->isSendData() && $this->validateNewItem()) {
            $status = $this->service->create($this->pars);
            $this->getFlashMessenger()->add($this->service->getMessages());
            if ($status) {
                $this->redirect('/admin/posts');
            }
        }

        $data = [];
        return $this->render('posts/form.twig', $data);
    }

    public function validateNewItem()
    {
        $this->pars = $this->getParameters();

        // @todo add validation

        return true;
    }

    /**
     * @return string
     */
    public function editItem()
    {
        $data = [];
        if ($this->isSendData() && $this->validateEditItem()) {
            $status = $this->service->update($this->pars);

            $this->getFlashMessenger()->add($this->service->getMessages());
            if ($status) {
                $this->redirect('/admin/posts');
            }

            $data['data_form'] = $this->pars;

        } else {
//            if ($this->getSession()->has('post_data')) {
//                $data['data_form'] = $this->getSession()->get('post_data');
//                $this->getSession()->remove('post_data');
//            } else {
//                $id = $this->getRequest()->get('id');
//                $data['data_form'] = $this->service->getItemById($id);
//            }
            $id = $this->getRequest()->get('id');
            $data['data_form'] = $this->service->getItemById($id);

            if (count( $data['data_form']) == 0) {
                $this->redirect('/admin/posts');
            }
        }

        return $this->render('posts/form.twig', $data);
    }

    public function validateEditItem()
    {
        $pars = $this->getParameters();
        $this->pars = $this->service->validateUpdate($pars);

        $this->pars['thumb'] = ''; // upload
        $this->pars['image'] = ''; // upload

        $this->getSession()->set('post_data', $this->pars );
        $this->getFlashMessenger()->add($this->service->getMessages());

        return $this->service->getStatus();
    }

    public function search()
    {
        $elastic = new Elastic(self::INDEX);
        $elastic->setType('posts');
        $elastic->setSize(100);

        $query = $elastic->getSearchMatch([
            'title' => '136'
        ]);

        $response = $elastic->find($query);

        if (isset($matchData['hits']['hits'])) {
            foreach ($matchData['hits']['hits'] as $key => $hit) {
                echo $hit['_source']['name'].'<br/>';
            }
        }


        d($response);
    }

    /**
     * @return JsonResponse
     */
    public function access()
    {
        $pars = $this->getParameters();
        $isRegister = true;

        $data = [
            'status' => $isRegister,
            'message' => $this->service->getMessage()
        ];

        return new JsonResponse($data);
    }
}