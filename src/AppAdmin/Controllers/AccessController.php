<?php

namespace AppAdmin\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class AccessController extends BaseController implements ControllerProviderInterface
{

    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];


        $this->get('/logoff', 'logoff');

        return $this->controllers;
    }



    /**
     * @return void
     */
    public function logoff()
    {
        $this->removeSessionVars();
        $this->redirect('/');
    }
}