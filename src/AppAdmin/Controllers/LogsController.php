<?php

namespace AppAdmin\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppAdmin\Service\LogsService;
use AppAdmin\Service\ApiService;

class LogsController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var LogsService
     */
    public $service;


    /**
     * @var ApiService
     */
    public $apiService;

    /**
     * @var array
     */
    public $pars;

    /**
     * LogsController constructor.
     * @param LogsService $baseService
     */
    public function __construct(LogsService $baseService)
    {
        $this->service = $baseService;
    }

    public function setApiService(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @param Application $app
     * @return mixed|\Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');

        $this->get('/new', 'newItem');
        $this->post('/new', 'newItem');

        $this->get('/edit/{id}', 'editItem');
        $this->post('/edit/{id}', 'editItem');

        $this->get('/create', 'create');
        $this->get('/search', 'search');

        return $this->controllers;
    }

    /**
     * @todo - refactoring
     *
     * @return string
     */
    public function index()
    {
        $this->checkAccess();

        $data = [];
        $data['data_padrao'] = date('m/Y');

        $doc = $this->getParameter('doc');
        $pars = [
            'doc' => $doc
        ];

        $logs = $this->service->lastLogs($pars);
        $methods = $this->apiService->listMethods();

        $data = [
            'logs' => $logs,
            'methods' => $methods
        ];

        $tpl = 'logs/index.twig';
        return $this->render($tpl, $data);
    }




}