<?php

namespace App\DataLayer;

use App\Service\StringUtil;
use App\Math\Convert;

Use App\Entities\Client;

class Mount
{
    private $data = [];

    public function __construct(Client $clientData, $debtorData)
    {
        $this->managePersonalData($clientData);
        $this->manageInvoices($debtorData);
    }

    private function managePersonalData(Client $clientData)
    {
        $this->data['name'] = $clientData->getCompleteName();
        $cpf = $clientData->getCpf();
        $cpfEncode = StringUtil::encodeCpf($cpf);
        $this->data['doc'] = $cpfEncode;
    }

    private function manageInvoices($debtorData)
    {
        $contracts = isset($debtorData['contracts']) ? $debtorData['contracts'] : [];

        $totalValue = 0;
        $totalSelected = 0;

        $contractsFilter = [];
        foreach($contracts as $key => $contractInfo) {
            $contractsFilter[$key]['number'] = $contractInfo['number'];
            $contractsFilter[$key]['invoices'] = [];

            foreach($contractInfo['invoices'] as $keyInvoice => $invoiceData) {

                $value = Convert::formatNumberToFloat($invoiceData['value']);
                if($invoiceData["discount"] != 'R$ 0,00') {
                    $value =  Convert::formatNumberToFloat($invoiceData['discount']);
                }

                $invoiceFilter = [
                    'value' => $value,
                    'boleto_id' => $invoiceData['boleto_id'],
                    'due_date' => $invoiceData['due_date'],
                    'selected' => $invoiceData['selected']
                ];

                $totalValue += $value;
                if($invoiceData['selected']) {
                    $totalSelected  += $value;
                }

                $contractsFilter[$key]['invoices'][] = $invoiceFilter;
            }
        }

        $this->data['contracts'] = $contractsFilter;
        $this->data['total_value'] = $totalValue;
        $this->data['total_value_selected'] = $totalSelected;
    }

    public function getFormatData()
    {
        if(isset($_GET['view_data_layer']) && $_GET['view_data_layer']) {
            echo 'DATA LAYER:<br/>';
            d($this->data);
        }

        return json_encode($this->data);
    }
}