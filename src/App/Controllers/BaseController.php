<?php

namespace App\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Session\Session;
use Twig_Environment;

use App\Service\BaseService;
use App\DataLayer\Mount;

use App\Storage\StorageClass;
use App\Entities\Client;

class BaseController
{

    const KEY_DEBTOR = 'debtor';
    const KEY_DEBTOR_PARCEL = 'debtor_parcel';
    const KEY_CONTRACT = 'contract';

    const KEY_CLIENT = 'client_data';

    const KEY_RECEPTIVE = 'receptive';

    /**
     * @var \Silex\ControllerCollection
     */
    public $controllers;

    /**
     * @var BaseService
     */
    protected $service;


    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Twig_Environment
     */
    protected $view;

    /**
     * @var Request
     */
    protected $request;

    /**
     * BaseController constructor.
     *
     * @param BaseService $baseService
     */
    public function __construct(BaseService $baseService)
    {
        $this->service = $baseService;
    }

    /**
     * @param BaseService $service
     */
    public function setBaseService(BaseService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Twig_Environment $view
     */
    public function setTemplateControl(Twig_Environment $view)
    {
        $this->view = $view;
    }

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session;

        $client = $this->getClassInSession(self::KEY_CLIENT);

        if($client != null && !is_array($client)) {
            $this->service->setClientData($client);
        }

        if(!$this->getSession()->has('hash')) {
            $randNumber = rand(0,9) * rand(0,9) ;
            $hash = base64_encode($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] . $randNumber) . $_SERVER['REQUEST_TIME'] . $randNumber;
            $this->getSession()->set('hash', $hash);
        }
    }


    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param $route
     * @param $method
     */
    public function get($route, $method)
    {
        $this->controllers->get($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);
            $this->loadClientData();

            return $this->$method();
        });
    }

    /**
     * @param $route
     * @param $method
     */
    public function post($route, $method)
    {
        $this->controllers->post($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);
            $this->loadClientData();

            return $this->$method();
        });
    }

    /**
     * @param $route
     * @param $method
     */
    public function put($route, $method)
    {
        $this->controllers->put($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);
            $this->loadClientData();

            return $this->$method();
        });
    }

    /**
     * @param $route
     * @param $method
     */
    public function delete($route, $method)
    {
        $this->controllers->delete($route, function(Application $app, Request $request) use ($method) {
            $this->setRequest($request);
            $this->setTemplateControl($app['twig']);
            $this->setSession($app['session']);
            $this->loadClientData();

            return $this->$method();
        });
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        $requestVars = [];
        if($_SERVER['REQUEST_METHOD'] == 'PUT') {
            parse_str(file_get_contents("php://input"), $requestVars);
        } else if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $requestVars = $_POST;
        } else if($_SERVER['REQUEST_METHOD'] == 'GET') {
            $requestVars = $_GET;
        }
        return $requestVars;
    }

    /**
     * @param $name
     * @return string
     */
    public function getParameter($name)
    {
        $pars = $this->getParameters();
        return isset($pars[$name]) ? $pars[$name] : '';
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return null|Request
     * @throws \Exception
     */
    public function getRequest()
    {
        $request = null;
        if($this->request instanceof Request) {
            $request = $this->request;
        } else {
            throw new \Exception("Request object not set!");
        }
        return $request;
    }

    /**
     * @return array
     */
    public function getPartsUri()
    {
        $uri = filter_input(INPUT_SERVER, 'REQUEST_URI');
        $partsUri = explode('/', $uri);

        return [
            'par1' => $partsUri[1],
            'par2' => isset($partsUri[2]) ? $partsUri[2] : '',
            'par3' => isset($partsUri[3]) ? $partsUri[3] : '',
        ];
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        return $_SERVER['QUERY_STRING'];
    }

    /**
     * @param $key
     * @return mixed|null
     * @throws \Exception
     */
    public function getClassInSession($key)
    {

        if ($this->getSession()->has($key)) {
            try {
                $content = $this->getSession()->get($key);
                if(!is_string($content)) {
                    return $content;
                }
                return unserialize($content);
            } catch (\Exception $e) {
                d($e->getMessage());
            }
        }

        return null;
    }

    /**
     * @param $key
     * @param $clazz
     */
    public function setClassInSession($key, $clazz)
    {
        $this->addRegister($key, $clazz);
        $this->getSession()->set($key, serialize($clazz));
    }

    /**
     * @param $key
     * @param $object
     * @return bool
     */
    public function addRegister($key, $object)
    {

        $storage = new StorageClass();
        return $storage->store($key, $object);
    }

    /**
     * @param $page
     * @param int $lastPage
     * @param string $cpf
     * @return int
     */
    public function registerPage($page, $lastPage = 0, $cpf = '')
    {
        /**
         *  Não registrar páginas se o acesso for de um consultor
         */


        $clientData = $this->getSession()->get('client_data');
        return $this->service->registerPage($page, $clientData, $this->getSessionKey(), $this->getIp(), $lastPage);
    }

    /**
     * @return mixed
     */
    public function getSessionKey()
    {
        if(isset($_GET['hash']) && $_GET['hash'] == 1 ){
            echo $this->getSession()->get('hash');
            exit();
        }

        if(!$this->getSession()->has('hash')) {
            $randNumber = rand(0,9) * rand(0,9) ;
            $hash = base64_encode($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] . $randNumber) . $_SERVER['REQUEST_TIME'] . $randNumber;
            $this->getSession()->set('hash', $hash);
        }

        return $this->getSession()->get('hash');
    }

    /**
     * @return string
     */
    public function getIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        if(!PROD) {
            $ip = '192.168.210.666';
        }

        return $ip;
    }

    /**
     * @return void
     */
    protected function checkAccess()
    {
        $session = $this->getSession();
        if(!$session->has(self::KEY_CLIENT)) {
            $this->registerPage('Acesso não autorizado.');
            $this->redirect('/');
        } else {
            $clientData = $session->get(self::KEY_CLIENT);
            $uri = filter_input(INPUT_SERVER, 'REQUEST_URI');
            if (!isset($clientData['DocumentoConsumidor']) && !strstr($uri, '/contact/')) {
                $this->redirect('/contact');
            }
        }
    }

    protected function loadClientData()
    {
        $clientData = $this->getSession()->get('client_data');
        $this->service->setClientData($clientData);
    }

    protected function getClientData()
    {
        $client = $this->service->getClientData();
        $this->getSession()->set('client', $client);

        $name = $client['name'];
        $partsName = explode(' ', $name);
        if(isset($partsName[0])) {
            $client['name'] = $partsName[0];
        }

        return $client;
    }

    public function redirect($url, $go = true)
    {
        if($go) {
            header('Location:' . $url);
            exit();
        }

        echo 'redirect for : ' . $url;
        exit();
    }

    public function render($tpl, $data = [], $prefix = 'site')
    {
        $data['data_layer'] = '{}';

        $clientData = $this->getClassInSession(self::KEY_CLIENT);

        if($clientData != null) {
            //$data['data_layer'] = (new Mount($clientData, $debtorData))->getFormatData();
        }

        $data['consultant']= '';
        $data['logon'] = ($clientData != null);
        $data['contact_menu'] = $this->session->get('link_contact');

        $data['gtm_code'] = GTM_NORMAL;

        $data['style_random'] = $this->getSession()->get('style_rand');

        $template = $prefix . '/' . $tpl;
        return $this->view->render($template, $data);
    }

    public function display()
    {
        echo get_class($this);
        echo '<br/>';
    }

    public function removeSessionVars(Session $session)
    {
        $session->remove(self::KEY_CONTRACT);
        $session->remove(self::KEY_DEBTOR);
        $session->remove(self::KEY_CLIENT);
        $session->remove(self::KEY_RECEPTIVE);
        $session->remove('client_data');
        $session->remove('user_data');

        $session->clear();
    }

}