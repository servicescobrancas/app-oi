<?php

namespace App\Controllers;

use Silex\Application;

use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\ChatService;
use App\Service\EnterService;
use App\Service\AccessService;

class ChatController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var ChatService
     */
    private $chatService;

    /**
     * @var EnterService
     */
    private $serviceEnter;

    /**
     * @var AccessService
     */
    private $accessService;

    /**
     * @var AccessService
     */
    private $serviceAccess;

    /**
     * ChatController constructor.
     * @param ChatService $chatService
     */
    public function __construct(ChatService $chatService)
    {
        $this->chatService = $chatService;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');
        $this->post('/send-message', 'sendMessageAndGetAnswer');
        $this->post('/open-conversation', 'openConversation');

        $this->post('/login', 'login');
        $this->post('/select-cpf', 'selectCpf');

        return $this->controllers;
    }

    /**
     * @param EnterService $serviceEnter
     */
    public function setServiceEnter(EnterService $serviceEnter)
    {
        $this->serviceEnter = $serviceEnter;
    }

    /**
     * @param AccessService $accessService
     */
    public function setServiceAccess(AccessService $accessService)
    {
        $this->accessService = $accessService;
    }

    /**
     * @return string
     */
    public function index()
    {
        $data = [];
        return $this->render('chat/page.twig', $data);
    }

    public function openConversation()
    {
        $pars = $this->getParameters();
        $pars['ip'] = $this->getRequest()->getClientIp();
        $status = $this->chatService->processOpenQuestion($pars);

        $data = [
            'status' => $status,
            'id' => $this->chatService->getId(),
            'messages' => $this->chatService->getMessages(),
            'token' => $this->chatService->getToken()
        ];
        return new JsonResponse($data);
    }

    public function sendMessageAndGetAnswer()
    {
        $pars = $this->getParameters();
        $this->chatService->processAnwser($pars);

        $data = [
            'status' => $this->chatService->getStatus(),
            'id' => $this->chatService->getId(),
            'messages' => $this->chatService->getMessages()
        ];

        return new JsonResponse($data);
    }

    public function registerPageView()
    {
        $data = [
            'status' => true,
            'message' => ''
        ];
        return new JsonResponse($data);
    }

    public function start()
    {
        return $this->render('info/start.twig', []);
    }

    public function selectCpf()
    {

        $data = [
            'status' => true,
            'message' => ''
        ];
        return new JsonResponse($data);
    }
}
