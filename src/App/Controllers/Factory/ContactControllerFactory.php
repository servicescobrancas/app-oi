<?php

namespace App\Controllers\Factory;

use Silex\Application;

use App\Service\ContactService;
use App\Controllers\ContactController;

class ContactControllerFactory
{

    /**
     * @var ContactController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $contactService = new ContactService($app['orm.em']);
        $this->controller = new ContactController($contactService);
    }

    public function getController()
    {
        return $this->controller;
    }
}