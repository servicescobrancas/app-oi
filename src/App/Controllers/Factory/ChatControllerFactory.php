<?php

namespace App\Controllers\Factory;

use Silex\Application;

use App\Service\PayInfoService;
use App\Service\EnterService;
use App\Service\AccessService;
use App\Service\ChatService;
use App\Controllers\ChatController;

class ChatControllerFactory
{

    /**
     * @var ChatController
     */
    private $controller;

    /**
     * ChatControllerFactory constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $payInfoService = new PayInfoService($app['orm.em']);
        $serviceEnter = new EnterService($app['orm.em']);
        $accessService = new AccessService($app['orm.em']);
        $chatService = new ChatService($app['orm.em']);

        $this->controller = new ChatController($chatService);
        $this->controller->setServiceEnter($serviceEnter);
        $this->controller->setServiceAccess($accessService);
    }

    /**
     * @return ChatController
     */
    public function getController()
    {
        return $this->controller;
    }
}