<?php

namespace App\Controllers\Factory;

use Silex\Application;

use App\Service\AccessService;
use App\Controllers\ReceptiveController;

class ReceptiveControllerFactory
{

    /**
     * @var ReceptiveController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new AccessService($app['orm.em']);
        $this->controller = new ReceptiveController($baseService);
    }

    public function getController()
    {
        return $this->controller;
    }
}