<?php

namespace App\Controllers\Factory;

use Silex\Application;

use App\Service\PayInfoService;
use App\Service\EnterService;
use App\Service\AccessService;
use App\Controllers\InfoController;

class InfoControllerFactory
{

    /**
     * @var InfoController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $payInfoService = new PayInfoService($app['orm.em']);
        $serviceEnter = new EnterService($app['orm.em']);
        $accessService = new AccessService($app['orm.em']);

        $this->controller = new InfoController($payInfoService);
        $this->controller->setServiceEnter($serviceEnter);
        $this->controller->setServiceAccess($accessService);
    }

    public function getController()
    {
        return $this->controller;
    }
}