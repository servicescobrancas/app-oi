<?php

namespace App\Controllers\Factory;

use Silex\Application;

use App\Service\PayInfoService;
use App\Controllers\PayInfoController;

class PayInfoControllerFactory
{

    /**
     * @var PayInfoController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $payInfoService = new PayInfoService($app['orm.em']);
        $this->controller = new PayInfoController($payInfoService);
    }

    public function getController()
    {
        return $this->controller;
    }
}