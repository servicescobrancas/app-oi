<?php

namespace App\Controllers\Factory;

use Silex\Application;

use App\Service\EnterService;
use App\Service\AccessService;
use App\Controllers\EnterController;

class EnterControllerFactory
{

    /**
     * @var EnterController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new EnterService($app['orm.em']);
        $accessService = new AccessService($app['orm.em']);
        $this->controller = new EnterController($baseService);
        $this->controller->setServiceAccess($accessService);
    }

    public function getController()
    {
        return $this->controller;
    }
}