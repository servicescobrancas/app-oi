<?php

namespace App\Controllers\Factory;

use Silex\Application;

use App\Service\BoletoService;
use App\Controllers\BoletoController;

class BoletoControllerFactory
{

    /**
     * @var BoletoController
     */
    private $controller;

    public function __construct(Application $app)
    {
        $baseService = new BoletoService($app['orm.em']);
        $this->controller = new BoletoController($baseService);
    }

    public function getController()
    {
        return $this->controller;
    }
}