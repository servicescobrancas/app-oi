<?php

namespace App\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\AccessService;

class ReceptiveController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var AccessService
     */
    public $service;

    /**
     * AccessController constructor.
     * @param AccessService $baseService
     */
    public function __construct(AccessService $baseService)
    {
        $this->service = $baseService;
    }

    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('', 'index');
        $this->post('/send-data', 'sendData');

        return $this->controllers;
    }

    public function index()
    {
        $this->activeReceptive();
        $firstPage = $this->registerPage('Tela de Entrada - Receptivo');

        $session = $this->getSession();
        $session->set('firstPage', $firstPage);

        $data = [];
        return $this->render('access/index.twig', $data);
    }
}