<?php

namespace App\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends BaseController implements ControllerProviderInterface
{

    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');
        $this->get('/download-boleto', 'downloadService');
        $this->get('/email-boleto', 'emailService');
        $this->post('/email-boleto', 'emailService');
        $this->get('/sms-boleto', 'smsService');

        return $this->controllers;
    }

    public function index()
    {
        return "index api";
    }

    public function downloadService()
    {
        $queryString = $this->getQueryString();
        $data = $this->service->download($queryString);
        $this->registerPage('Baixar PDF Boleto Serviço');

        $file = $data['Arquivo'];

        $fileName = trim($data['Contrato']) . '.pdf';

        header("Content-Disposition: attachment; filename=" . urlencode($fileName));
        header("Content-Type: application/pdf");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file));

        $fp = fopen($file, "r");
        while (!feof($fp)) {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }
        fclose($fp);

        // Rotinas para download do PDF
    }

    public function smsService()
    {
        $this->registerPage('SMS Boleto Serviço');

        $queryString = $this->getQueryString();
        $data = $this->service->sms($queryString);
        return new JsonResponse($data);
    }

    public function emailService()
    {
        $this->registerPage('E-mail Boleto Serviço');

        $queryString = $this->getQueryString();
        $data = $this->service->email($queryString);
        return new JsonResponse($data);
    }
}