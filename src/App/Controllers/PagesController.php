<?php

namespace App\Controllers;

use Silex\Application;

use Silex\Api\ControllerProviderInterface;

class PagesController extends BaseController implements ControllerProviderInterface
{

    private $pagesData = [
        '' => '',
    ];

    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');

        return $this->controllers;
    }

    public function indexPages()
    {
        $data = [
            'title' => 'A Service Digital'
        ];

        $tpl = 'pages/index.twig';
        return $this->render($tpl, $data);
    }

    public function segurancaConfiabilidade()
    {
        $data = [
            'title' => 'Segurança e Confiabilidade'
        ];

        $tpl = 'pages/segurancao-confiabilidade.twig';
        return $this->render($tpl, $data);
    }

    public function garantiaProfissionaisQualificados()
    {
        $data = [
            'title' => 'Garantia de profissionais qualificados'
        ];

        $tpl = 'pages/garantia-de-profissionais-qualificados.twig';
        return $this->render($tpl, $data);
    }

    public function educacaoDigital()
    {
        $data = [
            'title' => 'Educação digital'
        ];

        $tpl = 'pages/educacao-digital.twig';
        return $this->render($tpl, $data);
    }


    public function educacaoDigitalVivo()
    {
        $data = [
            'title' => 'Educação Digital Vivo'
        ];

        $tpl = 'pages/educacao-digital-vivo.twig';
        return $this->render($tpl, $data);
    }

    public function consumidoresTrocamCanaisVozServicosAtendimentoDigital()
    {
        $data = [
            'title' => 'Consumidores trocam canais de voz por serviços de atendimento digital'
        ];

        $tpl = 'pages/consumidores-trocam-canais-de-voz-por.twig';
        return $this->render($tpl, $data);
    }


    public function crescimentoServicosOnlineProjecaoFuturo()
    {
        $data = [
            'title' => 'Crescimento dos serviços online e projeção para o futuro'
        ];

        $tpl = 'pages/crescimento-dos-servicos-online-projecao.twig';
        return $this->render($tpl, $data);
    }
}
