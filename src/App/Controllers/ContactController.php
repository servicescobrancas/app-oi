<?php

namespace App\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\ContactService;

class ContactController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var ContactService
     */
    protected $service;

    /**
     * ContactController constructor.
     * @param ContactService $contactService
     */
    public function __construct(ContactService $contactService)
    {
        $this->service = $contactService;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');
        $this->get('/not-found', 'notFound');

        $this->get('/whatsapp/{channel}', 'whatsapp');
        $this->get('/sms/{channel}', 'sms');
        $this->get('/facebook/{channel}', 'facebook');
        $this->get('/chat/{channel}', 'chat');
        $this->get('/telegram/{channel}', 'telegram');
        $this->get('/ligacao/{channel}', 'call');
        $this->get('/email/{channel}', 'email');

        $this->post('/send', 'sendData');
        $this->get('/return-message', 'returnMessage');

        return $this->controllers;
    }

    public function index()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $dataContact = $this->service->defineContact($clientData);

        if (count($dataContact) > 0) {
            $session->set('channel', $dataContact);
        } else {
            $session->set('channel', []);
        }

        $data = [
            'client' => $clientData,
            'dataContact' => $dataContact
        ];
        $tpl = 'contact/index.twig';
        return $this->render($tpl, $data);
    }

    public function notFound()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $data = [
            'client' => $clientData
        ];
        $tpl = 'contact/not-found.twig';
        return $this->render($tpl, $data);
    }

    public function whatsapp()
    {
        $this->checkAccess();
        $channel = $this->getRequest()->get('channel');

        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $data = [
            'client' => $clientData,
            'channel' => $channel
        ];

        $tpl = 'contact/whatsapp.twig';
        return $this->render($tpl, $data);
    }

    public function sms()
    {
        $this->checkAccess();
        $channel = $this->getRequest()->get('channel');
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $data = [
            'client' => $clientData,
            'channel' => $channel
        ];
        $tpl = 'contact/sms.twig';
        return $this->render($tpl, $data);
    }

    public function facebook()
    {
        $this->checkAccess();
        $channel = $this->getRequest()->get('channel');
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $dataContact = $session->get('channel');

        $pars = [
            'type' => 'facebook',
            'channel' => 4
        ];
        $this->service->sendContact($dataContact, $pars);

        $this->redirect('https://www.messenger.com/t/servicesdigital11/');
    }

    public function chat()
    {
        $this->checkAccess();
        $channel = $this->getRequest()->get('channel');
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $data = [
            'client' => $clientData,
            'channel' => $channel,
            'cpf' => isset($clientData['DocumentoConsumidor']) ? $clientData['DocumentoConsumidor'] : $clientData['cpf']
        ];
        $tpl = 'contact/chat.twig';
        return $this->render($tpl, $data);
    }

    public function chatOld()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $dataContact = $session->get('channel');

        $pars = [
            'type' => 'chat',
            'channel' => 4
        ];
        $this->service->sendContact($dataContact, $pars);

        $urlChat = 'http://srvcloo2.callflex.com.br/omne/virtual/chat/';
        $urlChat .= (isset($clientData['DocumentoConsumidor']) ? $clientData['DocumentoConsumidor'] : $clientData['cpf']) . '/';
        $urlChat .= $this->service->getPool();

        $this->redirect($urlChat);
    }

    public function telegram()
    {
        $this->checkAccess();
        $channel = $this->getRequest()->get('channel');
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $data = [
            'client' => $clientData,
            'channel' => $channel
        ];
        $tpl = 'contact/telegram.twig';
        return $this->render($tpl, $data);
    }

    public function call()
    {
        $this->checkAccess();
        $channel = $this->getRequest()->get('channel');
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $data = [
            'client' => $clientData,
            'channel' => $channel
        ];
        $tpl = 'contact/call.twig';
        return $this->render($tpl, $data);
    }

    public function email()
    {
        $this->checkAccess();
        $channel = $this->getRequest()->get('channel');
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $data = [
            'client' => $clientData,
            'channel' => $channel
        ];
        $tpl = 'contact/email.twig';
        return $this->render($tpl, $data);
    }

    public function getChannel()
    {
        $session = $this->getSession();
        $channelId = $this->getRequest()->get('channel_id');
        $channelData = $session->get('channel');

        return $channelData['canais_liberados'][$channelId];
    }

    public function sendData()
    {
        $session = $this->getSession();
        $pars = $this->getParameters();

        $clientData = $session->get(self::KEY_CLIENT);
        if (isset($pars['name'])) {
            $clientData['name'] = $pars['name'];
        }
        if (isset($pars['phone'])) {
            $clientData['phone'] = $pars['phone'];
        }
        if (isset($pars['email'])) {
            $clientData['email'] = $pars['email'];
        }
        $dataContact = $this->service->defineContact($clientData);
        $this->service->sendContact($dataContact, $pars);
        $message = $this->service->getMessage();
        $session->set('message', $message);

        $pars = [
            'status' => true,
            'channel' => $pars['channel']
        ];
        return new JsonResponse($pars);
    }

    public function returnMessage()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $data = [
            'client' => $clientData,
            'message' => $session->get('message')
        ];
        $tpl = 'contact/return-message.twig';
        return $this->render($tpl, $data);
    }
}
