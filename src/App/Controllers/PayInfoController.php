<?php

namespace App\Controllers;

use Silex\Application;

use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\PayInfoService;

class PayInfoController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var PayInfoService
     */
    protected $service;

    /**
     * PayInfoController constructor.
     * @param PayInfoService $service
     */
    public function __construct(PayInfoService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/start-deal/{contract}/{date}', 'startDeal');
        $this->post('/reload-deals', 'reloadDeals');

        $this->get('/deal-invoices/{contract}', 'dealInvoices');
        $this->get('/deal-invoices/{contract}/{date}', 'dealInvoices');

        $this->get('/invoices/{contract}', 'invoices');
        $this->get('/invoices/{contract}/{date}', 'invoices');

        $this->get('/invoices-letter/{contract}', 'invoicesLetter');
        $this->get('/invoices-letter/{contract}/{date}', 'invoicesLetter');

        $this->get('/confirm-date/{contract}', 'confirmDate');
        $this->post('/register-date', 'registerDate');
        $this->get('/return-register-date', 'returnRegisterDate');

        $this->get('/confirm-deal/{contract}/1', 'confirmDeal');
        $this->get('/confirm-deal/{contract}/{option}', 'confirmDeal');

        $this->get('/confirm-deal-campaign/{contract}/{deal}/{option}', 'confirmDealCampaign');

        $this->get('/boleto-nao-emitido', 'BoletoNaoEmitido');

        $this->get('/view-boleto/{contract}', 'viewBoleto');
        $this->get('/select-boletos/{contract}', 'selectBoletos');
        $this->get('/select-boletos-acordos/{contract}', 'selectBoletosDeals');

        $this->get('/view-boleto-select/{contract}/{boleto}', 'viewBoletoSelect');
        $this->get('/view-boleto-acordo/{contract}/{boleto}', 'viewBoletoDeal');
        $this->get('/view-boleto-campaign/{contract}/{boleto_id}/{index_date}', 'viewBoletoCampaign');

        $this->get('/view-boleto-letter/{contract}/{boleto_id}/{index_date}', 'viewBoletoLetter');

        $this->get('/return-download-boleto/{contract}', 'returnDownloadBoleto');
        $this->get('/download-boleto/{contract}', 'downloadBoleto');

        $this->get('/download-boleto-campaign/{contract}', 'downloadBoleto');
        $this->get('/return-download-boleto-campaign/{contract}', 'returnDownloadBoletoCampaign');

        $this->get('/return-download-boleto-letter/{contract}', 'returnDownloadBoletoLetter');

        $this->post('/register-email/{contract}', 'registerEmail');
        $this->get('/return-send-email/{contract}', 'returnSendEmail');

        $this->get('/return-send-email-campaign/{contract}', 'returnSendEmailCampaign');
        $this->get('/return-send-email-letter/{contract}', 'returnSendEmailLetter');


        $this->post('/register-phone/{contract}', 'registerPhone');
        $this->get('/return-send-sms/{contract}', 'registerSendSms');
        $this->get('/return-send-sms-campaign/{contract}', 'registerSendSmsCampaign');
        $this->get('/return-send-sms-letter/{contract}', 'registerSendSmsLetter');

        $this->get('/invoice-details/{contract}', 'invoiceDetails');

        return $this->controllers;
    }

    public function startDeal()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $dealOption = $session->has('deal_option') ? $session->get('deal_option') : 0;

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/menu');
        }

        if ($session->has('date_select_' . $contract)) {
            $date = $session->get('date_select_' . $contract);
        } else {
            $date = $this->getRequest()->get('date') - 1;
        }

        $dates =  $this->service->getDatesPayment();

        $dateSel = 1;
        $dateDesc = $dates[$date];
        if ($date != '' && $date <= $this->service->getRangeDays()) {
            $dateSel = isset($dates[$date]) ? $date : 1;
            $dateDesc = isset($dates[$date]) ? $dates[$date] : $dates[0];
        }

        $this->service->setHeaders(['guid' => $clientData['Guid']]);

        $deals = $this->service->deals($clientData, $currentProduct, $dateDesc);
        if (count($deals) == 0) {
            $this->redirect('/enter/not-found');
        }

        $dealId = $this->service->getDealId();
        $valueCurrent = array_values($deals)[0]['Valor'];

        $products[$contract]['deal_id'] = $this->service->getDealId();
        $products[$contract]['deals'] = $deals;
        $session->set('products', $products);

        $this->registerPage('Iniciar Acordo');
        $data = [
            'client' => $clientData,
            'dealId' => $dealId,
            'deals' => $deals,
            'product' => $currentProduct,
            'dates' => $dates,
            'dateDesc' => $dateDesc,
            'dateSel' => $dateSel,
            'valueCurrent' => $valueCurrent,
            'dealOption' => $dealOption
        ];

        if ($session->get('camp_agroup_movel') == $contract) {
            $deal = array_shift($deals);
            $custom = $session->get('custom');
            $data['deal'] = $this->service->formatDealGroup($deal, $custom);

            return $this->render('pay-info/start-deal-group.twig', $data);
        } else {
            return $this->render('pay-info/start-deal.twig', $data);
        }
    }

    public function dealInvoices()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $dealOption = $session->has('deal_option') ? $session->get('deal_option') : 0;

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/logoff');
        }

        $blockPromo = ($currentProduct['IsCampaign'] && $currentProduct['isInvoicesDeal']);

        if ($session->has('date_select_' . $contract)) {
            $date = $session->get('date_select_' . $contract);
        } else {
            $date = 0;
            if ($this->getRequest()->get('date') != '') {
                $date = $this->getRequest()->get('date') - 1;
            }
        }

        $dates =  $this->service->getDatesPayment();

        $dateSel = 1;
        $dateDesc = $dates[$date];
        if ($date != '' && $date <= $this->service->getRangeDays()) {
            $dateSel = isset($dates[$date]) ? $date : 1;
            $dateDesc = isset($dates[$date]) ? $dates[$date] : $dates[0];
        }

        $this->service->setHeaders(['guid' => $clientData['Guid']]);

        $deals = $this->service->deals($clientData, $currentProduct, $dateDesc);
        $products[$contract]['deals'] = $deals;
        $deal = array_shift($deals);
        $dealId = $this->service->getDealId();

        $parcels = $this->service->formatInvoicesCampaign($currentProduct);
        $session->set('parcels', $parcels);

        $products[$contract]['deal_id'] = $dealId;
        $session->set('option', $deal['Id']);
        $session->set('products', $products);

        $this->registerPage('Iniciar Acordo');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'dates' => $dates,
            'dateDesc' => $dateDesc,
            'dateSel' => $dateSel,
            'parcels' => $parcels,
            'deal' => $deal,
            'dealId' => $dealId,
            'dealOption' => $dealOption,
            'blockPromo' => $blockPromo
        ];
        return $this->render('pay-info/deal-invoices.twig', $data);
    }

    public function invoices()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $dealOption = $session->has('deal_option') ? $session->get('deal_option') : 0;

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/logoff');
        }


        if ($session->has('date_select_' . $contract)) {
            $date = $session->get('date_select_' . $contract);
        } else {
            $date = 0;
            if ($this->getRequest()->get('date') != '') {
                $date = $this->getRequest()->get('date') - 1;
            }
        }
        $dates =  $this->service->getDatesPayment();

        $dateSel = 1;
        $dateDesc = $dates[$date];
        if ($date != '' && $date <= $this->service->getRangeDays()) {
            $dateSel = isset($dates[$date]) ? $date : 1;
            $dateDesc = isset($dates[$date]) ? $dates[$date] : $dates[0];
        }

        $this->service->setHeaders(['guid' => $clientData['Guid']]);

        $deals = $this->service->deals($clientData, $currentProduct, $dateDesc);
        $products[$contract]['deals'] = $deals;
        $deal = array_shift($deals);
        $dealId = $this->service->getDealId();

        /**
         * Caso da carteira 183 com campo CAMP-AGRUP-OIMOVEL nas opções de negociação
         * e a primeira parcelas ter dados custom
         */
        $checkClientLetter = $this->service->checkClientLetter($clientData, $currentProduct, $deal, $contract);

        if ($checkClientLetter) {
            $session->set('camp_agroup_movel', $contract);
            $this->redirect('/pay-info/start-deal/' . $contract . '/1');
        }

        $invoices = $this->service->formatInvoices($currentProduct);

        $session->set('parcels', $invoices);

        $products[$contract]['deal_id'] = $dealId;
        $session->set('option', $deal['Id']);
        $session->set('products', $products);

        $this->registerPage('Iniciar Acordo');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'dates' => $dates,
            'dateDesc' => $dateDesc,
            'dateSel' => $dateSel,
            'invoices' => $invoices,
            'deal' => $deal,
            'dealId' => $dealId,
            'dealOption' => $dealOption
        ];

        return $this->render('pay-info/invoices.twig', $data);
    }

    public function invoicesLetter()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $dealOption = $session->has('deal_option') ? $session->get('deal_option') : 0;

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/logoff');
        }

        if ($session->has('date_select_' . $contract)) {
            $date = $session->get('date_select_' . $contract);
        } else {
            $date = 0;
            if ($this->getRequest()->get('date') != '') {
                $date = $this->getRequest()->get('date') - 1;
            }
        }
        $dates =  $this->service->getDatesPayment();

        $dateSel = 1;
        $dateDesc = $dates[$date];
        if ($date != '' && $date <= $this->service->getRangeDays()) {
            $dateSel = isset($dates[$date]) ? $date : 1;
            $dateDesc = isset($dates[$date]) ? $dates[$date] : $dates[0];
        }

        $this->service->setHeaders(['guid' => $clientData['Guid']]);

        $deals = $this->service->deals($clientData, $currentProduct, $dateDesc);
        $products[$contract]['deals'] = $deals;
        $deal = array_shift($deals);
        $dealId = $this->service->getDealId();

        $invoiceLetter = $this->service->formatInvoiceLetter($currentProduct);

        $products[$contract]['deal_id'] = $dealId;
        $session->set('option', $deal['Id']);

        $session->set('products', $products);
        $session->set('invoiceLetter', $invoiceLetter);

        $this->registerPage('Iniciar Acordo');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'dates' => $dates,
            'dateDesc' => $dateDesc,
            'dateSel' => $dateSel,
            'invoice' => $invoiceLetter,
            'deal' => $deal,
            'dealId' => $dealId,
            'dealOption' => $dealOption
        ];
        return $this->render('pay-info/invoices-letter.twig', $data);
    }

    public function reloadDeals()
    {
        $session = $this->getSession();
        $pars = $this->getParameters();

        $contract = $pars['contract'];
        $dateIndex = $pars['date_index'] - 1;
        $dealId = $pars['deal_id'];
        $dealOption = $pars['deal_option'];

        $session->set('date_select_' . $contract, $dateIndex);
        $session->set('deal_option', $dealOption);

        $clientData = $session->get('client_data');
        $products = $session->get('products');

        $dates =  $this->service->getDatesPayment();
        $dateSel = $dates[$dateIndex];

        $this->service->setHeaders(['guid' => $clientData['Guid']]);
        $deals = $this->service->getDealOptions($dealId, $contract, $dateSel);

        $products[$contract]['deal_id'] = $dealId;
        $products[$contract]['deals'] = $deals;
        $session->set('products', $products);

        $this->registerPage('Nova Data');
        $data = [
            'deal_id' => $dealId,
            'date_index' => $dateIndex,
        ];
        return new JsonResponse($data);
    }

    public function confirmDealCampaign()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $option = $this->getRequest()->get('option');
        $deal = $this->getRequest()->get('deal');

        $session->set('option', $option);

        $dates =  $this->service->getDatesPayment();
        $date = $dates[count($dates) - 1];

        $clientData = $session->get('client_data');
        $products = $session->get('products');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $currentProduct['deal_id'] = $deal;
        $this->service->setHeaders(['guid' => $clientData['Guid']]);
        $status = $this->service->confirmDeal($currentProduct, $option, $date);

        if ($status) {
            $boleto = $this->service->getBoleto();
            $session->set('boletos', $boleto);
            $this->redirect('/pay-info/view-boleto/' . $contract);
        } else {
            $this->redirect('/pay-info/boleto-nao-emitido');
        }
    }

    public function confirmDeal()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $option = $this->getRequest()->get('option');
        $dateSel = $this->getRequest()->get('date');

        $session->set('option', $option);

        $dates =  $this->service->getDatesPayment();
        $date = isset($dates[$dateSel]) ? $dates[$dateSel] : '';

        $clientData = $session->get('client_data');
        $products = $session->get('products');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];

        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->service->setHeaders(['guid' => $clientData['Guid']]);
        $status = $this->service->confirmDeal($currentProduct, $option, $date);

        if ($status) {
            $boleto = $this->service->getBoleto();
            $session->set('boletos', $boleto);
            $this->redirect('/pay-info/view-boleto/' . $contract);
        } else {
            $this->redirect('/pay-info/boleto-nao-emitido');
        }
    }

    /**
     * @return string html
     * @throws \Exception
     */
    public function BoletoNaoEmitido()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $this->registerPage('Boleto não emitido : ' . $contract);

        $data = [];
        $data['client'] = $session->get('client_data');
        return $this->render('pay-info/boleto-nao-emitido.twig', $data);
    }

    public function confirmDate()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get('client_data');
        $products = $session->get('products');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $data = [
            'client'  => $clientData,
            'product' => $currentProduct,
        ];
        return $this->render('pay-info/confirm-date.twig', $data);
    }

    public function registerDate()
    {
        $session = $this->getSession();
        $pars = $this->getParameters();

        $contract = $pars['contract'];
        $dateConfirm = $pars['date_confirm'];

        $clientData = $session->get('client_data');
        $products = $session->get('products');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];

        $returnData = [];
        if (count($currentProduct) == 0) {
            $this->service->setStatus(false);
        } else {
            $this->service->setHeaders(['guid' => $clientData['Guid']]);
            $returnData = $this->service->confirmPayment($clientData, $currentProduct, $dateConfirm);
        }

        $data = [
            'status' => $this->service->getStatus(),
            'message' => $this->service->getMessage(),
            'data' => $returnData
        ];
        return new JsonResponse($data);
    }

    /**
     * @return string html
     * @throws \Exception
     */
    public function returnRegisterDate()
    {
        $this->checkAccess();
        $session = $this->getSession();

        $this->registerPage('Retorno da Confirmação de Pagamento');

        $data = [];
        $data['client'] = $session->get('client_data');
        return $this->render('pay-info/return-register-date.twig', $data);
    }

    public function viewBoleto()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');
        $boletos = $session->get('boletos');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $deal = $currentProduct['deals'][$option];
        $this->service->registerDealData($clientData, $contract, $currentProduct, $deal, $boletos, $products);
        $numBoletos = count($boletos);

        if ($numBoletos > 1) {
            $this->redirect('/pay-info/select-boletos/' . $contract);
        } else if ($numBoletos == 1) {
            $this->redirect('/pay-info/view-boleto-select/' . $contract . '/0');
        } else {
            $this->redirect('/enter/options');
        }
    }

    public function selectBoletosDeals()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get('client_data');
        $deals = $session->get('deals_actives');
        $deal = isset($deals[$contract]) ? $deals[$contract] : [];

        $this->service->setHeaders(['guid' => $clientData['Guid']]);
        $this->service->issueBoletoDeal($deal);
        $boletos = $this->service->getBoleto();
        $session->set('boletos', $boletos);

        $data = [
            'client' => $clientData,
            'deal' => $deal,
            'boletos' => $boletos
        ];
        return $this->render('pay-info/select-boletos-deals.twig', $data);
    }

    public function selectBoletos()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');
        $boletos = $session->get('boletos');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $deal = $currentProduct['deals'][$option];
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'deal' => $deal,
            'boletos' => $boletos
        ];
        return $this->render('pay-info/select-boletos.twig', $data);
    }

    public function viewBoletoSelect()
    {
        $session = $this->getSession();

        $contract = $this->getRequest()->get('contract');
        $boletoSelect = $this->getRequest()->get('boleto');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');
        $boletos = $session->get('boletos');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $boleto = isset($boletos[$boletoSelect]) ? $boletos[$boletoSelect] : [];
        if (count($boleto) == 0) {
            $this->redirect('/enter/options');
        }

        $session->set('currentProduct', $currentProduct);
        $session->set('boleto', $boleto);
        $session->set('contract', $contract);

        $deal = $currentProduct['deals'][$option];
        $this->registerPage('Acessar Boleto');

        $codigoBarra = $this->service->getCodigoBarra($boleto);

        $clientData['phone'] = str_replace([')', '(', '-', ' ',], '', $clientData['phone']);
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'deal' => $deal,
            'boleto' => $boleto,
            'codigoBarra' => $codigoBarra
        ];

        return $this->render('pay-info/view-boleto-select.twig', $data);
    }

    public function viewBoletoDeal()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boletoSelect = $this->getRequest()->get('boleto');
        $clientData = $session->get('client_data');
        $option = $session->get('option');
        $boletos = $session->get('boletos');

        $deals = $session->get('deals_actives');
        $deal = isset($deals[$contract]) ? $deals[$contract] : [];

        $boleto = isset($boletos[$boletoSelect]) ? $boletos[$boletoSelect] : [];

        if (count($boleto) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Acessar Boleto Acordo');
        $codigoBarra = $this->service->getCodigoBarra($boleto);

        $data = [
            'client' => $clientData,
            'deal' => $deal,
            'boleto' => $boleto,
            'codigoBarra' => $codigoBarra
        ];

        return $this->render('pay-info/view-boleto-deal.twig', $data);
    }

    public function viewBoletoCampaign()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boletoId = $this->getRequest()->get('boleto_id');
        $indexDate = $this->getRequest()->get('index_date');
        $deal = isset($deals[$contract]) ? $deals[$contract] : [];

        $session->set('boleto_id', $boletoId);
        $session->set('index_date', $indexDate);

        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $parcels  = $session->get('parcels');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/logoff');
        }

        $dates =  $this->service->getDatesPayment();
        $indexDate -= 1;
        $dateDesc = isset($dates[$indexDate]) ? $dates[$indexDate] : $dates[0];

        $boleto = $this->service->getBoletoCampanha($parcels, $boletoId);
        $boleto['Vencimento'] = $dateDesc;
        $session->set('boleto', $boleto);

        $this->registerPage('Acessar Boleto Campanha');
        $codigoBarra = $this->service->getCodigoBarra($boleto);

        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto' => $boleto,
            'codigoBarra' => $codigoBarra
        ];

        return $this->render('pay-info/view-boleto-campaign.twig', $data);
    }

    public function viewBoletoLetter()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boletoId = $this->getRequest()->get('boleto_id');
        $indexDate = $this->getRequest()->get('index_date');
        $deal = isset($deals[$contract]) ? $deals[$contract] : [];

        $session->set('boleto_id', $boletoId);
        $session->set('index_date', $indexDate);

        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $parcels  = $session->get('parcels');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/logoff');
        }

        $dates =  $this->service->getDatesPayment();
        $indexDate -= 1;
        $dateDesc = isset($dates[$indexDate]) ? $dates[$indexDate] : $dates[0];

        $boleto =  $session->get('invoiceLetter');
        $boleto['LinhaDigitavel'] = $boleto['codBarra'];
        $boleto['Vencimento'] = $dateDesc;

        $boleto['Valor_final_Format'] = $boleto['valorPagar'];
        $boleto['Datavencimentofatura'] = $dateDesc;


        $session->set('boleto', $boleto);

        $this->registerPage('Acessar Boleto Carta');
        $codigoBarra = $this->service->getCodigoBarra($boleto);

        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto' => $boleto,
            'codigoBarra' => $codigoBarra
        ];

        return $this->render('pay-info/view-boleto-letter.twig', $data);
    }

    public function returnDownloadBoletoCampaign()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $boletoId = $this->getSession()->get('boleto_id');
        $indexDate = $this->getSession()->get('index_date');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno Download Boleto Campanha');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto_id' => $boletoId,
            'index_date' => $indexDate,
        ];

        return $this->render('pay-info/return-download-campaign.twig', $data);
    }

    public function returnDownloadBoletoLetter()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $boletoId = $this->getSession()->get('boleto_id');
        $indexDate = $this->getSession()->get('index_date');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno Download Boleto Campanha');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto_id' => $boletoId,
            'index_date' => $indexDate,
        ];

        return $this->render('pay-info/return-download-letter.twig', $data);
    }

    public function returnDownloadBoleto()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');
        $boletos = $session->get('boletos');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno Download Boleto');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'deal' => $currentProduct['deals'][$option],
            'boletos' => $boletos
        ];

        return $this->render('pay-info/return-download.twig', $data);
    }

    public function downloadBoleto()
    {
        $session = $this->getSession();
        $file = $session->get('file');
        $parts = explode('\\', $file);
        $name = $parts[count($parts) - 1];

        header("Content-Disposition: attachment; filename=" . urlencode($name));
        header("Content-Type: application/pdf");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file));
        readfile($file);

    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function registerEmail()
    {
        $this->checkAccess();

        $phone = $this->getRequest()->get('email');

        $data = [
            'status' => true,
            'message' => '',
        ];

        $clientData = $this->getSession()->get(self::KEY_CLIENT);
        $clientData['email'] = $phone;
        $this->getSession()->set(self::KEY_CLIENT, $clientData);

        return new JsonResponse($data);
    }

    public function returnSendEmail()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');
        $boleto = $session->get('boleto');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno Email Boleto');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'deal' => $currentProduct['deals'][$option],
            'boleto' => $boleto
        ];

        return $this->render('pay-info/return-email.twig', $data);
    }

    public function returnSendEmailCampaign()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $boletoId = $this->getSession()->get('boleto_id');
        $indexDate = $this->getSession()->get('index_date');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno Email Boleto Campanha');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto_id' => $boletoId,
            'index_date' => $indexDate,
        ];

        return $this->render('pay-info/return-email-campaign.twig', $data);
    }

    public function returnSendEmailLetter()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $boletoId = $this->getSession()->get('boleto_id');
        $indexDate = $this->getSession()->get('index_date');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno Email Boleto Campanha');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto_id' => $boletoId,
            'index_date' => $indexDate,
        ];

        return $this->render('pay-info/return-email-letter.twig', $data);
    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function registerPhone()
    {
        $this->checkAccess();

        $phone = $this->getRequest()->get('phone');

        $data = [
            'status' => true,
            'message' => '',
        ];

        $clientData = $this->getSession()->get(self::KEY_CLIENT);
        $clientData['phone'] = str_replace([')', '(', '-', ' ',], '', $phone);
        $this->getSession()->set(self::KEY_CLIENT, $clientData);

        return new JsonResponse($data);
    }

    public function registerSendSms()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');
        $boleto = $session->get('boleto');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno Email Boleto');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'deal' => $currentProduct['deals'][$option],
            'boleto' => $boleto
        ];


        return $this->render('pay-info/return-sms.twig', $data);
    }


    public function registerSendSmsCampaign()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $boletoId = $this->getSession()->get('boleto_id');
        $indexDate = $this->getSession()->get('index_date');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno SMS Boleto Campanha');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto_id' => $boletoId,
            'index_date' => $indexDate,
        ];

        return $this->render('pay-info/return-sms-campaign.twig', $data);
    }

    public function registerSendSmsLetter()
    {
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $boletoId = $this->getSession()->get('boleto_id');
        $indexDate = $this->getSession()->get('index_date');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $this->registerPage('Retorno SMS Boleto Campanha');
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto_id' => $boletoId,
            'index_date' => $indexDate,
        ];

        return $this->render('pay-info/return-sms-letter.twig', $data);
    }
}
