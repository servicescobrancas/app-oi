<?php

namespace App\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\EnterService;
use App\Service\AccessService;
use App\Entities\Client;


class EnterController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var AccessService
     */
    private $serviceAccess;

    public function __construct(EnterService $enterService)
    {
        $this->service = $enterService;
    }

    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/options', 'options');
        $this->get('/menu', 'menu');
        $this->get('/inform-payment', 'informPayment');
        $this->get('/view-deals', 'viewDeals');
        $this->get('/parcels/{contract}', 'parcels');
        $this->get('/not-found', 'notFound');

        $this->get('/info', 'info');
        $this->post('/send-info', 'sendInfo');

        return $this->controllers;
    }

    public function setServiceAccess(AccessService $serviceAccess)
    {
        $this->serviceAccess = $serviceAccess;
    }

    public function menu()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get('client_data');
        $products = null; // $session->get('products');
        $this->registerPage('Menu');

        $data = [
            'client' => $clientData
        ];
        $tpl = 'enter/menu.twig';
        return $this->render($tpl, $data);
    }

    public function informPayment()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $products = null; // $session->get('products');
        $this->registerPage('Informar Pagamento');

        if ($products == null) {
            $this->service->setHeaders(['guid' => $clientData['Guid']]);
            $products = $this->service->loadPendencies($clientData);
            if (count($products) == 0) {
                $this->redirect('/enter/not-found');
            }
            $session->set('products', $products);
        }
        $data = [
            'client' => $clientData,
            'products' => $products
        ];
        $tpl = 'enter/inform-payment.twig';
        return $this->render($tpl, $data);
    }

    public function options()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $products = null; // $session->get('products');
        $this->registerPage('Lista de Contratos');

        if ($products == null) {
            $this->service->setHeaders(['guid' => $clientData['Guid']]);
            $products = $this->service->loadPendencies($clientData);
            $custom = $this->service->getCustomData();
            if (count($products) == 0) {
                $this->redirect('/enter/not-found');
            }
            $session->set('camp_agroup_movel', '');
            $session->set('custom', $custom);
            $session->set('products', $products);
        }
        $data = [
            'client' => $clientData,
            'products' => $products
        ];
        $tpl = 'enter/index.twig';
        return $this->render($tpl, $data);
    }

    public function parcels()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');

        $clientData = $session->get(self::KEY_CLIENT);
        $products = $session->get('products');
        $this->registerPage('Lista de Parcelas');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $data = [
            'client' => $clientData,
            'product' => $currentProduct
        ];
        $tpl = 'enter/parcels.twig';
        return $this->render($tpl, $data);
    }

    public function viewDeals()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);

        $this->registerPage('Lista de Acordos');

        $this->service->setHeaders(['guid' => $clientData['Guid']]);
        $deals = $this->service->getDealsActives($clientData);
        $session->set('deals_actives', $deals);

        $data = [
            'client' => $clientData,
            'deals' => $deals
        ];
        $tpl = 'enter/view-deals.twig';
        return $this->render($tpl, $data);
    }

    public function notFound()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $this->registerPage('Cliente não encontrato');

        $data = [
            'client' => $clientData
        ];
        $tpl = 'enter/not-found.twig';
        return $this->render($tpl, $data);
    }

    public function info()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $this->registerPage('Registro Dados');

        $data = [
            'client' => $clientData
        ];
        $tpl = 'enter/info.twig';
        return $this->render($tpl, $data);
    }

    public function sendInfo()
    {
        $pars = $this->getParameters();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $isValid = $this->service->registerInfo($clientData, $pars);

        $data = [
            'status' => $isValid,
            'message' => $this->service->getMessage()
        ];
        return new JsonResponse($data);
    }

    public function returnInfo()
    {
        $this->checkAccess();
        $session = $this->getSession();
        $clientData = $session->get(self::KEY_CLIENT);
        $this->registerPage('Registro Dados');

        $data = [
            'client' => $clientData
        ];
        $tpl = 'enter/return-info.twig';
        return $this->render($tpl, $data);
    }

}
