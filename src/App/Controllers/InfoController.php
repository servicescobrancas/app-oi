<?php

namespace App\Controllers;

use Silex\Application;

use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\EnterService;
use App\Service\AccessService;

class InfoController extends BaseController implements ControllerProviderInterface
{

    /**
     * @var EnterService
     */
    private $serviceEnter;

    /**
     * @var AccessService
     */
    private $accessService;


    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $this->get('/', 'index');
        $this->get('/start', 'start');

        $this->post('/login', 'login');
        $this->post('/select-cpf', 'selectCpf');

        return $this->controllers;
    }

    /**
     * @param EnterService $serviceEnter
     */
    public function setServiceEnter(EnterService $serviceEnter)
    {
        $this->serviceEnter = $serviceEnter;
    }

    /**
     * @param AccessService $accessService
     */
    public function setServiceAccess(AccessService $accessService)
    {
        $this->accessService = $accessService;
    }

    public function index()
    {
        if( $this->getSession()->has(self::KEY_OPER)) {
            $this->redirect('/info/start');
        }

        $this->registerPage('Acesso Info');

        $status = $this->getParameter('invalid-credentials');

        $data = ['status' => $status];
        return $this->render('info/index.twig', $data);
    }

    public function login()
    {
        $pars = $this->getParameters();

        if($pars['user'] == 'digital' && $pars['password'] == 'Digital#2016') {
            $this->getSession()->set(self::KEY_OPER, true);
            $this->redirect('/info/start');
        } else {
            $this->redirect('/info/?status=invalid-credentials');
        }
    }

    public function start()
    {
        return $this->render('info/start.twig', []);
    }

    public function selectCpf()
    {

        $session = $this->getSession();
        $cpf = $this->getParameter('cpf');
        $client = [
            'name' => '',
            'cpf' => $cpf,
            'telefone' => '',
            'email' => '',
            'dominio' => DOMAIN
        ];

        $clientEntity = $this->accessService->populateResumeClientData($client, 5);
        $debtorData = $this->serviceEnter->getDebtorData($clientEntity);
        $status = false;
        $message = 'CPF não encontrado.';
        if($debtorData) {
            $session->set(self::KEY_DEBTOR, $debtorData);

            $clientData = $this->serviceEnter->getClientData();
            $clientData['id_origem'] = 10;
            $clientData['token'] = '';

            $this->setClassInSession(self::KEY_CLIENT, $clientEntity);
            $status = true;
            $message = '';

            $this->registerPage('Acesso Info - CPF', '', $cpf);
        }
        $data = [
            'status' => $status,
            'message' => $message
        ];
        return new JsonResponse($data);
    }

    /**
     * @param $page
     * @param int $lastPage
     * @param string $cpf
     * @return int
     */
    public function registerPage($page, $lastPage = 0, $cpf = '')
    {
        if ($this->getSession()->has(self::KEY_CLIENT)) {
            $client = $this->getClassInSession(self::KEY_CLIENT);
            $cpf = $client->getCpf();
            $this->service->setClientData($client);
        } else if ($this->getSession()->has(self::KEY_CLIENT_TMP)) {
            $client = $this->getClassInSession(self::KEY_CLIENT_TMP);
            $cpf = $client->getCpf();
            $this->service->setClientData($client);
        }

        return $this->service->registerPage($page, $cpf, $this->getSessionKey(), '200.186.56.98', $lastPage);
    }
}
