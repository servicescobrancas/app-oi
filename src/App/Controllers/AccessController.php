<?php

namespace App\Controllers;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\AccessService;

class AccessController extends PagesController implements ControllerProviderInterface
{

    /**
     * @var AccessService
     */
    public $service;

    /**
     * AccessController constructor.
     * @param AccessService $baseService
     */
    public function __construct(AccessService $baseService)
    {
        $this->service = $baseService;
    }

    /**
     * @param Application $app
     * @return mixed|\Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $this->controllers = parent::connect($app);

        $this->get('/', 'index');

        $this->get('/acesso', 'access');
        $this->post('/acesso-enviar', 'accessSend');

        $this->get('/logoff', 'logoff');

        return $this->controllers;
    }

    /**
     * @return string
     */
    public function index()
    {
        $firstPage = $this->registerPage('Tela de Entrada - Abertura');

        $session = $this->getSession();
        $session->set('firstPage', $firstPage);

        $data = [];
        return $this->render('access/index.twig', $data);
    }

    /**
     * @return string
     */
    public function access()
    {
        $checkRecaptcha = $this->service->checkRecaptcha();
        $this->getSession()->set('is_recaptcha', $checkRecaptcha);

        if ($this->getSession()->has('pass_steps')) {
            //$this->redirect('/enter/menu');
        }

        $firstPage = $this->registerPage('Tela de Entrada - Abertura');

        $session = $this->getSession();
        $session->set('firstPage', $firstPage);

        $data = [
            'is_recaptcha' => $checkRecaptcha
        ];
        return $this->render('access/access.twig', $data);
    }

    /**
     * @return string
     */
    public function accessSend()
    {
        $this->service->setHeaders([]);

        $pars = $this->getParameters();
        $gRecaptchaResponse = isset($pars['g-recaptcha-response']) ? $pars['g-recaptcha-response'] : false;
        $remoteIp = '';

        $checkRecaptcha = $this->getSession()->get('is_recaptcha');

        $recaptchaCheck = true;
        if ($checkRecaptcha) {
            $recaptcha = new \ReCaptcha\ReCaptcha(RECAPCHA_KEY);
            $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);

            $isValid = false;
            $recaptchaCheck = $gRecaptchaResponse ? $resp->isSuccess() : false;
        }

        if ($recaptchaCheck) {
            $this->service->access($pars);
            $isValid = $this->service->getStatus();

            if ($isValid) {
                $dataClient = $this->service->getData();
                $this->getSession()->set('terms', $isValid);
                $this->getSession()->set('pass_steps', $isValid);

                $this->getSession()->set(self::KEY_CLIENT, $dataClient);
            } else {
                $this->getSession()->set(self::KEY_CLIENT, $pars);
            }
        }

        $this->registerPage('Enviar Cadastro');

        $data = [
            'status' => $isValid,
            'message' => $this->service->getMessage(),
            'check_recaptcha' => $recaptchaCheck
        ];
        return new JsonResponse($data);
    }

    /**
     *
     */
    public function logoff()
    {
        $session = $this->getSession();
        $this->removeSessionVars($session);

        $this->redirect('/');
    }
}