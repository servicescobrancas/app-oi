<?php

namespace App\Controllers;

use App\Entities\Client;
use Silex\Application;

use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\BoletoService;

class BoletoController extends BaseController implements ControllerProviderInterface
{

    const KEY_DAYS = 'days';
    const KEY_FILE = 'file';

    const REGISTER_PRINT = 'Gerar PDF Boleto';
    const REGISTER_DOWNLOAD = 'Baixar PDF Boleto';
    const REGISTER_EMAIL = 'Enviar Boleto Por E-mail';
    const REGISTER_SMS = 'Enviar Boleto Por SMS';

    /**
     * @var BoletoService
     */
    protected $service;

    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $this->controllers = $app['controllers_factory'];

        $schemaMethods = [
            '/days' => 'days',
            '/download-boleto/{contract}' => 'download',
            '/download-boleto-campaign/{contract}' => 'downloadCampaign',
            '/download-boleto-letter/{contract}' => 'downloadLetter',
            '/print' => 'printBoleto',

            '/email' => 'email',
            '/email-campaign/{contract}' => 'emailCampaign',

            '/sms' => 'sms',
            '/sms-campaign/{contract}' => 'smsCampaign',

            '/download' => 'downloadFile',
        ];

        foreach($schemaMethods as $uri => $method) {
            $this->get($uri, $method);
        }

        return $this->controllers;
    }

    /**
     * BaseController constructor.
     *
     * @param BoletoService $baseService
     */
    public function __construct(BoletoService $baseService)
    {
        $this->service = $baseService;
    }

    public function download()
    {
        $this->checkAccess();
        $this->registerPage(self::REGISTER_PRINT);

        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boleto = $session->get('boleto');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];

        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $deal = $currentProduct['deals'][$option];

        $codigoBarra = $this->service->getCodigoBarra($boleto);
        $clientData['phone'] = str_replace([')', '(', '-', ' ',], '', $clientData['phone']);
        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'deal' => $deal,
            'boleto' => $boleto,
            'codigoBarra' => $codigoBarra
        ];

        $file = $this->service->generateBoleto($data, $codigoBarra, $this->view);
        $data['file'] = $file;
        $session->set(self::KEY_FILE, $data['file']);

        $this->redirect('/pay-info/return-download-boleto/' . $contract);
    }

    public function downloadCampaign()
    {
        $this->checkAccess();
        $this->registerPage(self::REGISTER_PRINT);

        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boleto = $session->get('boleto');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];

        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $codigoBarra = $this->service->getCodigoBarra($boleto);
        $clientData['phone'] = str_replace([')', '(', '-', ' ',], '', $clientData['phone']);

        $pars = [
            'LinhaDigitavel' => $boleto['LinhaDigitavel'],
            'Valor' => $boleto['Valor_final_Format'],
            'Vencimento' => $boleto['Vencimento'],
            'VencOrigial' => $boleto['Datavencimentofatura'],
        ];

        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto' => $pars,
            'codigoBarra' => $codigoBarra
        ];

        $file = $this->service->generateBoleto($data, $codigoBarra, $this->view);
        $data['file'] = $file;
        $session->set(self::KEY_FILE, $data['file']);

        $this->redirect('/pay-info/return-download-boleto-campaign/' . $contract);
    }

    public function downloadLetter()
    {
        $this->checkAccess();
        $this->registerPage(self::REGISTER_PRINT);

        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boleto = $session->get('boleto');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];

        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $codigoBarra = $this->service->getCodigoBarra($boleto);
        $clientData['phone'] = str_replace([')', '(', '-', ' ',], '', $clientData['phone']);

        $pars = [
            'LinhaDigitavel' => $boleto['LinhaDigitavel'],
            'Valor' => $boleto['Valor_final_Format'],
            'Vencimento' => $boleto['Vencimento'],
            'VencOrigial' => $boleto['Datavencimentofatura'],
        ];

        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto' => $pars,
            'codigoBarra' => $codigoBarra
        ];

        $file = $this->service->generateBoleto($data, $codigoBarra, $this->view);
        $data['file'] = $file;
        $session->set(self::KEY_FILE, $data['file']);

        $this->redirect('/pay-info/return-download-boleto-letter/' . $contract);
    }

    public function downloadFile()
    {
        $this->checkAccess();

        $session = $this->getSession();
        $this->registerPage(self::REGISTER_DOWNLOAD);

        $client = $this->getClassInSession(self::KEY_CLIENT);
        $file = $session->get(self::KEY_FILE);

        $fileName = $client->getCpf() . '.pdf';

        header("Content-Disposition: attachment; filename=" . urlencode($fileName));
        header("Content-Type: application/pdf");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file));
        readfile($file);
    }

    public function email()
    {
        $this->checkAccess();
        $this->registerPage(self::REGISTER_EMAIL);

        $session = $this->getSession();
        $contract = $session->get('contract');
        $boleto = $session->get('boleto');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');
        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];
        $deal = $currentProduct['deals'][$option];
        $codigoBarra = $this->service->getCodigoBarra($boleto);

        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'deal' => $deal,
            'boleto' => $boleto,
            'codigoBarra' => $codigoBarra
        ];

        $file = $this->service->generateBoleto($data, $codigoBarra, $this->view);
        $status = $this->service->registerEmail($clientData, $boleto, $contract, $file);
        $returnData = [
            'status' => $status,
            'message' => ''
        ];
        return new JsonResponse($returnData);
    }

    public function emailCampaign()
    {
        $this->checkAccess();
        $this->registerPage(self::REGISTER_PRINT);

        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boleto = $session->get('boleto');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];

        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $codigoBarra = $this->service->getCodigoBarra($boleto);
        $clientData['phone'] = str_replace([')', '(', '-', ' ',], '', $clientData['phone']);

        $parsBoleto = [
            'LinhaDigitavel' => $boleto['LinhaDigitavel'],
            'Valor' => $boleto['Valor_final_Format'],
            'Vencimento' => $boleto['Vencimento'],
            'VencOrigial' => $boleto['Datavencimentofatura'],
        ];

        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto' => $parsBoleto,
            'codigoBarra' => $codigoBarra
        ];

        $file = $this->service->generateBoleto($data, $codigoBarra, $this->view);
        $status = $this->service->registerEmail($clientData, $parsBoleto, $contract, $file);
        $returnData = [
            'status' => $status,
            'message' => ''
        ];
        return new JsonResponse($returnData);
    }

    public function sms()
    {
        $session = $this->getSession();
        $this->registerPage(self::REGISTER_SMS);

        $clientData = $this->getClassInSession(self::KEY_CLIENT);

        $debtor = $session->get(self::KEY_DEBTOR);

        $currentProduct = $session->get('currentProduct');
        $clientData = $session->get('client_data');
        $boleto = $session->get('boleto');
        $contract = $session->get('contract');

        $client = new Client();
        $client->setCompleteName($clientData['NomeConsumidor']);
        $client->setCpf($clientData['DocumentoConsumidor']);
        $client->setPhone($clientData['phone']);
        $client->setContract($contract);
        $client->setToken($clientData['Token']);
        $client->setWallet(15);
        $client->setOrigin(2);

        $contractData = $debtor['contracts'][$contract];
        $boleto['boleto_id'] = $currentProduct['deal_id'];

        $this->service->registerPhone($clientData,$boleto);

        $data = [
            'status' => $this->service->getStatus(),
            'message' => $this->service->getMessage()
        ];

        return new JsonResponse($data);
    }

    public function smsCampaign()
    {
        $this->checkAccess();
        $this->registerPage(self::REGISTER_PRINT);

        $session = $this->getSession();
        $contract = $this->getRequest()->get('contract');
        $boleto = $session->get('boleto');
        $clientData = $session->get('client_data');
        $products = $session->get('products');
        $option = $session->get('option');

        $currentProduct = isset($products[$contract]) ? $products[$contract] : [];

        if (count($currentProduct) == 0) {
            $this->redirect('/enter/options');
        }

        $codigoBarra = $this->service->getCodigoBarra($boleto);
        $clientData['phone'] = str_replace([')', '(', '-', ' ',], '', $clientData['phone']);

        $parsBoleto = [
            'LinhaDigitavel' => $boleto['LinhaDigitavel'],
            'Valor' => $boleto['Valor_final_Format'],
            'Vencimento' => $boleto['Vencimento'],
            'VencOrigial' => $boleto['Datavencimentofatura'],
        ];

        $data = [
            'client' => $clientData,
            'product' => $currentProduct,
            'boleto' => $parsBoleto,
            'codigoBarra' => $codigoBarra
        ];

        $file = $this->service->generateBoleto($data, $codigoBarra, $this->view);
        $status = $this->service->registerPhone($clientData, $parsBoleto);
        $returnData = [
            'status' => $status,
            'message' => ''
        ];
        return new JsonResponse($returnData);
    }
}
