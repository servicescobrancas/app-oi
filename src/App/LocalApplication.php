<?php

namespace App;

use Silex\Application;

use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SessionServiceProvider;

use Silex\Provider\DoctrineServiceProvider;
use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;

use Silex\Provider\TwigServiceProvider;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Debug\ExceptionHandler;

use App\Controllers\Factory\AccessControllerFactory;
use App\Controllers\Factory\EnterControllerFactory;
use App\Controllers\Factory\PayInfoControllerFactory;
use App\Controllers\Factory\InfoControllerFactory;
use App\Controllers\Factory\BoletoControllerFactory;
use App\Controllers\Factory\ContactControllerFactory;
use App\Controllers\Factory\ApiControllerFactory;
use App\Controllers\Factory\ChatControllerFactory;

use App\Controllers\Factory\ReceptiveControllerFactory;

use AppAdmin\Controllers\Factory\LoginControllerFactory;
use AppAdmin\Controllers\Factory\HomeControllerFactory;
use AppAdmin\Controllers\Factory\LogsControllerFactory;
use AppAdmin\Controllers\Factory\ExecuteControllerFactory;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Whoops\Run;
use Whoops\Handler\PrettyPageHandler;

class LocalApplication extends Application
{

    public function __construct(array $values = [])
    {
        parent::__construct($values);

        ExceptionHandler::register(false);

        $this->before(function(Request $request) {
        });

        $this->init();

        //handling CORS respons with right headers
        $this->after(function(Request $request, Response $response) {
            $response->headers->set('Access-Control-Allow-Origin', '*');
        });
    }

    public function init()
    {

        $app['debug'] = !PROD;

        if(PROD) {
            $whoops = new Run();
            $whoops->pushHandler(new PrettyPageHandler());
            $whoops->register();
        }

        $this->registerProviders();

        $this->error(function (\Exception $e, Request $request, $code) {

            if(!PROD ) {

                $this->registerLogError($e->getMessage());

                $request->getBaseUrl();
                $request->getMethod();
                $request->getClientIp();
                $data = [
                    'data_layer' => '{}',
                    'logon' => false
                ];

                if ($code == 404) {
                    return $this['twig']->render('site/errors/404.twig', $data);
                }

                return $this['twig']->render('site/errors/generic_error.twig', $data);
            }

            return false;
        });

        $this->mountController();
    }

    public function registerProviders()
    {
        $configDoctrine = include __DIR__.'/../../app/config/doctrine.php';

        $this->register(new DoctrineServiceProvider(), $configDoctrine['db.options']);
        $this->register(new DoctrineOrmServiceProvider(), $configDoctrine);

        $this->register(new ServiceControllerServiceProvider());
        $this->register(new SessionServiceProvider());

        $this->register(new TwigServiceProvider(), [
            'twig.path' => __DIR__.'/../../views',
            'debug' => true
        ]);
    }

    public function mountController()
    {
        /**
         * Mount Controllers
         */
        $this->mount('/', (new AccessControllerFactory($this))->getController() );
        $this->mount('/enter', (new EnterControllerFactory($this))->getController() );
        $this->mount('/pay-info', (new PayInfoControllerFactory($this))->getController() );
        $this->mount('/info', (new InfoControllerFactory($this))->getController() );
        $this->mount('/boleto', (new BoletoControllerFactory($this))->getController() );
        $this->mount('/contact', (new ContactControllerFactory($this))->getController() );
        $this->mount('/api', (new ApiControllerFactory($this))->getController() );
        $this->mount('/chat', (new ChatControllerFactory($this))->getController() );

        // Receptive Section
        $this->mount('/receptivo', (new ReceptiveControllerFactory($this))->getController() );

        $this->mount('/admin', (new LoginControllerFactory($this))->getController() );
        $this->mount('/admin/home', (new HomeControllerFactory($this))->getController() );
        $this->mount('/admin/logs', (new LogsControllerFactory($this))->getController() );
        $this->mount('/admin/execute', (new ExecuteControllerFactory($this))->getController() );
    }


    public function registerLogError($message)
    {
        $date = new \Datetime();

        // create a log channel
        $log = new Logger('App');
        $hourControl = $date->format('Y-m-d-H');
        $fileName = 'error_app_'.$hourControl.'.txt';

        $log->pushHandler(new StreamHandler('../app/logs/'.$fileName, Logger::WARNING));
        // add records to the log
        $log->error($message);
    }

}