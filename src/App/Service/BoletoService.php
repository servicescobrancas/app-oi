<?php

namespace App\Service;

use App\Entities\Client;
use Dompdf\Dompdf;

/**
 *
 * Class PanelService
 * @package App\Service
 */
class BoletoService extends BaseService
{
    const MINIMUM_INVOICES = 1;

    public function printRequest(Client $client, $contract, $boleto)
    {
        $parameters = [
            'Id'  => $boleto['boleto_id'],
            'Nome'  => $client->getCompleteName(),
            'Cpf'  => $client->getCpf(),
            'Email'  => "",
            'Phone'  => $client->getPhone(),
            'Titulo'  => $boleto['boleto_id'],
            'Valor'  => $boleto['Valor'],
            'Carteira'  => $client->getWallet(),
            'Contrato'  => $client->getContract(),
            'DataBoleto'  => $boleto['Vencimento'],
            'DataBoletoOrignal'  => $boleto['Vencimento'],
            'LinhaDigitavel'  => $boleto['LinhaDigitavel'],
            'Dias' => 0,
            'IdOrigem' => $client->getOrigin(),
            'Token' => $client->getToken()
        ];


        //$this->generateBoleto($parameters);

        $data = $this->getReturnData('/boleto/sms-oi', $parameters);

        if(!$data['Status']) {
            $this->message = $data['Message'];
            $this->status = false;
        }

        return true;
    }



    public function emailRequest(Client $client, $contract, $boleto)
    {
        $parameters = [
            'Id'  => $boleto['boleto_id'],
            'Nome'  => $client->getCompleteName(),
            'Cpf'  => $client->getCpf(),
            'Email'  => $client->getEmail(),
            'Phone'  =>'',
            'Titulo'  => $boleto['boleto_id'],
            'Valor'  => $boleto['Valor'],
            'Carteira'  => $client->getWallet(),
            'Contrato'  => $client->getContract(),
            'DataBoleto'  => $boleto['Vencimento'],
            'DataBoletoOrignal'  => $boleto['Vencimento'],
            'LinhaDigitavel'  => $boleto['LinhaDigitavel'],
            'Dias' => 0,
            'IdOrigem' => $client->getOrigin(),
            'Token' => $client->getToken()
        ];

        $data = $this->getReturnData('/boleto/email-oi', $parameters);

        if(!$data['Status']) {
            $this->message = $data['Message'];
            $this->status = false;
        }

        return true;
    }

    public function generateBoleto($data, $codigoBarra, $view)
    {
        $html = $this->getHtmlBoleto($data, $codigoBarra, $view);
        $date = new \DateTime();

        $arquivoNome = $data['product']['Contrato']. '.'.$date->format('YmdHis');
        $pathTemplate = 'C:\\www\\app-oi\\public\\boletos\\';
        if (PROD || TEST) {
            $pathTemplate = 'C:\\inetpub\\wwwroot\\AppOi\\public\\boletos\\';
        }

        $pars = [
            'Html' => $html,
            'Nome' => $arquivoNome . '.pdf',
            'Caminho' => $pathTemplate
        ];

        $filePdf = $this->getReturnData('/boleto/oi-boleto-html', $pars);
        $filePdf = str_replace('"', '', $filePdf);
        return str_replace("\\\\", "\\", $filePdf);
    }

    public function getHtmlBoleto($data, $codigoBarra, $view)
    {
        $pathTemplate = 'C:\\www\\app-oi\\public\\boletos\\';
        if (PROD || TEST) {
            $pathTemplate = 'C:\\inetpub\\wwwroot\\AppOi\\public\\boletos\\';
        }

        $pars = [
            'path_template' => $pathTemplate,
            'cliente_nome' => $data['client']['NomeConsumidor'],
            'contrato' => $data['product']['Contrato'],
            'linha_digital' => $data['boleto']['LinhaDigitavel'],
            'valor_boleto' => $data['boleto']['Valor'],
            'vencimento' =>  $data['boleto']['Vencimento'],
            'vencimento_original' =>  $data['boleto']['VencOrigial'],
            'codigo_barra_img' => $codigoBarra
        ];

        return $view->render('site/boletos/modelo.1.twig', $pars);
    }

    public function getCodigoBarra($data)
    {
        $path = 'C:\\www\\app-oi\\public\\boletos\\';
        if (PROD || TEST) {
            $path = 'C:\\inetpub\\wwwroot\\AppOi\\public\\boletos\\';
        }

        $linhaDigital = str_replace(['-', '.',  '_', ' '], '', $data['LinhaDigitavel']);

        $pars = [
            'Path' => $path,
            'LinhaDigitavel' => $linhaDigital
        ];
        $codigoBarra = $this->getReturnData('/boleto/oi-boleto', $pars);
        return str_replace('"', '', $codigoBarra);
    }

    public function registerEmail($clientData, $boleto, $contract, $arquivo)
    {
        $dominio = 'oipaguefacil.com.br';
        $assunto = 'Boleto OI';

        $corpo = "<b>Olá " . $clientData['NomeConsumidor'] . " <br/>" .
                "Conta: " . $contract . "<br/></b>" .
                "Segue linha digitável para pagamento de sua fatura OI com vencimento em " . $boleto['Vencimento'] . "." .
                "<br/>" .
                $boleto['LinhaDigitavel'] . " <br/>" .
                "Em caso de dúvidas, entre em contato conosco:<br/><b> " .
                "Central de negociações e informações OI: <a href='tel:10611'>106 11</a></b><br/>" .
                "Horário de Atendimento: de domingo à domingo das 07:00h às 23:00h.<br/>";

        $carteira = 1;
        $arquivo = str_replace('\\', '\\\\', $arquivo);

        $sql  = ' INSERT INTO fila_envio_email ( ';
        $sql .= ' dominio, destinatario, anexos, datacad, assunto, corpo, idcarteira) ';
        $sql .= ' VALUES ("'.$dominio.'", "'.$clientData['email'].'", "'.$arquivo.'", NOW(), "'.$assunto.'", "'.$corpo.'", "'.$carteira.'")';

        $this->executeSql($sql, null);

        return true;
    }

    public function registerPhone($clientData,$boleto)
    {
        $phone = $clientData['phone'];
        if ($phone != '') {

            $boleto['Valor'] = str_replace(' ', '', $boleto['Valor']);
            $boleto['Valor'] = str_replace('R$R$', 'R$', $boleto['Valor']);

            $phone = str_replace(['-', '.', '(', ' '], '', $phone);
            $boleto['LinhaDigitavel'] = str_replace(['-', '.', '(', ' '], '', $boleto['LinhaDigitavel']);

            $mensagem  = 'Cliente OI:Segue linha digitavel para pgto. da sua negociacao via portal:';
            $mensagem .= $boleto['LinhaDigitavel'];
            $mensagem .= ' Venc:' . substr($boleto['Vencimento'], 0, 5);
            $mensagem .= ' Valor:' . $boleto['Valor'];

            $idcarteira = 1;
            $idConfigSMS = 4;

            $sql  = ' INSERT INTO fila_envio_sms ( ';
            $sql .= ' telefone, datacad, mensagem, idcarteira, idConfigSMS) ';
            $sql .= ' VALUES ("'.$phone.'", NOW(), "'.$mensagem.'", "'.$idcarteira.'", "'.$idConfigSMS.'")';

            $this->executeSql($sql, null);
        }

        return true;
    }

    public function getPathFiles()
    {
        return 'C:\\Temp\\boletos\\';
    }

    public function smsRequest(Client $client, $contract, $boleto)
    {
        $parameters = [
            'Id'  => $boleto['boleto_id'],
            'Nome'  => $client->getCompleteName(),
            'Cpf'  => $client->getCpf(),
            'Email'  => "",
            'Phone'  => $client->getPhone(),
            'Titulo'  => $boleto['boleto_id'],
            'Valor'  => $boleto['Valor'],
            'Carteira'  => $client->getWallet(),
            'Contrato'  => $client->getContract(),
            'DataBoleto'  => $boleto['Vencimento'],
            'DataBoletoOrignal'  => $boleto['Vencimento'],
            'LinhaDigitavel'  => $boleto['LinhaDigitavel'],
            'Dias' => 0,
            'IdOrigem' => $client->getOrigin(),
            'Token' => $client->getToken()
        ];

        $data = $this->getReturnData('/boleto/sms-oi', $parameters);

        if(!$data['Status']) {
            $this->message = $data['Message'];
            $this->status = false;
        }

        return true;
    }
}