<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;

use GuzzleHttp\Client;

use App\Entities\Client as ClientEntity;
use Mockery\CountValidator\Exception;

use App\Math\Convert;
use App\Util\StringUtil;

class BaseService
{

    /**
     * Data contains info about entity
     * @var array
     */
    protected $data = [];

    /**
     * Client in session context.
     * @var array
     */
    protected $client;

    /**
     * Flag for tracking status of operation
     * @var bool
     */
    protected $status = true;

    /**
     * Message from result operation
     * @var string
     */
    protected $message = '';

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var array
     */
    protected $headersApi = [];

    /**
     * BaseService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->init();
    }

    public function init()
    {
        // implement actions for simulate constructor class
    }

    /**
     * @param array $data
     * @return bool
     */
    public function newRegister(array $data)
    {}

    /**
     * @param array $data
     * @return bool
     */
    public function updateRegister(array $data)
    {}

    /**
     * @param int $id
     * @return bool
     */
    public function deleteRegister($id)
    {}

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getData()
    {
        return $this->data;
    }

    public function registerPage($page, $clientData, $httpCookie, $ip, $lastPage = 0)
    {
        $cpf = '';
        $token = '';
        $carteira = 500;

        if ($clientData != null) {
            $cpf = isset($clientData['DocumentoConsumidor']) ? $clientData['DocumentoConsumidor'] : null;
            $token = isset($clientData['Token']) ? $clientData['Token'] : null;
            $carteira = 500;
        }

        $sql  = ' INSERT INTO visualizao_paginas ( ';
        $sql .= ' cpf, dominio, pagina, data, ip, http_cookie, token, idcarteira) ';
        $sql .= ' VALUES ("'.$cpf.'", "'.DOMAIN.'", "'.$page.'", NOW(), "'.$ip.'", "'.$httpCookie.'", "'.$token.'", "'.$carteira.'")';

        $this->executeSql($sql, null);

        $id = 0;
        return $id;
    }

    public function getReturnDataQueryString($uri, $query)
    {
        $urlRequest = $this->getUrlWs() . $uri . '?' . $query;

        if(isset($_GET['view_url'])) {
            echo $urlRequest."\n";
        }

        $client = new Client(['proxy' => '']);

        try {

            $response = $client->request('GET', $urlRequest);

            if($response->getStatusCode()) {
                $body = $response->getBody();
                $remainingBytes = $body->getContents();

                if(!strstr($remainingBytes,'\\')) {
                    $data = json_decode($remainingBytes, true);
                    $this->registerInfoCallApi($uri, $query, $data);
                    return $data;
                }

                $str = json_decode($remainingBytes, true);
                if(is_array($str)) {
                    $this->registerInfoCallApi($uri, $query, $str);
                    return $str;
                }

                $data = json_decode($str, true, 1024);
                $this->registerInfoCallApi($uri, $query, $data);
                return $data;
            }
        } catch(\Exception $e) {
            $this->registerErrorApi($uri, $query, $e->getMessage());
            $dataError = [
                'Status' => false,
                'Message' => 'Erro no sistema.'
            ];

            return $dataError;
        }

        return false;
    }

    /**
     * @param array $pars
     */
    public function setHeaders($pars = [])
    {
        $this->headersApi['Content-Type'] = 'application/json';
        $this->headersApi['api-senha'] = API_PASS;
        $this->headersApi['api-usuario'] = API_USER;

        if (isset($pars['guid'])) {
            $this->headersApi['api-guid'] = $pars['guid'];
        }

        if (filter_input(INPUT_GET, 'headers') == 1) {
            d($this->headersApi, 'var_dump', false);
        }

        if (filter_input(INPUT_GET, 'client') == 1) {
            d( $this->getClientData(), 'var_dump', false);
        }
    }

    public function getHeaders()
    {
        return $this->headersApi;
    }

    public function getDataApi($uri, $pars)
    {
        if (isset($pars['DocumentoConsumidor'])) {
            $doc = $pars['DocumentoConsumidor'];

        } else {
            $clientData = $this->getClientData();
            $doc = $clientData['DocumentoConsumidor'];
        }

        $id_monitor = $this->apiTimeControl(['uri'=> $uri, 'doc' => $doc, 'pars' => json_encode($pars)] );
        $urlRequest = $this->getUrlWs() . $uri;
        $client = new Client();
        $data = [];
        try {
            $response = $client->request('POST', $urlRequest, [
                'headers' => $this->headersApi,
                'json' => $pars]
            );
            if($response->getStatusCode()) {
                $body = $response->getBody();
                $remainingBytes = $body->getContents();
                $data = json_decode($remainingBytes, true, 1024);
                $this->registerInfoCallApi($uri, $pars, $data);

                if ($id_monitor)
                    $this->apiTimeControl(['id'=> $id_monitor, 'return' =>  json_encode($data)], true);
                return $data;
            }
        } catch(\Exception $e) {
            $this->registerErrorApi($uri, $pars, $e->getMessage());

            $dataError = [
                'Erro' => true,
                'Message' => 'Erro no sistema.'
            ];

            if ($id_monitor) {
                $this->apiTimeControl(['id'=> $id_monitor, 'return' =>  json_encode(['error_api' => '1', 'message' => $e->getMessage()]) ], true);
            }

            return $dataError;
        }

        if ($id_monitor) {
            if (count($data) == 0) {
                $data['error_api'] = "Não encontrou o registro";
            }
            $this->apiTimeControl(['id' => $id_monitor, 'return' => json_encode($data)], true);
        }

        return $data;
    }

    function apiTimeControl($dados, $update = false)
    {
        try {
            if ($update) {
                $this->em->getConnection()->update('monitor_api', ['saida' => date('Y-m-d H:i:s'), 'retorno' => $dados['return']], ['id' => $dados['id']]);
            } else {
                $this->em->getConnection()->insert('monitor_api', ['metodo' => $dados['uri'], 'entrada' => date('Y-m-d H:i:s'), 'cpf_cnpj' => $dados['doc'], 'parametros' => $dados['pars']]);
                return $this->em->getConnection()->lastInsertId();
            }
        } catch (Exception $e) {
            echo $e->getMessage(); exit;
        }
    }

    /**
     * @param $sql
     * @param string $get
     * @return array|bool|mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executeSql($sql, $get = 'unique')
    {
        $stmt = $this->em->getConnection()->prepare($sql);
        try {
            $stmt->execute();
        }
        catch(\Exception $e) {
            echo $e->getMessage();
            exit();
        }

        if ($get == 'all' ) {
            return $stmt->fetchAll();
        }

        if ($get == null ) {
            return true;
        }

        return $stmt->fetch();
    }

    public function downloadFile($uri, $pars, $fileName)
    {
        $clientData = $this->getClientData();
        $doc = $clientData['DocumentoConsumidor'];
        $id_monitor = $this->apiTimeControl(['uri'=> $uri, 'doc' => $doc, 'pars' => json_encode($pars)] );

        $urlRequest = $this->getUrlWs() . $uri;
        $client = new Client();
        $data = [];
        try {
            $client->request('POST', $urlRequest, [
                    'headers' => $this->headersApi,
                    'json' => $pars,
                    'sink' => $fileName]
            );

            if ($id_monitor) {
                $this->apiTimeControl(['id'=> $id_monitor, 'return' =>  json_encode($data)], true);
            }

        } catch(\Exception $e) {
            $this->registerErrorApi($uri, $pars, $e->getMessage());
            $dataError = [
                'Status' => false,
                'Message' => 'Erro no sistema.'
            ];

            if ($id_monitor) {
                $this->apiTimeControl(['id'=> $id_monitor, 'return' =>  json_encode(['message' => $e->getMessage()]) ], true);
            }

            return $dataError;
        }

        if ($id_monitor) {
            $this->apiTimeControl(['id'=> $id_monitor, 'return' =>  json_encode($data)], true);
        }

        return $data;
    }

    public function getReturnData($uri, $pars)
    {
        $urlRequest = URL_WS_SRV . $uri;
        $client = new Client(['proxy' => '']);

        $pars['dominio']  = DOMAIN;

        try {

            $response = $client->request('POST', $urlRequest,
                ['form_params' => $pars]
            );

            if($response->getStatusCode()) {
                $body = $response->getBody();
                $remainingBytes = $body->getContents();

                if(!strstr($remainingBytes,'\\')) {
                    $data = json_decode($remainingBytes, true);
                    $this->registerInfoCallApi($uri, $pars, $data);
                    return $data;
                }

                $str = json_decode($remainingBytes, true);
                if(is_array($str)) {
                    $this->registerInfoCallApi($uri, $pars, $str);
                    return $str;
                }

                $data = json_decode($str, true, 1024);
                $this->registerInfoCallApi($uri, $pars, $data);
                return $data;
            }
        } catch(\Exception $e) {
            $this->registerErrorApi($uri, $pars, $e->getMessage());
            $dataError = [
                'Status' => false,
                'Message' => 'Erro no sistema.'
            ];

            return $dataError;
        }

        return false;
    }

    public function registerInfoCallApi($uri, $pars, $dataReturn)
    {
        if($uri == '/view/page') {
            return;
        }

        $date = new \Datetime();
        $hourControl = $date->format('Y-m-d-H');
        $fileName = 'log_'.$hourControl.'.txt';

        $logDesc  = 'URI: '.$uri . ' - ';
        $logDesc .= 'DATETIME:  '.$date->format('Y-m-d H:i:s'). "\n";
        $logDesc .= 'PARS: ' . json_encode($pars)."\n";
        $logDesc .= 'RETURN: ' . json_encode($dataReturn)."\n\n";

        $f = fopen('../.../../app/logs/'.$fileName, 'a+');
        fwrite($f, $logDesc);
        fclose($f);
    }

    public function registerErrorApi($uri, $pars, $error)
    {
        if($uri == '/view/page') {
            return;
        }

        $date = new \Datetime();
        $hourControl = $date->format('Y-m-d-H');
        $fileName = 'error_'.$hourControl.'.txt';

        $logDesc  = 'URI: '.$uri . ' - ';
        $logDesc .= 'DATETIME:  '.$date->format('Y-m-d H:i:s'). "\n";
        $logDesc .= 'PARS: ' . json_encode($pars)."\n";
        $logDesc .= 'ERROR: ' . $error."\n\n";

        $f = fopen('../.../../app/logs/'.$fileName, 'a+');
        fwrite($f, $logDesc);
        fclose($f);
    }

    public function getClientData()
    {
        return $this->client;
    }

    public function setClientData($client)
    {
        return $this->client = $client;
    }

    public function getUrlWs()
    {
        return URL_WS;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param int $id
     * @return object
     */
    public function getItem($id)
    {}

    /**
     * @return array
     */
    public function getList()
    {}

    public function formatDate($dateInput)
    {
        $dataVenc = $dateInput;
        if(strstr($dataVenc, " ") !=  "") {
            $partsVenc = explode(" ", $dataVenc);
            $dataVenc = $partsVenc[0];
        }

        if(strstr($dataVenc, "T") !=  "") {
            $partsVenc = explode("T", $dataVenc);
            $dataVenc = $partsVenc[0];
        }

        $partsDate = explode('/', $dataVenc);
        if(strlen($partsDate[0]) < 2) {
            $partsDate[0] = '0'.$partsDate[0];
        }

        if(strlen($partsDate[1]) < 2) {
            $partsDate[1] = '0'.$partsDate[1];
        }

        return $partsDate[0] . '/' . $partsDate[1] . '/' . $partsDate[2];
    }

    public function formatNumberToFloat($number)
    {
        $number = str_replace('R$ ', '', $number);
        $number = str_replace(',', '.', $number);

        return floatval($number);
    }

    public function formatValue($number)
    {
        setlocale(LC_MONETARY, 'pt_BR');
        return 'R$ '.\number_format( $number, 2, ',', '.');
    }


    public function getCustom($pars)
    {
        $uri = '/Contas/ConsultarDadosCustomizados';
        $parameters = [
            'Documento' => $pars['DocumentoConsumidor'],
            'ToKen' => $pars['ToKen']
        ];
        $returnApi = $this->getDataApi($uri, $parameters);

        $this->status = ($returnApi['Erro'] === false);
        $valuesCustom = [];
        if ($this->status) {
            $values = $returnApi['Data'];
            foreach ($values as $key => $items) {
                $valuesItems = $items['Valores'];

                $mount = [];
                $number = 0;
                foreach ($valuesItems as $keyValue =>  $itemValue) {
                    $name  = StringUtil::getSlugCustom($itemValue['Nome']);
                    $value = trim($itemValue['Valor']);
                    if ($name == 'NUMERO_PARC') {
                        $number = $value;
                    } else if ($name == 'FATURA_VALOR' || $name == 'FATURA_ENCARGOS' ||  $name == 'FATURA_TOTAL' ||  $name == 'DESC_TOTAL') {
                        $value = Convert::formatNumberToFloat($value);
                    }
                    $mount[$name] = $value;
                }
                $valuesCustom[$number] = $mount;
            }
        }

        return $valuesCustom;
    }
}
