<?php

namespace App\Service;

use App\Entities\Client;
use App\Math\Convert;
use App\Util\StringUtil;

/**
 *
 * Class PanelService
 * @package App\Service
 */
class EnterService extends BaseService
{
    private $isCampaign = false;
    private $isInvoicesDeal = false;
    private $isExceptionWallet = false;
    private $isExceptionDeal = false;
    private $isExceptionLetter = false;

    /**
     * @var array
     */
    private $customData = [];

    /**
     * Credores elegíveis para pagamento fatura a fatura e consolidado.
     * @var array
     */
    private $creditorsInvoices = [58, 157, 183, 230];

    /**
     * @param $pars
     * @return array
     */
    function loadPendencies($pars)
    {
        $uri = '/contas/consultardividas';
        $token = $pars['Token'];

        if (filter_input(INPUT_GET, 'clear') == 1) {
            $this->clearDeals($pars['DocumentoConsumidor']);
        }

        $pars = [
            'DocumentoConsumidor' => $pars['DocumentoConsumidor'],
            'ToKen' => $token
        ];

        $this->customData = $this->getCustom($pars);

        $returnApi = $this->getDataApi($uri, $pars);
        if (filter_input(INPUT_GET, 'api') ) {
            debug($pars, '/contas/consultardividas', 'Parâmetros:', false);
            debug($returnApi, '/contas/consultardividas', 'Retorno API:', false);
        }

        $products = [];
        $this->status = ($returnApi['Erro'] === false);
        if ($this->status) {
            $productsList = $returnApi['Data'];
            foreach ($productsList as $key => $product) {
                $contractNumber = $productsList[$key]['Contrato'];

                $totalContract = 0;
                $customNumber = 0;
                foreach ($productsList[$key]['Parcelas'] as $keyParcel => $parcel) {

                    $parcelData = $productsList[$key]['Parcelas'][$keyParcel];

                    $parcelData['Valor'] = $this->formatValue($parcel['Valor']);

                    $dataVenc = StringUtil::convertDateDB($parcel['Vencimento']);
                    $refDate = substr($dataVenc, 3, strlen($dataVenc));
                    $parcelData['Vencimento'] = $dataVenc;
                    $parcelData['RefDate'] = $refDate;

                    $number = $parcelData['Numero'];
                    $parcelData['custom'] = [];
                    if (isset($this->customData[$number])) {
                        $custom = $this->customData[$number];
                        $parcelData['custom'] = $custom;

                        if ( isset($custom['FATURA_ENCARGOS']) &&  isset($custom['DESC_TOTAL']) && $custom['DESC_TOTAL'] > 0) {
                            ++$customNumber;
                        }
                    } else {
                       // $totalContract += $parcel['Valor'];
                    }

                    $totalContract += $parcel['Valor'];
                    $productsList[$key]['Parcelas'][$keyParcel] = $parcelData;
                }

                $productsList[$key]['ValorContract'] = $totalContract;
                $productsList[$key]['ValorContractFormat'] = $this->formatValue($totalContract);


                $aux = $this->getAuxData([
                    'Contrato' => $contractNumber,
                    'Token' => $token,
                    'IdCredor' => $product['IdCredor']
                ]);

                if (in_array($product['IdCredor'], $this->creditorsInvoices)) {
                    $this->isInvoicesDeal = (count($aux) > 0);
                    if ($product['IdCredor'] == 58 || $product['IdCredor'] == 230) {
                        $this->isCampaign = true;
                    } elseif ($product['IdCredor'] == 157 ) {
                        $this->isExceptionWallet = true;
                    } elseif ($product['IdCredor'] == 183 && $customNumber == count($productsList[$key]['Parcelas'])) {
                        $this->isExceptionLetter = true;
                    }elseif ($product['IdCredor'] == 183 && count($aux) > 1) {
                        $this->isExceptionDeal = true;
                    }
                }

                $productsList[$key]['Aux'] = $aux;
                $productsList[$key]['IsCampaign'] = $this->isCampaign;
                $productsList[$key]['isInvoicesDeal'] = $this->isInvoicesDeal;
                $productsList[$key]['isExceptionWallet'] = $this->isExceptionWallet;
                $productsList[$key]['isExceptionLetter'] = $this->isExceptionLetter;
                $productsList[$key]['isExceptionDeal'] = $this->isExceptionDeal;

                $this->isCampaign = false;
                $this->isExceptionWallet = false;
                $this->isExceptionLetter = false;
                $this->isExceptionDeal = false;

                $products[$contractNumber] = $productsList[$key];
            }
        }

        if (filter_input(INPUT_GET, 'code') ) {
            debug($products, '/contas/consultardividas', 'Dados tratados:');
        }

        return $products;
    }

    /**
     * @param $contract
     * @param $cpfCnpj
     * @return bool
     */
    public function checkClientLetter($contract, $cpfCnpj)
    {
        $sqlLetter  = "SELECT count(id) as total FROM clientes_carta where contrato_tit = '".$contract."' and  cpf_cnpj = '".$cpfCnpj."' ";
        $rsLetter = $this->executeSql($sqlLetter);

        try {
            return ($rsLetter['total'] > 1);

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $pars
     * @return array
     */
    public function getAuxData($pars)
    {
        $uri = '/Contas/ConsultarDadosAuxiliares';
        $parameters = [
            'Contrato' => $pars['Contrato'],
            'ToKen' => $pars['Token']
        ];

        $returnApi = $this->getDataApi($uri, $parameters);
        $status = ($returnApi['Erro'] === false);
        $auxData = [];
        if ($status) {
            foreach ($returnApi['Data'] as $keyItem => $item) {
                if ($pars['IdCredor'] != 183) {
                    $keyAux = $item['ValorChave'];
                } else {
                    $keyAux = $item['ValorChave'];
                }

                if (count($item['Valores']) == 0) {
                    continue;
                }

                $auxData[$keyAux] = [];
                foreach ($item['Valores'] as $key => $itemValor) {
                    $keyName = StringUtil::getSlugCustom($itemValor['Nome']);
                    $valueItem = trim($itemValor['Valor']);

                    if ($keyName == 'Valororiginaldebito' || $keyName == 'Valormulta' || $keyName == 'Valorjuros' || $keyName == 'Valordesconto') {
                        $valueItem = (int) $valueItem;
                        if ($valueItem > 0) {
                            $valueItem = substr($valueItem, 0, strlen($valueItem) - 2) . '.' . substr($valueItem, strlen($valueItem) - 2, strlen($valueItem));
                        }

                        $auxData[$keyAux][$keyName] = $valueItem;
                        $auxData[$keyAux][$keyName.'Format'] = $this->formatValue($valueItem);
                    } else if ($keyName == 'Valor_final') {
                        $valorFinal = str_replace(',', '.', $valueItem);
                        $auxData[$keyAux][$keyName] = $valorFinal;
                        $auxData[$keyAux][$keyName.'_Format'] = $this->formatValue($valorFinal);

                    } else if ($keyName == 'Tipo_fase') {
                        $this->isCampaign = ($valueItem == 'CAMPANHA MOVEL' || $valueItem == 'CAMPANHA FIXA');
                    }

                    $auxData[$keyAux][$keyName] = $valueItem;
                }
            }
        }

        return $auxData;
    }

    public function getAuxDataInvoices($pars)
    {
        $uri = '/Contas/ConsultarDadosAuxiliares';
        $parameters = [
            'Contrato' => $pars['Contrato'],
            'ToKen' => $pars['Token']
        ];

        $returnApi = $this->getDataApi($uri, $parameters);
        $status = ($returnApi['Erro'] === false);
        $auxData = [];
        $i = 0;
        if ($status) {
            foreach ($returnApi['Data'] as $keyItem => $item) {
                $auxData[$item['ValorChave']] = [];
                foreach ($item['Valores'] as $key => $itemValor) {
                    $keyName = StringUtil::getSlugCustom($itemValor['Nome']);
                    $valueItem = trim($itemValor['Valor']);

                    if ($keyName == 'Valororiginaldebito' || $keyName == 'Valormulta' || $keyName == 'Valorjuros' || $keyName == 'Valordesconto') {
                        $valueItem = (int) $valueItem;
                        if ($valueItem > 0) {
                            $valueItem = substr($valueItem, 0, strlen($valueItem) - 2) . '.' . substr($valueItem, strlen($valueItem) - 2, strlen($valueItem));
                        }
                        $auxData[$item['ValorChave']][$keyName] = $valueItem;
                        $auxData[$item['ValorChave']][$keyName.'Format'] = $this->formatValue($valueItem);
                    } else if ($keyName == 'Valor_final') {
                        $valorFinal = str_replace(',', '.', $valueItem);
                        $auxData[$item['ValorChave']][$keyName] = $valorFinal;
                        $auxData[$item['ValorChave']][$keyName.'_Format'] = $this->formatValue($valorFinal);

                    } else if ($keyName == 'Cod_barra') {
                        ++$i;
                    }

                    $auxData[$item['ValorChave']][$keyName] = $valueItem;
                }
            }
        }

        $this->isInvoicesDeal = ($i > 0);
        return $auxData;
    }

    public function getDeals($client)
    {
        $doc = $client['DocumentoConsumidor'];

        $sql  = ' SELECT documento, contrato, dados, data_cad, data_vencimento ';
        $sql .= ' FROM acordo ';
        $sql .= ' WHERE  documento = "'. $doc .'"';

        $rs = $this->executeSql($sql, 'all');

        $deals = [];
        if ($rs) {
            foreach ($rs as $value) {
                $data = json_decode($value['dados'], true);
                $deals[] = $data;
            }
        }
        return $deals;
    }

    /**
     * @param $client
     * @return array
     */
    public function getDealsActives($client)
    {
        $uri = '/negociacao/ativas';
        $pars = [
            'DocumentoConsumidor' => $client['DocumentoConsumidor'],
            'ToKen' => $client['Token'],
        ];

        $returnApi = $this->getDataApi($uri, $pars);

        $deals = [];
        if (!$returnApi['Erro']) {
            $dealsNeg = $returnApi['Data']['Negociacoes'];
            foreach ($dealsNeg as $key => $value) {
                $deals[$value['Contrato']] = $value;
            }
        }

        return $deals;
    }

    /**
     * @param $doc
     */
    public function clearDeals($doc)
    {
        $uri = '/negociacao/cancelar';
        $pars = [
            'DocumentoConsumidor' => $doc
        ];

        $returnApi = $this->getDataApi($uri, $pars);
        $this->status = ($returnApi['Erro'] === false);
    }

    /**
     * @return array
     */
    public function getCustomData()
    {
        return $this->customData;
    }

    /**
     * @param $data
     * @param $pars
     */
    public function registerInfo($data, $pars)
    {
        $phone = str_replace([' ', '-', '(', ')'], '', $pars['phone']);
        $data['date'] = '';

        $sql  = ' INSERT INTO clientes ( ';
        $sql .= ' token, documento_consumidor, nome_consumidor,
                  guid, data_aniversario, email,
                  telefone, data_cad) ';

        $sql .= ' VALUES (
                  "'.$data['Token'].'", "'.$data['DocumentoConsumidor'].'", "'.$data['NomeConsumidor'].'",
                  "'.$data['Guid'].'", "'.$data['date'].'", "'.$pars['email'].'",
                  "'.$phone.'", NOW())';

        $this->executeSql($sql, null);
    }
}