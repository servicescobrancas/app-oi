<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AccessService
 * @package App\Service
 */
class ApiService extends BaseService
{

    public function download($query)
    {
        $data =  $this->getReturnDataQueryString('/boleto/download-servico', $query);
        return $data;
    }

    public function sms($query)
    {
        return $this->getReturnDataQueryString('/boleto/sms-servico', $query);
    }

    public function email($query)
    {
        return $this->getReturnDataQueryString('/boleto/email-servico', $query);
    }

}