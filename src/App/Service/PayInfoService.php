<?php

namespace App\Service;

use Doctrine\ORM\EntityManager;


use App\Service\StringUtil;
use App\Math\Convert;
use Spipu\Html2Pdf\Html2Pdf;

/**
 *
 * Class PayInfoService
 * @package App\Service
 */
class PayInfoService extends BaseService
{

    const FOLDER_EMAIL = __DIR__.'\\..\\..\\..\\views\\site\\email\\';

    const RANGE_DAYS = 5;

    /**
     * @var int
     */
    private $dealId = 0;

    private $boletoData = [];

    public function deals($clientData, $product, $dateSel = '')
    {
        $uri = '/negociacao/iniciar';
        $pars = [
            'IdConta' => $product['Id'],
            'ToKen' => $clientData['Token']
        ];

        $returnApi = $this->getDataApi($uri, $pars);

        $deals = [];
        $this->status = (!isset($returnApi['Status']) && $returnApi['Erro'] === false);
        if ($this->status) {
            $contract = $product['Contrato'];
            $this->dealId = $returnApi['Data']['IdNegociacao'];

            $deals = $this->getDealOptions($this->dealId, $contract, $dateSel);
        }

        if (filter_input(INPUT_GET, 'deals') == 1) {
            debug($pars,  '/negociacao/iniciar', 'Parâmetros:', false);
            debug($returnApi, '/negociacao/iniciar', 'Retorno API:', false);
        }

        return $deals;
    }

    public function getDealOptions($dealId, $contract, $dateSel = '')
    {
        $uri = '/negociacao/opcoes';
        $pars = [
            'IdNegociacao' => $dealId,
            'DataDoCalculo' => StringUtil::convertDateBR($dateSel)
        ];

        $returnApi = $this->getDataApi($uri, $pars);
        if (filter_input(INPUT_GET, 'options') == 1) {
            debug($pars,  '/negociacao/opcoes', 'Parâmetros:', false);
            debug($returnApi, '/negociacao/opcoes', 'Retorno API:', false);
        }

        $this->status = (!isset($returnApi['Status']) && $returnApi['Erro'] === false);

        $dealOptions = [];
        if ($this->status) {
            $options = $returnApi['Data']['Opcoes'];
            $valorOriginal = 0;

            foreach($options as $key => $deal) {
                $deal['Contrato'] = $contract;
                $deal['DataDoCalculo'] = str_replace('T00:00:00', '', $deal['DataDoCalculo']);
                $deal['DataDoCalculo'] =  StringUtil::convertDateDB($deal['DataDoCalculo']);
                $deal['Desconto'] = $deal['DescontoConcedido'];
                $valor = $deal['ValorAVista'];
                if ($valor == 0) {
                    if ($deal['ValorDeEntrada'] == 0) {
                        $valor = $deal['ValorDemaisParcelas'];
                    } else {
                        $valor = $deal['ValorDeEntrada'];
                    }
                }

                if ($key == 0) {
                    $valorOriginal = $deal['ValorAVista'];
                } else if($deal['QuantidadeDeParcelas'] == 1)  {
                    $deal['DescontoConcedido'] = $valorOriginal - $deal['ValorDemaisParcelas'];
                } else {
                    $total  = $deal['ValorDeEntrada'];
                    $total += $deal['ValorDemaisParcelas'] * ($deal['QuantidadeDeParcelas'] - 1);
                    $valor = $total;
                    $deal['DescontoConcedido'] = $valorOriginal - $total;
                }

                $deal['Valor'] = Convert::formatValue($valor);
                $deal['ValorDemaisParcelas'] = Convert::formatValue($deal['ValorDemaisParcelas']);
                $deal['ValorDeEntrada'] = Convert::formatValue($deal['ValorDeEntrada']);
                $deal['DescontoConcedido'] = Convert::formatValue($deal['DescontoConcedido']);

                $dealOptions[$deal['Id']] = $deal;
            }
        }

        $contracts = array_keys($dealOptions);
        if (count($dealOptions) == 2 && $dealOptions[$contracts[1]]['InformacoesAdicionais'] == 'CAMP-AGRUP-OIMOVEL') {

            $deal = $dealOptions[$contracts[1]];
            $deal['Caso'] = 'CampAgroupOIMovel';
            $dealOptions = [];
            $dealOptions[$contracts[1]] = $deal;

        }  else if (count($dealOptions) == 2 && $dealOptions[$contracts[1]]['QuantidadeDeParcelas'] == 0) {

            $deal = $dealOptions[$contracts[0]];
            $deal['Caso'] = 'DuplicadoErro';
            $dealOptions = [];
            $dealOptions[$contracts[0]] = $deal;

        }

        if (filter_input(INPUT_GET, 'options') == 1) {
            debug($dealOptions, '/negociacao/opcoes', 'Dados tratados:', false);
        }

        return $dealOptions;
    }

    public function checkClientLetter($clientData, $currentProduct, $deal, $contract)
    {
//        if ($currentProduct['IdCredor'] == 183) {
//
//            d($currentProduct['Parcelas']);
//
//            return (count($currentProduct['Aux']) ==  '');
//
//            $cpfCnpj = $clientData['DocumentoConsumidor'];
//            $sqlLetter  = "SELECT count(id) as total FROM clientes_carta where contrato_tit = '".$contract."' and  cpf_cnpj = '".$cpfCnpj."' ";
//            $rsLetter = $this->executeSql($sqlLetter);
//
//            try {
//                return ($rsLetter['total'] > 1);
//
//            } catch (\Exception $e) {
//
//            }
//        }

        return false;
    }

    public function formatDealGroup($dealData, $custom)
    {
        $valorOriginal = $dealData['ValorAVista'] + $dealData['Desconto'];
        $valorPagamento = $dealData['ValorAVista'];
        $desconto = $valorOriginal - $valorPagamento;

        return [
            'deal_id' => $dealData['Id'],
            'valor_original' => Convert::formatValue($valorOriginal),
            'valor_pagamento' => Convert::formatValue($valorPagamento),
            'desconto' => Convert::formatValue($desconto)
        ];
    }

    public function formatInvoiceLetter($product)
    {
        $codBarra = '';
        $encargos = 0;
        $valor = 0;
        $total = 0;
        $desconto = 0;

        foreach ($product['Parcelas'] as $parcel) {

            $codBarra = str_replace([' ', '-', '.'], '', $parcel['custom']['COD_BARRA']);
            $encargos += $parcel['custom']['FATURA_ENCARGOS'];
            $valor += $parcel['custom']['FATURA_VALOR'];
            $total += $parcel['custom']['FATURA_TOTAL'];
            $desconto = $parcel['custom']['DESC_TOTAL'];
        }

        $valorPagar = ($valor - $desconto) + $encargos;

        $pars = [
            'Id' => $product['Contrato'],
            'codBarra' => $codBarra,
            'contrato' => $product['Contrato'],
            'valor' => Convert::formatValue($valor),
            'encargos' => Convert::formatValue($encargos),
            'valorAtualizado' => Convert::formatValue($total),
            'desconto' => Convert::formatValue($desconto),
            'valorPagar' => Convert::formatValue($valorPagar)
        ];

        return $pars;
    }

    public function formatInvoices($product)
    {
        $parcels = $product['Parcelas'];
        $auxData = $product['Aux'];

        if ($product['IdCredor'] == 157) {
            foreach ($parcels as $key => $parcel) {
                $auxParcel = $auxData[$parcel['Numero']];

                $parcel['codBarraFormat'] = str_replace([' ', '-', '.'], '', $auxParcel['Codigo_barras']);

                $valorCobrado = (double)str_replace(',', '.', $auxParcel['Valor_cobrado']);

                $parcel['Valor_final_Format'] = Convert::formatValue($valorCobrado);

                $parcel['Datavencimentofatura'] = $parcel['Vencimento'];
                $parcel['Codigodebarrasdebito'] = $auxParcel['Codigo_barras'];

                $parcels[$key] = $parcel;
            }
        } else if ($product['IdCredor'] == 183) {
            foreach ($parcels as $key => $parcel) {

                if (!isset($auxData[$parcel['Numero']])) {
                    unset($parcels[$key]);
                    continue;
                }

                $auxParcel = $auxData[$parcel['Numero']];

                if (!isset($auxParcel['Cod_barra'])) {
                    unset($parcels[$key]);
                    continue;
                }

                $parcel['codBarraFormat'] = str_replace([' ', '-', '.'], '', $auxParcel['Cod_barra']);

                $saldoTotal = (double)str_replace(',', '.', $auxParcel['Saldo_total_aberto']);
                $valorOriginal = (double)str_replace(',', '.', $auxParcel['Valor_original']);
                $valorAjustado = (double)str_replace(',', '.', $auxParcel['Valor_ajustado']);

                $parcel['Valororiginaldebito'] = $valorOriginal;

                $valorDesconto = 0;
                if ($valorAjustado > 0) {
                    $valorDesconto = $valorOriginal - $valorAjustado;
                } else if ($saldoTotal > $valorOriginal) {
                    $valorDesconto = $saldoTotal - $valorOriginal;
                    $valorAjustado = $valorOriginal;
                    $valorOriginal = $saldoTotal;
                }

                $parcel['Valor_original_format'] = Convert::formatValue($saldoTotal);
                $parcel['Valordesconto'] = Convert::formatValue(0);

                $parcel['Valor_final'] = $valorOriginal;
                $parcel['Valor_final_Format'] = $parcel['Valor'];

                $parcel['Datavencimentofatura'] = $parcel['Vencimento'];
                $parcel['Codigodebarrasdebito'] = $auxParcel['Cod_barra'];

                $parcels[$key] = $parcel;
            }
        } else {

            foreach ($parcels as $key => $parcel) {

                $auxParcel = $auxData[$parcel['Numero']];

                if (!isset($auxParcel['Cod_barra'])) {
                    unset($parcels[$key]);
                    continue;
                }

                $parcel['codBarraFormat'] = str_replace([' ', '-', '.'], '', $auxParcel['Cod_barra']);

                $saldoTotal = (double)str_replace(',', '.', $auxParcel['Saldo_total_aberto']);
                $valorOriginal = (double)str_replace(',', '.', $auxParcel['Valor_original']);
                $valorAjustado = (double)str_replace(',', '.', $auxParcel['Valor_ajustado']);

                $parcel['Valororiginaldebito'] = $valorOriginal;

                $valorDesconto = 0;
                if ($valorAjustado > 0) {
                    $valorDesconto = $valorOriginal - $valorAjustado;
                } else if ($saldoTotal > $valorOriginal) {
                    $valorDesconto = $saldoTotal - $valorOriginal;
                    $valorAjustado = $valorOriginal;
                    $valorOriginal = $saldoTotal;
                }

                $parcel['Valor_original_format'] = Convert::formatValue($saldoTotal);
                $parcel['Valordesconto'] = Convert::formatValue(0);

                $parcel['Valor_final'] = $valorOriginal;
                $parcel['Valor_final_Format'] = $parcel['Valor'];

                $parcel['Datavencimentofatura'] = $parcel['Vencimento'];
                $parcel['Codigodebarrasdebito'] = $auxParcel['Cod_barra'];

                $parcels[$key] = $parcel;
            }
        }

        return $parcels;
    }

    public function formatInvoicesCampaign($product)
    {
        $parcels = $product['Parcelas'];
        $auxData = $product['Aux'];

        foreach ($parcels as $key => $parcel) {
            $auxParcel = $auxData[$parcel['Numero']];

            $parcel['Valororiginaldebito'] = $auxParcel['Valororiginaldebito'];
            $parcel['ValororiginaldebitoFormat'] = $auxParcel['ValororiginaldebitoFormat'];
            $parcel['Valormulta'] = $auxParcel['Valormulta'];
            $parcel['ValormultaFormat'] = $auxParcel['ValormultaFormat'];
            $parcel['Valorjuros'] = $auxParcel['Valorjuros'];
            $parcel['ValorjurosFormat'] = $auxParcel['ValorjurosFormat'];
            $parcel['Valordesconto'] = Convert::formatValue($auxParcel['Valordesconto']);


            $parcel['Valor_final'] = $auxParcel['Valor_final'];
            $parcel['Valor_final_Format'] = $auxParcel['Valor_final_Format'];
            $parcel['Datavencimentofatura'] = $auxParcel['Datavencimentofatura'];
            $parcel['Codigodebarrasdebito'] = $auxParcel['Codigodebarrasdebito'];

            $parcels[$key] = $parcel;
        }

        return $parcels;
    }

    public function confirmDeal($currentProduct, $option, $date)
    {
        $dealId = $currentProduct['deal_id'];

        $uri = '/negociacao/finalizar/opcao';
        $pars = [
            'IdNegociacao' => $dealId,
            'IdOpcaoDeCalculo' => $option
        ];

        $returnApi = $this->getDataApi($uri, $pars);
        $this->status = (!isset($returnApi['Status']) && $returnApi['Erro'] === false);
        $status = false;
        if ($this->status) {
            $status = $this->issueBoleto($currentProduct, $dealId);
        }
        return $status;
    }

    public function confirmPayment($clientData, $currentProduct, $dateConfirm)
    {
        $validateDate = $this->validateDateRegisterPayment($dateConfirm);

        if(!$validateDate['status']) {
            $this->message = $validateDate['message'];
            $this->status = false;
            return false;
        }

        $dateConfirm = StringUtil::convertDateBR($dateConfirm);

        $dominio = 'oipaguefacil.om.br';
        $carteira = 500;
        $currentProduct['Contrato'] = str_replace('-', '', $currentProduct['Contrato']);
        $sql  = ' INSERT INTO alega_pagamento ( ';
        $sql .= ' boleto_id, titulo, cpf,
                  data_informada, data_cad, dominio,
                  contrato, idcarteira, token,
                  idorigem) ';
        $sql .= ' VALUES (
                  "'.$currentProduct['Contrato'].'", "'.$currentProduct['Contrato'].'", "'.$clientData['DocumentoConsumidor'].'",
                  "'.$dateConfirm.'", NOW(), "'.$dominio.'",
                  "'.$currentProduct['Contrato'].'", "'.$carteira.'", "'.$clientData['Token'].'",
                  3)';

        $this->executeSql($sql, null);

        $this->status = true;
        return $this->status;
    }

    /**
     * @param string $date (dd/mm/yyyyy)
     * @return bool
     */
    public function validateDateRegisterPayment($date)
    {
        $backDaysToRegisterPayments = 30;

        $return = [
            'status' => false,
            'message' => 'Data inválida'
        ];

        if (strstr($date, '/') != '') {
            $dateParts = explode('/', $date);

            $datePartsCompare = $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0];
            $format = 'Y-m-d';

            $dateCompare = \DateTime::createFromFormat($format, $datePartsCompare);
            $dateNow = new \DateTime('now');
            $dti = $dateNow->diff($dateCompare);

            if ($dateCompare <= $dateNow && $dti->days <= $backDaysToRegisterPayments) {
                $return = [
                    'status' => true,
                    'message' => ''
                ];
            } elseif ($dateCompare > $dateNow) {
                $return = [
                    'status' => false,
                    'message' => 'Você selecionou uma data maior que a data de hoje. Se deseja agendar um pagamento,
                                  volte para a tela inicial e escolha a opção: Visualizar seus débitos.'
                ];
            } elseif ($dateCompare < $dateNow && $dti->days > $backDaysToRegisterPayments) {
                $return = [
                    'status' => false,
                    'message' => 'Você pode registrar somente pagamentos feitos nos últimos '.$backDaysToRegisterPayments.' dias.'
                ];
            }
        }

        return $return;
    }

    public function getDatesPayment()
    {
        $days = self::RANGE_DAYS;

        $dateNow = new \Datetime('now');
        //$dateNow->setDate(2016,11,11); // #test com data próxima ao domingo
        $dateStart = $dateNow->format('Y-m-d');
        $dateEnd =  $dateNow->add(new \DateInterval('P'.($days - 1).'D'))->format('Y-m-d');

        return $this->getDatesFromRange($dateStart, $dateEnd, 'd/m/Y');
    }

    /**
     * Generate an array of string dates between 2 dates
     *
     * @param string $start Start date
     * @param string $end End date
     * @param string $format Output format (Default: Y-m-d)
     *
     * @return array
     */
    function getDatesFromRange($start, $end, $format = 'Y-m-d')
    {
        $interval = new \DateInterval('P1D');

        $realEnd = new \DateTime($end);
        $realEnd->add($interval);

        $period = new \DatePeriod(new \DateTime($start), $interval, $realEnd);

        $dates = [];
        $addDay = false;

        foreach($period as $date) {
            if($date->format('N') == 7) {
                $addDay = true;
            }

            if(!$addDay) {
                $dates[] = $date->format($format);
            } else {
                $dates[] = $date->add(new \DateInterval('P1D'))->format($format);
            }
        }

        return $dates;
    }

    public function getRangeDays()
    {
        return self::RANGE_DAYS;
    }

    public function issueBoleto($currentProduct,$dealId)
    {
        $uri = '/boleto/emitirboletos';
        $pars = [
            'IdNegociacao' => $dealId
        ];

        $returnApi = $this->getDataApi($uri, $pars);
        $this->status = (!isset($returnApi['Status']) && $returnApi['Erro'] === false);
        $status = false;
        if ($this->status) {
            $status = true;
            $data = $returnApi['Data'];

            $boletos = [];
            $i = 0;
            foreach ($data as $key => $bol) {
                $bol['VencOrigial'] = StringUtil::convertDateDB($currentProduct['Parcelas'][$i]['Vencimento']);
                $bol['Valor'] = Convert::formatValue($bol['Valor']);
                $bol['Vencimento'] = str_replace('T00:00:00', '', $bol['Vencimento']);
                $bol['Vencimento'] =  StringUtil::convertDateDB($bol['Vencimento']);

                $boletos[] = $bol;
                ++$i;
            }

            $this->boletoData = $boletos;
        }
        return $status;
    }

    public function issueBoletoDeal($deal)
    {
        $uri = '/boleto/emitirboletos';
        $pars = [
            'IdNegociacao' => $deal['IdNegociacao']
        ];

        $returnApi = $this->getDataApi($uri, $pars);
        $status = false;
        $this->status = (!isset($returnApi['Status']) && $returnApi['Erro'] === false);
        if ($this->status) {
            $status = true;
            $data = $returnApi['Data'];
            $boletos = [];
            foreach ($data as $key => $bol) {
                $bol['VencOrigial'] = ' -- ';
                $bol['Contrato'] = $deal['Contrato'];
                $bol['Valor'] = Convert::formatValue($bol['Valor']);
                $bol['Vencimento'] = str_replace('T00:00:00', '', $bol['Vencimento']);
                $bol['Vencimento'] =  StringUtil::convertDateDB($bol['Vencimento']);

                $boletos[] = $bol;
            }

            $this->boletoData = $boletos;
        }
        return $status;
    }

    public function getDealId()
    {
        return $this->dealId;
    }

    public function details($clientData, $contract)
    {
        $uri = '/Contas/ConsultarDadosAuxiliares';
        $pars = [
            'Contrato' => $contract,
            'ToKen' => $clientData['Token'],
        ];

        $returnApi = $this->getDataApi($uri, $pars);
        $invoices = [];
        $this->status = (!isset($returnApi['Status']) && $returnApi['Erro'] === false);
        if ($this->status) {
            $invoices = $returnApi['Data'];
            foreach ($invoices as $key => $part) {
                $totalInvoices = 0;
                $invoices[$key]['Items'] = [];
                foreach ($invoices[$key]['Valores'] as $keyValue => $value) {
                    $value['Nome'] = StringUtil::getSlug($value['Nome']);

                    if (in_array($value['Nome'], ['dt_bord_parc', 'vcto_parc'])) {
                        $value['Valor'] = str_replace(' 00:00:00', '', $value['Valor']);
                    }
                    $invoices[$key]['Items'][$value['Nome']] = $value['Valor'];
                }
                unset($invoices[$key]['Valores']);
                $invoices[$key]['ValorInvoices'] = $totalInvoices;
                $invoices[$key]['ValorInvoicesFormat'] = $this->formatValue($totalInvoices);
            }
        }

        return $invoices;
    }

    public function registerDealData($clientData, $contract, $currentProduct, $deal, $boletos, $products)
    {
        $data = [
            'contract' => $contract,
            'product' => $currentProduct,
            'deal' => $deal,
            'boletos' => $boletos,
            'products' => $products
        ];

        $json = json_encode($data);
        $dataVencimento = StringUtil::convertDateBR($boletos[0]['Vencimento']);

        $sql  = " SELECT id ";
        $sql .= " FROM acordo ";
        $sql .= " WHERE documento = '".$clientData['DocumentoConsumidor']."'
                  AND contrato = '".$contract."'";

        $rs = $this->executeSql($sql, 'all');

        if (!$rs) {
            $sql  = " INSERT INTO acordo ( ";
            $sql .= " documento, contrato, dados, data_cad, data_vencimento) ";
            $sql .= " VALUES (
                  '".$clientData['DocumentoConsumidor']."', '".$contract."', '".$json."',
                  NOW(), '".$dataVencimento."')";

            $this->executeSql($sql, null);
        }
    }


    public function getBoletoCampanha($parcels, $boletoId)
    {
        $boletoData = [];
        foreach ($parcels as $parcel) {
            if ($parcel['Id'] == $boletoId) {
                $boletoData = $parcel;
                break;
            }
        }

        $boletoData['LinhaDigitavel'] = $boletoData['Codigodebarrasdebito'];

        return $boletoData;
    }

    public function getBoletoCarga($parcels, $boletoId)
    {
        $boletoData = [];
        foreach ($parcels as $parcel) {
            if ($parcel['Id'] == $boletoId) {
                $boletoData = $parcel;
                break;
            }
        }

        $boletoData['LinhaDigitavel'] = $boletoData['Codigodebarrasdebito'];

        return $boletoData;
    }

    public function generateBoleto($data)
    {
        $content = file_get_contents(__DIR__.'\..\..\..\\public\\boletos\\index.html');

        $file = '';
        $protocol = 'http';
        $url = 'http://localhost:8088/boletos/';
        $path = 'C:\www\app-oi\public\boletos/';
        if (PROD) {
            $url = 'http://localhost:8088/boletos/';
            $path = 'C:\www\app-oi\public\boletos/';
            $protocol = 'https';
        }

        $host = $protocol.'://'.filter_input(INPUT_SERVER, 'HTTP_HOST');
        $content = str_replace('[url_base]', $url, $content);
        $linhaDigital = str_replace(['-', ' '], '', $data['boleto']['LinhaDigitavel']);

        $pars = [
            'Path' => $path,
            'LinhaDigitavel' => $linhaDigital
        ];

        $codigoBarra = $this->getReturnData('/boleto/oi-boleto', $pars);
        $codigoBarra = str_replace('"', '', $codigoBarra);

        $content = str_replace('[url_base]', $url, $content);

        $content = str_replace('[cliente_nome]', $data['client']['NomeConsumidor'], $content);
        $content = str_replace('[contrato]', $data['product']['Contrato'], $content);

        $content = str_replace('[linha_digital]', $data['boleto']['LinhaDigitavel'], $content);
        $content = str_replace('[valor_boleto]', $data['boleto']['Valor'], $content);
        $content = str_replace('[vencimento]', $data['boleto']['Vencimento'], $content);
        $content = str_replace('[vencimento_original]', $data['boleto']['VencOrigial'], $content);

        $content = str_replace('[codigo_barra_img]', $codigoBarra, $content);
        echo $content; exit();
        // instantiate and use the dompdf class
        $html2pdf = new Html2Pdf();
        $html2pdf->writeHTML($content);
        $html2pdf->output();

        exit();

        return $file;
    }

    public function getCodigoBarra($data)
    {
        $path = 'C:\\www\\app-oi\\public\\boletos\\';
        if (PROD || TEST) {
            $path = 'C:\\inetpub\\wwwroot\\AppOi\\public\\boletos\\';
        }

        $linhaDigital = str_replace(['-', '.',  '_', ' '], '', $data['LinhaDigitavel']);

        $pars = [
            'Path' => $path,
            'LinhaDigitavel' => $linhaDigital
        ];
        $codigoBarra = $this->getReturnData('/boleto/oi-boleto', $pars);
        return str_replace('"', '', $codigoBarra);
    }

    public function downloadBoleto($deal)
    {
        $uri = '/boleto/imprimirboletos';
        $pars = [
            'IdNegociacao' => $deal
        ];

        $data = $this->getDataApi($uri, $pars);
        $nameFile = '0000000'.$deal;

        $files = [];
        foreach ($data['Data'] as $index => $file) {
            $content = $file['Arquivo'];
            $name = $nameFile.'-'.$index;
            $namePath = __DIR__.'\..\..\..\\public\\boletos\\' . $name.'.pdf';
            $bol = fopen($namePath, 'w+');
            fwrite($bol, base64_decode($content));
            fclose($bol);
            $files[] = [
                'file' => $namePath,
                'name' =>  $name.'.pdf',
            ];
        }

        return $files;
    }

    public function registerEmail($clientData, $email, $arquivo)
    {
        if ($email != "") {
            $dominio = 'oipaguefacil.com.br';
            $assunto = 'Boleto OI';

            $arquivo = str_replace('\\', '/', $arquivo);

            $corpo = file_get_contents(self::FOLDER_EMAIL . 'boleto.html') ;
            $corpo = str_replace('[nome]', $clientData['NomeConsumidor'], $corpo);
            $carteira = 1;

            $sql  = ' INSERT INTO fila_envio_email ( ';
            $sql .= ' dominio, destinatario, anexos, datacad, assunto, corpo, idcarteira) ';
            $sql .= ' VALUES ("'.$dominio.'", "'.$email.'", "'.$arquivo.'", NOW(), "'.$assunto.'", "'.$corpo.'", "'.$carteira.'")';

            $this->executeSql($sql, null);
        }

        return true;
    }

    public function registerPhone($currentProduct, $option, $boleto, $phone)
    {
        if ($phone != '') {
            $deal = $currentProduct['deals'][$option];

            $boleto['Valor'] = str_replace(' ', '', $boleto['Valor']);
            $boleto['Valor'] = str_replace('$', 'S', $boleto['Valor']);

            $mensagem  = 'Cliente OI:Segue linha digitavel para pagamento da sua negociacao via portal:';
            $mensagem .= $boleto['LinhaDigitavel'];
            $mensagem .= ' Venc:' . substr($boleto['Vencimento'], 0, 5);
            $mensagem .= 'Valor:' . $boleto['Valor'];

            $idcarteira = 1;
            $idConfigSMS = 4;

            $sql  = ' INSERT INTO fila_envio_sms ( ';
            $sql .= ' telefone, datacad, mensagem, idcarteira, idConfigSMS) ';
            $sql .= ' VALUES ("'.$phone.'", NOW(), "'.$mensagem.'", "'.$idcarteira.'", "'.$idConfigSMS.'")';

            $this->executeSql($sql, null);
        }

        return true;
    }

    public function getPathFiles()
    {
        return 'C:\\Temp\\boletos\\';
    }

    public function getBoleto()
    {
        return $this->boletoData;
    }




}