<?php

namespace App\Service;

use App\Util\StringUtil;
use GuzzleHttp\Client as ClientBrowser;

/**
 *
 * Class ContactService
 * @package App\Service
 */
class ContactService extends BaseService
{

    public function defineContact($client)
    {
        $doc = isset($client['DocumentoConsumidor']) ? $client['DocumentoConsumidor'] : $client['cpf'];

        $name = isset($client['NomeConsumidor']) ? $client['NomeConsumidor'] : '';
        if (isset($client['name'])) {
            $name = $client['name'];
        }

        $phone = isset($client['phone']) ? $client['phone'] : '';
        $email = isset($client['email']) ? $client['email'] : '';

        $f = $this->getPool();
        $k = (isset($client['token'])) ? $client['token'] : $doc;
        $phone = ($phone != null) ? str_replace([' ', '-', '(', ')'], '', $phone) : '';
        $cpf = $doc;
        $email = (($email != null) ? $email : '');

        $valorFatura = 0;
        $codBarras = '000';
        $dueDate = '00/00/0000';
        $custom1 = 1;
        $custom2 = 0;

        $parameters = [
            'method'   => 'setMcCadastroCliente',
            'f'  => $f,
            'k' => $k,
            'nome' => $name,
            'telefone' => $phone,
            'cpf' => $cpf,
            'email'  =>$email,
            'valor_fatura'  => $valorFatura,
            'vencimento_fatura'   => $codBarras,
            'cod_barras_fatura'   => $dueDate,
            'custom1'   => $custom1,
            'custom2'   => $custom2,
        ];

        $return = $this->callWs($parameters);
        if (count($return) > 0) {
            foreach ($return['canais_liberados'] as $key => $value) {
                $canalSlug = StringUtil::getSlug($value['canal']);
                $return['canais_liberados'][$key]['channel'] = str_replace('-', '', $canalSlug);
            }
        }

        return $return;
    }

    public function sendContact($dataContact, $pars)
    {
        $email = isset($pars['email']) ? $pars['email'] : '';
        $phone = isset($pars['phone'])  ? str_replace([' ', '-', '(', ')'], '', $pars['phone']) : '';

        $parameters = [
            'method'   => 'setMcAtendimentoMultiCanal',
            'chave_integracao' => $dataContact['chave_integracao'],
            'id_canal' => $pars['channel'],
            'telefone' => $phone,
            'email' => $email
        ];
        $this->callWs($parameters);
        $this->setMessage($dataContact, $pars);
        return true;
    }

    public function setMessage($dataContact, $pars)
    {
        $channel = $pars['channel'] - 1;
        $selectChannel = $dataContact['canais_liberados'][$channel];

        if ($selectChannel['channel'] == 'whatsapp') {
            $this->message = 'Aguarde nosso contato via Whatsapp.';
        } else if ($selectChannel['channel'] == 'sms') {
            $this->message = 'Você receberá um SMS em instantes para prosseguir com o atendimento.';
        } else if ($selectChannel['channel'] == 'email') {
            $this->message = 'Aguarde nosso contato via E-mail.';
        } else if ($selectChannel['channel'] == 'chat') {
            $this->message = '';
        } else if ($selectChannel['channel'] == 'ligacao') {
            $this->message = 'Aguarde nossa ligação.';
        } else if ($selectChannel['channel'] == 'telegram') {
            $this->message = 'Aguarde nosso contato via Telegram.';
        } else if ($selectChannel['channel'] == 'facebook') {
            $this->message = '';
        } else if ($selectChannel['channel'] == 'chat') {
            $this->message = '';
        }
    }

    /**
     * @param $parameters
     * @param string $method
     * @return array
     */
    public function callWs($parameters, $method = 'POST')
    {
        $client = new ClientBrowser(['proxy' => '']);
        $data = [];
        $urlRequest = $this->getUrlWs();
        try {

            $response = $client->request($method, $urlRequest,
                ['form_params' => $parameters]
            );

            if ($response->getStatusCode()) {
                $body = $response->getBody();
                $remainingBytes = $body->getContents();
                $json = json_decode($remainingBytes, true);
                $this->registerInfoCallApi($urlRequest, $parameters, $remainingBytes);

                if ($json['status'] === 'success') {
                    $data = $json['data']['return'];
                }
            }
        } catch(\Exception $e) {
            $this->registerInfoCallApi($urlRequest, $parameters, $e->getMessage());
            $dataError = [
                'Status' => false,
                'Message' => 'Erro no sistema.'
            ];
            return $dataError;
        }

        return $data;
    }

    public function getUrlWs()
    {


        $token = 'b8e69c944df51ea08b7efe610e0f0f1cc5a02d5b';
        return 'http://srvcloo2.callflex.com.br/omne/webservice/?token=' . $token;
    }

    public function getPool()
    {
        return 10;
    }

    public function filterChannel($id, $dataContact)
    {
        $contact = [];
        foreach ($dataContact['canais_liberados'] as $channel) {
            if ($channel['id_canal'] == $id) {
                $channel['template'] = StringUtil::getSlug($channel['canal']);
                $contact = $channel;
                break;
            }
        }
        return $contact;
    }

}