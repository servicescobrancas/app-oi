<?php

namespace App\Service;

use App\Entities\Client;

use App\Validate\Cpf as ValidateCpf;

/**
 * Class AccessService
 * @package App\Service
 */
class AccessService extends BaseService
{

    const FOLDER_DATA =  __DIR__.'\..\..\..\scripts';

    const MESSAGE_DOC_INVALID = 'O cpf informado não confere!';
    const MESSAGE_INVALID_CPF = 'Informe um CPF válido!';

    /**
     * @var Client
     */
    protected $clientEntity = null;

    /**
     * @var string
     */
    protected $token;


    public function checkRecaptcha()
    {
        $url = "https://www.google.com/recaptcha/api.js";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_exec($ch);
        $info = curl_getinfo($ch);

        return ($info["http_code"] == 200);
    }

    public function access($pars)
    {
        $doc = $pars['cpf'];
        $uri = '/token/gerartoken';
        $pars = [
            'DocumentoConsumidor' => $doc,
            'IdentificadorUnidadeDeCobranca' => UNIT,
        ];

        $returnApi = $this->getDataApi($uri, $pars);

        $this->status = ($returnApi['Erro'] === false);
        if ($this->status) {

            $tokenApp =  $returnApi['Data']['Token'];
            $this->data = $this->getDataToken($doc, $tokenApp);

            $this->setHeaders(['guid' => $this->data['Guid']]);
            $this->clearDeals($this->data['DocumentoConsumidor']);
        }

        return $this->status;
    }

    private function getDataToken($doc, $token)
    {
        $uri = '/token/token';
        $pars = [
            'DocumentoConsumidor' => $doc,
            'ToKen' => $token
        ];
        $return = $this->getDataApi($uri, $pars);
        $data = [];
        $this->status = ($return['Erro'] === false);
        if ($this->status) {
            $data =  $return['Data'];
        }
        return $data;
    }

    public function registerData($data)
    {
        $sql  = ' INSERT INTO clientes ( ';
        $sql .= ' token, documento_consumidor, nome_consumidor,
                  guid, data_aniversario, email,
                  telefone, data_cad) ';

        $sql .= ' VALUES (
                  "'.$data['Token'].'", "'.$data['DocumentoConsumidor'].'", "'.$data['NomeConsumidor'].'",
                  "'.$data['Guid'].'", "'.$data['date'].'", "'.$data['email'].'",
                  "'.$data['phone'].'", NOW())';

        $this->executeSql($sql, null);
    }

    /**
     * @param $doc
     */
    public function clearDeals($doc)
    {
        $uri = '/negociacao/cancelar';
        $pars = [
            'DocumentoConsumidor' => $doc
        ];

        $returnApi = $this->getDataApi($uri, $pars);
        $this->status = ($returnApi['Erro'] === false);
    }

    /**
     * @return Client
     */
    public function getClientEntity()
    {
        return $this->clientEntity;
    }

    public function getClientData()
    {
        return $this->data;
    }
}