<?php

namespace App\Service;

/**
 *
 * Class ChatService
 * @package App\Service
 */
class ChatService extends BaseService
{

    const TYPE_DOC = 1000;
    const TYPE_INVOICE = 1001;
    const TYPE_RECEIVE = 1002;
    const TYPE_FINISH = 1003;

    private $id;

    private $token;

    /**
     * Message from result operation
     * @var string
     */
    protected $messages = [];

    public function processOpenQuestion($pars)
    {
        $this->message[] = 'Boa tarde José, para prosseguir com seu atendimento digite os últimos 4 dígitos de seu CPF (0344059****).';
        $this->id = self::TYPE_DOC;
        $this->token = 'p12we3409E3r4fgaF';

        return true;
    }

    /**
     * @todo - checar se é a primeira interação. Se for, valida o cpf.
     *
     * @param $pars
     * @return bool
     */
    public function processAnwser($pars)
    {
        if ( ($pars['id'] == self::TYPE_DOC) ) {
            if (!$this->validateDoc($pars['message']) ) {
                $this->id = self::TYPE_DOC;
                $this->message[] = 'Para prosseguir, informe corretamente os os últimos 4 dígitos de seu CPF : 0344059**** !';
                return false;
            }

            if (!$this->searchInvoice()) {
                $this->id = self::TYPE_DOC;
                $this->message[] = 'Obrigado pela confirmação. Não encontramos nenhuma pendência em CPF!';
                return false;
            }
        }

        else if ( ($pars['id'] == self::TYPE_INVOICE) ) {
            $this->optionsPayment();
        }

        else if ( ($pars['id'] == self::TYPE_RECEIVE) ) {
            $this->optionsReceive();
        }

        return true;
    }

    public function validateDoc($doc)
    {
        return ($doc == 1234);
    }

    public function searchInvoice()
    {
        $this->id = self::TYPE_INVOICE;

        $this->message[]  = 'Obrigado pela confirmação. <br />José, Não identificamos o pagamento da fatura com vencimento em 09/09/2016.
                            O Contrato é o 456878467 e o valor atualizado é de R$ 32,80.';

        $menu = '';
        $menu .= 'Digite o número de uma das opções para prosseguir:<br/>';
        $menu .= '1 - PAGAMENTO ATÉ AMANHÃ<br/>';
        $menu .= '2 - AGENDAR PAGAMENTO<br/>';
        $menu .= '3 - JÁ REALIZEI O PAGAMENTO<br/>';
        $menu .= '4 - ATENDIMENTO<br/>';

        $this->message[] = $menu;

        return true;
    }

    public function optionsPayment()
    {
        $this->id = self::TYPE_RECEIVE;

        $this->message[]  = 'Registramos informação em nosso sistema! <br />';

        $menu = '';
        $menu .= 'Digite o número de uma das opções para receber seu boleto:<br/>';
        $menu .= '1 - DOWNLOAD<br/>';
        $menu .= '2 - E-MAIL<br/>';
        $menu .= '3 - SMS<br/>';

        $this->message[] = $menu;

        return true;
    }

    public function optionsReceive()
    {
        $this->id = self::TYPE_FINISH;

        $this->message[]  = 'Registramos informação em nosso sistema! <br />';

        $menu = '';
        $menu .= '<a href="#">Faça download do boleto</a><br/>';

        $this->message[] = $menu;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getMessages()
    {
        return $this->messages;
    }
}