<?php

namespace App\Storage;

use Elasticsearch\ClientBuilder;

class Elastic
{

    /**
     * @var ClientBuilder
     */
    public $client;

    /**
     * @var string
     */
    private $index;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $id = '';

    /**
     * @var array
     */
    private $data;

    /**
     * @var integer
     */
    private $from = 0;

    /**
     * @var integer
     */
    private $size = 10;

    /**
     * @var array
     */
    private $sort = null;

    /**
     * Elastic constructor.
     * @param string $index
     */
    public function __construct($index)
    {
        $this->client = ClientBuilder::create()->build();
        $this->index = $index;
        if(null !== $index) {
            $this->index = $index;
        }
    }

    /**
     * @param $index
     */
    public function setIndex($index)
    {
        $this->index = $index;;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param string $id
     * @return Elastic
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param array $data
     * @return Elastic
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param int $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @param array $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $data
        ];

        return $this->client->index($params);
    }

    public function update(array $data)
    {
        $id = $data['id'];
        unset($data['id']);

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => [
                'doc' => $data
            ],
            'id' => $id
        ];

        try {
            $response = $this->client->update($params);
            $status = (bool)$response['_shards']['successful'];
        } catch(\Exception $e) {
            $status = false;

            echo __FILE__.'='.__LINE__;
            echo $e->getTraceAsString(); exit();
        }

        return  $status;
    }

    public function get($id)
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id
        ];

        return $this->client->get($params);
    }

    /**
     * @param $pars
     * @return array
     */
    public function getSearchMatch($pars)
    {
        return [
            'query' => [
                'match' => $pars
            ]
        ];
    }

    /**
     * @param $query
     * @return array
     */
    public function find($query)
    {
        $body = [];
        if(is_array($query) && count($query) > 0) {
            $body['query'] = $query;
        }

        if(is_array($this->sort) && count($this->sort) > 0) {
            $body['sort'] = $this->sort;
        }

        $params = [
            'index' => $this->index,
            'type'  => $this->type,
            'body' => $body,
            'from' => $this->from,
            'size'  => $this->size
        ];

        return $this->client->search($params);
    }
}
