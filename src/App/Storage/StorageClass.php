<?php

namespace App\Storage;


/**
 * Manage PHP object in file system.
 *
 * Class StorageClass
 * @package App\Storage
 */
class StorageClass
{

    /**
     * @var
     */
    protected $key;

    /**
     * @var \stdClass
     */
    protected $obj;

    /**
     * Path to file
     * @var string
     */
    protected $fileName;

    /**
     *
     * @var
     */
    protected $resourceFile;


    public function store($key, $obj)
    {
        if($this->openFile($key)) {
            $this->write($obj);
        }

        return true;
    }

    private function openFile($key)
    {
        $this->fileName = __DIR__ . '\\..\\..\\..\\app\\data\\' . $key . '_list.json';
        $this->checkFile();

        $content = $this->getContentFile();
        return is_array($content);
    }

    public function write($obj)
    {
        $contentFile = $this->getContentFile();
        $strObj = serialize($obj);

        $contentFile[md5($strObj)] = $strObj;

        file_put_contents($this->fileName, json_encode($contentFile));

        //$contentFile = $this->getContentFile();
    }

    public function getContentFile()
    {
        $content = file_get_contents($this->fileName);
        return json_decode($content, true);
    }

    public function checkFile()
    {
        if(!is_file($this->fileName)) {
            $f = fopen($this->fileName, 'w+');
            fwrite($f, '{}');
            fclose($f);
        }
    }

    public function get($key)
    {
        // @todo - implement search object
    }

    public function delete($key, $index)
    {
        // @todo - implement remove object
    }

    public function __destruct()
    {
        if(is_resource($this->resourceFile)) {
            fclose($this->resourceFile);
        }
    }

}