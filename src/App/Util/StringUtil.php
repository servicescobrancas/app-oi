<?php

namespace App\Util;

class StringUtil 
{
    /**
     * 
     * @param  string
     * @return int
     * @desc:
     */
    public static function converteAcentosParaCaracter( $string )
    {
        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï",
                        "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç", 
                        "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", 
                        "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );

        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", 
                        "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", 
                        "A", "A", "A", "A", "A", "E", "E", "E", "E", 
                        "I", "I", "I", "I", "O", "O", "O", "O", "O", 
                        "U", "U", "U", "U", "C" );

        return str_replace( $array1, $array2, $string );
    }
        
    /**
     * Converte para maiusculo inclusive carcteres acenturados
     * 
     * @param  string $string
     * @return string
     */
    public static function converteMaiusculo($string)
    {
        // @todo - refatorar para vetor  
        $string = str_replace ("â", "Â", $string);
        $string = str_replace ("á", "Á", $string);
        $string = str_replace ("ã", "Ã", $string);
        $string = str_replace ("à", "A", $string);
        $string = str_replace ("ê", "Ê", $string);
        $string = str_replace ("é", "É", $string);
        $string = str_replace ("Î", "I", $string);
        $string = str_replace ("í", "Í", $string);
        $string = str_replace ("ó", "Ó", $string);
        $string = str_replace ("õ", "Õ", $string);
        $string = str_replace ("ô", "Ô", $string);
        $string = str_replace ("ú", "Ú", $string);
        $string = str_replace ("Û", "Û", $string);
        $string = str_replace ("ç", "Ç", $string);

        $string = strtoupper($string);

        return $string;
    }
    
    
    /**
     * troca os espaços de uma string por um separador
     * Entrada : Campanha Verão 1.1 - Saida : Campanha Verão 1-1
     * 
     * @param  string
     * @return int
     */
    public static function trocarCaracterPorSeparador($str, $caracter = '.', $separador = "_")
    {
        return str_replace($caracter, $separador, trim($str));
    }

        
    /**
     * troca os espaços de uma string por um separador
     * @todo   alterar para expressões regulares
     * Entrada : Campanha Verão 1.1 - Saida : campanha-verao-1-1
     * 
     * @param  string $string
     * @return string
     */
    public static function retirarCaracteresInvalidos($string)
    {
        // @todo - refatorar para vetor 
        $vetorInvalidos = array('',
                                '',
                                '');

        // @todo - mudar código para única linha de str_replace com looping
        // @todo - checar os caracteresd da string se for código ascii

        $string = str_replace('ª', '', $string);
        $string = str_replace('º', '', $string);
        $string = str_replace('?', '', $string);
        $string = str_replace('/', '', $string);
        $string = str_replace('\\', '_', $string);
        $string = str_replace('//', '', $string);
        $string = str_replace('_', '-', $string);
        $string = str_replace('!', '', $string);
        $string = str_replace('@', '', $string);
        $string = str_replace('®', '', $string);
        $string = str_replace('#', '', $string);
        $string = str_replace('$', '', $string);
        $string = str_replace('%', '', $string);
        $string = str_replace('¨', '', $string);
        $string = str_replace('&', '', $string);
        $string = str_replace('*', '', $string);
        $string = str_replace('(', '', $string);
        $string = str_replace(')', '', $string);
        $string = str_replace('=', '', $string);
        $string = str_replace('"', '', $string);
        $string = str_replace('=', '', $string);
        $string = str_replace('\'', '', $string);
        $string = str_replace('/', '', $string);
        $string = str_replace('´', '', $string);
        $string = str_replace('`', '', $string);
        $string = str_replace('{', '', $string);
        $string = str_replace('}', '', $string);
        $string = str_replace('[', '', $string);
        $string = str_replace(']', '', $string);
        $string = str_replace('~', '', $string);
        $string = str_replace('^', '', $string);
        $string = str_replace('<', '', $string);
        $string = str_replace('>', '', $string);
        $string = str_replace(':', '', $string);
        $string = str_replace(';', '', $string);
        $string = str_replace('/', '', $string);
        $string = str_replace('?', '', $string);

        return $string;
    }
    
    
    /**
     * @param  string
     * @return int
     * @desc: troca os espaços de uma string por um separador
     * Entrada : Campanha Verão 2008 - Saida : Campanha_Verão_2008
     */
    static function trocarEspacosPorSeparador($str, $separator = "_")
    {
            $str = str_replace("  ", " ", $str);
            $str = str_replace(" ", $separator, trim($str));

            return $str;
    }

        
    /**
     * @param  string $str
     * @return string
     * @desc:
     */
    static function retirarHTML($str)
    {
            return preg_replace("/(<\/?)(\w+)([^>]*>)/e", "", $str);

    }
    
    
    /**
     * Pega um texto qualquer e deixa em formato amigavel para url's
     * 
     * @param  string $string
     * @return string
     *  
     */
    static function getSlug($string)
    {   
        $string = StringUtil::converteAcentosParaCaracter($string);
        $string = StringUtil::retirarCaracteresInvalidos($string);
        $string = StringUtil::trocarEspacosPorSeparador($string, '-');

        return strtolower($string);
    }

    /**
     * Pega um texto qualquer e deixa em formato amigavel para url's
     *
     * @param  string $string
     * @return string
     *
     */
    static function getSlugCustom($string)
    {
        $string = StringUtil::converteAcentosParaCaracter($string);
        $string = StringUtil::retirarCaracteresInvalidos($string);
        $string = StringUtil::trocarEspacosPorSeparador($string, '_');
        $string = str_replace('-', '_', $string);
        return $string;
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * $num = 4; $zerofill= 3; returns "004"

     * @param  int $num
     * @param  int $zerofill
     * @return string
     */
    public static function zerofill($num, $zerofill) 
    {
        while(strlen($num) < $zerofill) {
            $num = "0".$num;
        }
        return $num;
    }
    
    
    /**
     * Pegar um item na url como valor, baseado no item anterior.
     * Ex: 
     * url -> /chave/roda
     * $this->getItemNaUrl('chave') retornará roda
     * 
     * @method getImagemNaUrl($itemAnterior)
     * @param  string  $itemAnterior
     * @param  string  $indice
     * @return string  $item
     * 
     */
    static function getItemNaUrl($itemAnterior, $indice = 1)
    {	
        $uri = $_SERVER['REQUEST_URI']; 
        
        if(strstr($uri, "?") != '')
        {
            $partsSlug   = explode("?", $uri);
            $uri = $partsSlug[0];
        }

        $itens    = explode('/', $uri);
        $numItens = count($itens);

        $item  = "";

        for($i = 0; $i < $numItens; $i++)
        {
            if($itens[$i] == $itemAnterior)
            {
                if(isset($itens[$i + $indice]))
                {
                    $item = $itens[$i + $indice];
                    break;
                }
            } 
        }

        return $item;
    }
        
    /**
     * @param  $string $url
     * @return string  $item
     * @desc   pegar o último item da url, útil quando para ser usado em buscas
     */
    static function getLastItemUrl($url)
    {
        $item = '';
        
        if(!strstr($url, '/'))
            return $item;
        
        $parts = explode('/', $url);
        $item = $parts[count($parts) - 1];
        
        if($item == '')
            $item = $parts[count($parts) - 2];
        
        return $item;
    }

    /**
     * Pegar um item na url como valor, baseado no item posterior.
     * Ex: 
     * url -> /chave/roda
     * $this->getItemAnteriorNaUrl('roda') retornará chave
     * 
     * @param  string  $itemPosterior
     * @return string  $item
     * 
     */
    function getItemAnteriorNaUrl($itemPosterior, $indice = 1)
    {	
        $uri   = $_SERVER['REQUEST_URI']; 
        $itens = explode('/', $uri);
        $numItens = count($itens);

        $item  = "";

        for($i = 0; $i < $numItens; $i++)
        {
            if($itens[$i] == $itemPosterior)
            {
                if(isset($itens[$i-$indice]))
                {
                    $item = $itens[$i-$indice];
                    break;
                }
            } 
        }

        return $item;
    }

    public static function convertDateDB($date)
    {
        if(strstr($date, 'T') != '') {
            $partsDate = explode('T', $date);
            $date = $partsDate[0];
        }

        if(strstr($date, '-') != '') {
            $partsDate = explode('-', $date);
            if(strlen($partsDate[0]) == 4) {
                return $partsDate[2] . '/' . $partsDate[1] . '/' . $partsDate[0];
            }
        }
        return $date;
    }

    public static function convertDateBR($date)
    {
        if(strstr($date, '/') != '') {
            $partsDate = explode('/', $date);
            if(strlen($partsDate[2]) == 4) {
                return $partsDate[2] . '-' . $partsDate[1] . '-' . $partsDate[0];
            }
        }
        return $date;
    }

    public static function encodeCpf($cpf)
    {
        /**
         * Encode cpf with add one, if number attends conditions.
         */
        $newFormat = "";
        $calc = substr($cpf, strlen($cpf) - 2, strlen($cpf) - 1);
        $numStr = strlen($cpf);

        for($i = 0; $i < $numStr; ++$i) {
            $number = $cpf[$i];
            $add = 1;
            if ($number == 0 || $number >= 8) {
                $add = 0;
            }
            $number += $add;
            $newFormat .= $number;
        };


        $noDigits = substr($newFormat, 0, strlen($newFormat) - 2);
        if($calc < 90) {
            $calc += 5;
            if($calc < 9) {
                $calc = '0' . $calc;
            }
        }
        return $noDigits.$calc;
    }

    public static function decodeCpf($strCpf)
    {
        $noDigits = substr($strCpf, 0, strlen($strCpf) - 2);
        $calc = substr($strCpf, strlen($strCpf) - 2, strlen($strCpf) - 1);
        $numStr = strlen($noDigits);

        $cpf = '';
        for($i = 0; $i < $numStr; ++$i) {
            $number = $noDigits[$i];
            $sub = 1;
            if($number > 0 && $number < 8) {
                $number -= $sub;
            }
            $cpf .= $number;
        }

        if($calc < 90) {
            $calc -= 5;
            if($calc < 9) {
                $calc = '0' . $calc;
            }
        }
        $cpf .= $calc;

        return $cpf;
    }
        
}
