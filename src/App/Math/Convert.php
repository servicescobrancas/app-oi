<?php

namespace App\Math;

class Convert
{
    public static function formatNumberToFloat($number)
    {
        $number = str_replace('R$ ', '', $number);
        $number = str_replace(',', '.', $number);

        return floatval($number);
    }

    public static function formatValue($number)
    {
        setlocale(LC_MONETARY, 'pt_BR');
        return 'R$ '.\number_format( $number, 2, ',', '.');
    }

}