<?php

namespace App\Entities;

class Client
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $completeName;

    /**
     * @var string
     */
    private $cpf;

    /**
     * @var string
     */
    private $contract;

    /**
     * @var string
     */
    private $phone;


    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $wallet;

    /**
     * @var integer
     */
    private $origin;

    /**
     * @var string
     */
    private $keyId;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $domain;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Client
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompleteName()
    {
        return $this->completeName;
    }

    /**
     * @param string $completeName
     * @return Client
     */
    public function setCompleteName($completeName)
    {
        $this->completeName = $completeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @return string
     */
    public function getCpfPartial()
    {
        return  substr($this->cpf, 0, 7);
    }

    /**
     * @param bool $empty
     * @return string
     */
    public function getCpfEnd($empty = false)
    {
        if($empty) {
            return "";
        }

        return substr($this->cpf, 7, 16);
    }

    /**
     * @param string $cpf
     * @return Client
     */
    public function setCpf($cpf)
    {
        if(strlen($cpf) == 10) {
            $cpf = '0' . $cpf;
        }

        $this->cpf = trim($cpf);
        return $this;
    }

    /**
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * @param string $contract
     * @return Client
     */
    public function setContract($contract)
    {
        $this->contract = $contract;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Client
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param int $wallet
     * @return Client
     */
    public function setWallet($wallet)
    {
        $this->wallet = $wallet;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param int $origin
     * @return Client
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return string
     */
    public function getKeyId()
    {
        return $this->keyId;
    }

    /**
     * @param string $keyId
     * @return Client
     */
    public function setKeyId($keyId)
    {
        $this->keyId = $keyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return Client
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return Client
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }
}